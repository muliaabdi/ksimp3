								<div class="row setup-content col-md-12" id="step-7">
									<div class="col-xs-6 col-md-offset-3">
										<div class="col-md-12">
										<br>
											<br>
											<br>
											<div class="form-group col-md-10">
												<h3 style="float:left;margin-left:5%;"">Nama Perusahaan  </h3><h3 style="float:left;margin-left:7%;color:#69CEF1;">: 	{{name}}</h3>
											</div>
											<div class="form-group col-md-10">
												<h3 style="float:left;margin-left:5%;"">Alamat Perusahaan  </h3><h3 style="float:left;margin-left:5%;color:#69CEF1;">: 	{{alamat}}</h3>
											</div>
												<br>
											<div class="form-group col-md-10">
												<h3 style="float:left;margin-left:5%;"">Contact Person  </h3><h3 style="float:left;margin-left:13%;color:#69CEF1;">: 	{{contact}}</h3>
											</div>
											<div class="form-group col-md-10">
												<h3 style="float:left;margin-left:5%;"">No Telp         </h3><h3 style="float:left;margin-left:28.5%;color:#69CEF1;">: 	{{telp}}</h3>
											</div>
											<div class="form-group col-md-10">
												<h3 style="float:left;margin-left:5%;"">Email           </h3><h3 style="float:left;margin-left:32.5%;color:#69CEF1;">: 	{{email}}</h3>
											</div>
												<br>
											<div class="form-group col-md-10">
												<h4>Pastikan Data Anda Benar Sebelum Menekan Tombol selesai</h4>
												<button class="btn btn-success btn-lg pull-right" type="submit">Selesai &rarr;</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</body>
				<!-- Core JavaScript Files -->
				<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
				<script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>

				<!-- bootstrap progress js -->
				<script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
				<script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
				<!-- icheck -->
				<script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>
				<!-- datatables -->
				<script src="<?php echo base_url(); ?>assets/js/dataTables/jquery.dataTables.js"></script>
				<!-- parsley -->
				<script src="<?php echo base_url(); ?>assets/js/parsley/parsley.min.js"></script>
				<!-- Step wizard -->
				<script type="text/javascript">
					$(document).ready(function () {
					  var navListItems = $('div.setup-panel div a'),
					          allWells = $('.setup-content'),
					          allNextBtn = $('.nextBtn');

					  allWells.hide();

					  navListItems.click(function (e) {
					      e.preventDefault();
					      var $target = $($(this).attr('href')),
					              $item = $(this);

					      if (!$item.hasClass('disabled')) {
					          navListItems.removeClass('btn-primary').addClass('btn-default');
					          $item.addClass('btn-primary');
					          allWells.hide();
					          $target.show();
					          $target.find('input:eq(0)').focus();
					      }
					  });

					  allNextBtn.click(function(){
					      var curStep = $(this).closest(".setup-content"),
					          curStepBtn = curStep.attr("id"),
					          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
					          curInputs = curStep.find("input[type='text'],input[type='url']"),
					          isValid = true;

					      $(".form-group").removeClass("has-error");
					      for(var i=0; i<curInputs.length; i++){
					          if (!curInputs[i].validity.valid){
					              isValid = false;
					              $(curInputs[i]).closest(".form-group").addClass("has-error");
					          }
					      }

					      if (isValid)
					          nextStepWizard.removeAttr('disabled').trigger('click');
					  });

					  $('div.setup-panel div a.btn-primary').trigger('click');
					});
				</script>


				<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
				<script src="<?php echo base_url(); ?>assets/js/caroline.js"></script>

				<script>
					NProgress.start();
	        //
	        NProgress.done();
	    </script>
	    </html>