<style type="text/css">
	h5{
		font-size: 10pt;
	}
	.cont-output-type{
		width: 5%;
		float: left;
	}
</style>
<script>
function myCreateFunction() {
    var table = document.getElementById("myTable");
    var row = table.insertRow(0);
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
			element1.type = "text";
			element1.class = "form-control";
			element1.name = "NAMAKANAL_UMUM[]";
			cell1.appendChild(element1);
    var cell2 = row.insertCell(1); 
    var element2 = document.createElement("input");
			element2.type = "radio";
			element2.class = "rb";
			element2.value = "1";
			element2.name = "DALAMNEGERI_UMUM[]";
			cell2.innerHTML = " Dalam Negeri ";
			cell2.appendChild(element2);
    var cell3 = row.insertCell(2); 
    var element3 = document.createElement("input");
			element3.type = "radio";
			element3.class = "rb";
			element3.value = "1";
			element3.name = "DALAMNEGERI_UMUM[]";
			cell3.innerHTML = " Luar Negeri ";
			cell3.appendChild(element3);
}

function myDeleteFunction() {
    document.getElementById("myTable").deleteRow(0);
}
function myCreateFunctionBerita() {
    var table = document.getElementById("myTableBerita");
    var row = table.insertRow(0);
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
			element1.type = "text";
			element1.class = "form-control";
			element1.name = "NAMAKANAL_BERITA[]";
			cell1.appendChild(element1);
    var cell2 = row.insertCell(1); 
    var element2 = document.createElement("input");
			element2.type = "radio";
			element2.class = "rb";
			element2.value = "1";
			element2.name = "DALAMNEGERI_BERITA[]";
			cell2.innerHTML = " Dalam Negeri ";
			cell2.appendChild(element2);
    var cell3 = row.insertCell(2); 
    var element3 = document.createElement("input");
			element3.type = "radio";
			element3.class = "rb";
			element3.value = "1";
			element3.name = "DALAMNEGERI_BERITA[]";
			cell3.innerHTML = " Luar Negeri ";
			cell3.appendChild(element3);
}

function myDeleteFunctionBerita() {
    document.getElementById("myTableBerita").deleteRow(0);
}
function myCreateFunctionMusik() {
    var table = document.getElementById("myTableMusik");
    var row = table.insertRow(0);
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
			element1.type = "text";
			element1.class = "form-control";
			element1.name = "NAMAKANAL_MUSIK[]";
			cell1.appendChild(element1);
    var cell2 = row.insertCell(1); 
    var element2 = document.createElement("input");
			element2.type = "radio";
			element2.class = "rb";
			element2.value = "1";
			element2.name = "DALAMNEGERI_MUSIK[]";
			cell2.innerHTML = " Dalam Negeri ";
			cell2.appendChild(element2);
    var cell3 = row.insertCell(2); 
    var element3 = document.createElement("input");
			element3.type = "radio";
			element3.class = "rb";
			element3.value = "1";
			element3.name = "DALAMNEGERI_MUSIK[]";
			cell3.innerHTML = " Luar Negeri ";
			cell3.appendChild(element3);
}

function myDeleteFunctionMusik() {
    document.getElementById("myTableMusik").deleteRow(0);
}
function myCreateFunctionOlahraga() {
    var table = document.getElementById("myTableOlahraga");
    var row = table.insertRow(0);
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
			element1.type = "text";
			element1.class = "form-control";
			element1.name = "NAMAKANAL_OLAHRAGA[]";
			cell1.appendChild(element1);
    var cell2 = row.insertCell(1); 
    var element2 = document.createElement("input");
			element2.type = "radio";
			element2.class = "rb";
			element2.value = "1";
			element2.name = "DALAMNEGERI_OLAHRAGA[]";
			cell2.innerHTML = " Dalam Negeri ";
			cell2.appendChild(element2);
    var cell3 = row.insertCell(2); 
    var element3 = document.createElement("input");
			element3.type = "radio";
			element3.class = "rb";
			element3.value = "1";
			element3.name = "DALAMNEGERI_OLAHRAGA[]";
			cell3.innerHTML = " Luar Negeri ";
			cell3.appendChild(element3);
}

function myDeleteFunctionOlahraga() {
    document.getElementById("myTableOlahraga").deleteRow(0);
}
function myCreateFunctionPendidikan() {
    var table = document.getElementById("myTablePendidikan");
    var row = table.insertRow(0);
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
			element1.type = "text";
			element1.class = "form-control";
			element1.name = "NAMAKANAL_PENDIDIKAN[]";
			cell1.appendChild(element1);
    var cell2 = row.insertCell(1); 
    var element2 = document.createElement("input");
			element2.type = "radio";
			element2.class = "rb";
			element2.value = "1";
			element2.name = "DALAMNEGERI_PENDIDIKAN[]";
			cell2.innerHTML = " Dalam Negeri ";
			cell2.appendChild(element2);
    var cell3 = row.insertCell(2); 
    var element3 = document.createElement("input");
			element3.type = "radio";
			element3.class = "rb";
			element3.value = "1";
			element3.name = "DALAMNEGERI_PENDIDIKAN[]";
			cell3.innerHTML = " Luar Negeri ";
			cell3.appendChild(element3);
}

function myDeleteFunctionPendidikan() {
    document.getElementById("myTablePendidikan").deleteRow(0);
}
function myCreateFunctionDakwah() {
    var table = document.getElementById("myTableDakwah");
    var row = table.insertRow(0);
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
			element1.type = "text";
			element1.class = "form-control";
			element1.name = "NAMAKANAL_DAKWAH[]";
			cell1.appendChild(element1);
    var cell2 = row.insertCell(1); 
    var element2 = document.createElement("input");
			element2.type = "radio";
			element2.class = "rb";
			element2.value = "1";
			element2.name = "DALAMNEGERI_DAKWAH[]";
			cell2.innerHTML = " Dalam Negeri ";
			cell2.appendChild(element2);
    var cell3 = row.insertCell(2); 
    var element3 = document.createElement("input");
			element3.type = "radio";
			element3.class = "rb";
			element3.value = "1";
			element3.name = "DALAMNEGERI_DAKWAH[]";
			cell3.innerHTML = " Luar Negeri ";
			cell3.appendChild(element3);
}

function myDeleteFunctionDakwah() {
    document.getElementById("myTableDakwah").deleteRow(0);
}
function myCreateFunctionFilm() {
    var table = document.getElementById("myTableFilm");
    var row = table.insertRow(0);
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
			element1.type = "text";
			element1.class = "form-control";
			element1.name = "NAMAKANAL_FILM[]";
			cell1.appendChild(element1);
    var cell2 = row.insertCell(1); 
    var element2 = document.createElement("input");
			element2.type = "radio";
			element2.class = "rb";
			element2.value = "1";
			element2.name = "DALAMNEGERI_FILM[]";
			cell2.innerHTML = " Dalam Negeri ";
			cell2.appendChild(element2);
    var cell3 = row.insertCell(2); 
    var element3 = document.createElement("input");
			element3.type = "radio";
			element3.class = "rb";
			element3.value = "1";
			element3.name = "DALAMNEGERI_FILM[]";
			cell3.innerHTML = " Luar Negeri ";
			cell3.appendChild(element3);
}

function myDeleteFunctionFilm() {
    document.getElementById("myTableFilm").deleteRow(0);
}
function myCreateFunctionLainnya() {
    var table = document.getElementById("myTableLainnya");
    var row = table.insertRow(0);
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
			element1.type = "text";
			element1.class = "form-control";
			element1.name = "NAMAKANAL_LAINNYA[]";
			cell1.appendChild(element1);
    var cell2 = row.insertCell(1); 
    var element2 = document.createElement("input");
			element2.type = "radio";
			element2.class = "rb";
			element2.value = "1";
			element2.name = "DALAMNEGERI_LAINNYA[]";
			cell2.innerHTML = " Dalam Negeri ";
			cell2.appendChild(element2);
    var cell3 = row.insertCell(2); 
    var element3 = document.createElement("input");
			element3.type = "radio";
			element3.class = "rb";
			element3.value = "1";
			element3.name = "DALAMNEGERI_LAINNYA[]";
			cell3.innerHTML = " Luar Negeri ";
			cell3.appendChild(element3);
}

function myDeleteFunctionLainnya() {
    document.getElementById("myTableLainnya").deleteRow(0);
}
</script>
<div class="form-group border-form col-md-12">
	<label>Total Kanal</label>
	<br><br>
	<div class="form-group border-form col-md-12">
			<h5>Umum/general entertainment </h5><input name="JMLKANAL_UMUM" class="cont-output-type"><h5>  buah, sebutkan nama kanal, asal dalam negeri atau luar negeri:</h5>
		<div class="form-group col-md-12" >
			<table id="myTable"  class="form-group col-md-12">
				
			</table>
			<button onclick="myCreateFunction()">Create row</button>
			<button onclick="myDeleteFunction()">Delete row</button>
			
		</div>
	</div>
	<div class="form-group border-form col-md-12">
			<h5>Berita </h5><input name="JMLKANAL_BERITA" class="cont-output-type"><h5>  buah, sebutkan nama kanal, asal dalam negeri atau luar negeri:</h5>
		<div class="form-group col-md-12">
			<table id="myTableBerita"  class="form-group col-md-12">
				
			</table>
			<button onclick="myCreateFunctionBerita()">Create row</button>
			<button onclick="myDeleteFunctionBerita()">Delete row</button>
		</div>
	</div>
	<div class="form-group border-form col-md-12">
			<h5>Musik </h5><input name="JMLKANAL_MUSIK" class="cont-output-type"><h5>  buah, sebutkan nama kanal, asal dalam negeri atau luar negeri:</h5>
		<div class="form-group col-md-12">
			<table id="myTableMusik"  class="form-group col-md-12">
				
			</table>
			<button onclick="myCreateFunctionMusik()">Create row</button>
			<button onclick="myDeleteFunctionMusik()">Delete row</button>
		</div>
	</div>
	<div class="form-group border-form col-md-12">
			<h5>Olahraga </h5><input name="JMLKANAL_OLAHRAGA" class="cont-output-type"><h5>  buah, sebutkan nama kanal, asal dalam negeri atau luar negeri:</h5>
		<div class="form-group col-md-12">
			<table id="myTableOlahraga"  class="form-group col-md-12">
				
			</table>
			<button onclick="myCreateFunctionOlahraga()">Create row</button>
			<button onclick="myDeleteFunctionOlahraga()">Delete row</button>
		</div>
	</div>
	<div class="form-group border-form col-md-12">
			<h5>Pendidikan dan anak-anak  </h5><input name="JMLKANAL_PENDIDIKAN" class="cont-output-type"><h5>  buah, sebutkan nama kanal, asal dalam negeri atau luar negeri:</h5>
		<div class="form-group col-md-12">
			<table id="myTablePendidikan"  class="form-group col-md-12">
				
			</table>
			<button onclick="myCreateFunctionPendidikan()">Create row</button>
			<button onclick="myDeleteFunctionPendidikan()">Delete row</button>
		</div>
	</div>
	<div class="form-group border-form col-md-12">
			<h5>Dakwah  </h5><input name="JMLKANAL_DAKWAH" class="cont-output-type"><h5>  buah, sebutkan nama kanal, asal dalam negeri atau luar negeri:</h5>
		<div class="form-group col-md-12">
			<table id="myTableDakwah"  class="form-group col-md-12">
				
			</table>
			<button onclick="myCreateFunctionDakwah()">Create row</button>
			<button onclick="myDeleteFunctionDakwah()">Delete row</button>
		</div>
	</div>
	<div class="form-group border-form col-md-12">
			<h5>Film  </h5><input name="JMLKANAL_FILM" class="cont-output-type"><h5>  buah, sebutkan nama kanal, asal dalam negeri atau luar negeri:</h5>
		<div class="form-group col-md-12">
			<table id="myTableFilm"  class="form-group col-md-12">
				
			</table>
			<button onclick="myCreateFunctionFilm()">Create row</button>
			<button onclick="myDeleteFunctionFilm()">Delete row</button>
		</div>
	</div>
	<div class="form-group border-form col-md-12">
			<h5>Lainnya  </h5><input name="JMLKANAL_LAINNYA" class="cont-output-type"><h5>  buah, sebutkan nama kanal, asal dalam negeri atau luar negeri:</h5>
		<div class="form-group col-md-12">
			<table id="myTableLainnya"  class="form-group col-md-12">
				
			</table>
			<button onclick="myCreateFunctionLainnya()">Create row</button>
			<button onclick="myDeleteFunctionLainnya()">Delete row</button>
		</div>
	</div>
	
</div>