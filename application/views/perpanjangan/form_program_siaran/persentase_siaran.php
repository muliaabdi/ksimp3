<style type="text/css">
	h5{
		font-size: 10pt;
		/*width: 50%;*/
		float: left;
	}
	.cont-output-type{
		width: 5%;
		float: left;
	}
</style>
<div class="form-group border-form col-md-12">
	<label>Persentase Siaran</label>
	<br><br>
	<div class="form-group border-form col-md-12">
			<h5>Lokal :</h5><input name="LOKAL_PS" class="cont-output-type"> %
			<h5>1. Kanal khusus berlangganan, produksi lingkungan sendiri :</h5><input name="LINGKUNGANSENDIRI_PS" class="cont-output-type"> %
			<h5>2. Kanal khusus berlangganan, produksi dari luar perusahaan/sister company :</h5><input name="LUARPERUSAHAAN_PS" class="cont-output-type"> %
			<h5>3. Kanal free-to-air (satelit atau terestrial, disiarkan bebas) :</h5><input name="FREETOAIR_PS" class="cont-output-type"> %
	</div>
	<div class="form-group border-form col-md-12">
			<h5>Asing :</h5><input name="ASING_PS" class="cont-output-type"> %
	</div>
</div>