	<!-- <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script> -->
	<body>
	<div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_right">
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Konfirmasi Data</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
		<div class="container bg-container">
			<div class="col-md-4" >
				<!-- <h1 style="text-align: left;color: black">Perpanjangan</h1> -->
				<div class="stepwizard col-md-offset-3">
					<div class="stepwizard-row setup-panel">
						<div class="stepwizard-step">
							<ul style="text-align: left; font-size: 13pt;margin-left: -30%;">
								<li><a href="#step-1"  class="btn btn-primary ">1. Data Administrasi</a></li>
								<li style="text-align: left; font-size: 10pt;margin-left: 5%;"><a href="#step-2" class="btn btn-default " disabled="disabled">1.1 Data Manajemen</a></li>
								<li style="text-align: left; font-size: 10pt;margin-left: 5%;"><a href="#step-3"  class="btn btn-default " disabled="disabled">1.2 Data Kepegawaian</a></li>
								<li><a href="#step-4"  class="btn btn-default " disabled="disabled">2. Program Siaran</a></li>
								<li><a href="#step-5"  class="btn btn-default " disabled="disabled">3. Data Teknis</a></li>
								<li><a href="#step-6"  class="btn btn-default " disabled="disabled">4. File yang dilampirkan</a></li>
								<li><a href="#step-7"  class="btn btn-default " disabled="disabled">5. Konfirmasi</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8" style="margin-left: -9%">
				<form role="form" action="<?php echo site_url('pemohon_baru/add'); ?>" method="post"  enctype="multipart/form-data" ng-app="">
					<div class="row setup-content col-md-12" id="step-1">
						<legend>Data Administrasi</legend>
						<div class="col-md-10">
							<legend>A. <?php echo $nm_jenis_izin ?></legend>
						<input type="hidden" name="id_izin" value="<?php echo $id_izin;?>" >
