<script type="text/javascript">

	$(function(){

		$.ajaxSetup({
			type:"POST",
			url: "<?php echo base_url('register/alamat_kantor') ?>",
			cache: false,
		});

		$("#provinsi-pemancar").change(function(){

			var value=$(this).val();
			if(value>0){
				$.ajax({
					data:{modul:'kabupaten',id:value},
					success: function(respond){
						$("#kabupaten-kota-pemancar").html(respond);
					}
				})
			}

		});


		$("#kabupaten-kota-pemancar").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
					data:{modul:'kecamatan',id:value},
					success: function(respond){
						$("#kecamatan-pemancar").html(respond);
					}
				})
			}
		})

		$("#kecamatan-pemancar").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
					data:{modul:'kelurahan',id:value},
					success: function(respond){
						$("#kelurahan-desa-pemancar").html(respond);
					}
				})
			} 
		})

	})

</script>
<div class="form-group border-form">
	<label>A.	Stasiun Pengendali Penyiaran</label><br>
	<label>Alamat Pemancar</label>
	<div class="form-group border-form">
			<label class="control-label">Jalan</label>
			<textarea required="required" class="form-control" placeholder="Masukan Alamat Pemancar" name="ALAMATPEMANCAR"></textarea>
		</div>
		<div class="form-group">
			<label class="control-label">Provinsi</label>
			<select class="form-control" id="provinsi-pemancar" name="PROPPEMANCAR">
				<option value="0">-Pilih Provinsi-</option>
				<?php 
					foreach ($provinsi as $prov) {
					echo "<option value='$prov[id]'>$prov[name]</option>";
				}
				?>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Kota</label>
			<select class="form-control" id="kabupaten-kota-pemancar" name="KABPEMANCAR">
				<option value="0">-Pilih Kota-</option>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Kecamatan</label>
			<select class="form-control" id="kecamatan-pemancar" name="KECPEMANCAR">
				<option value="0">-Pilih Kecamatan-</option>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Kelurahan</label>
			<select class="form-control" id="kelurahan-desa-pemancar" name="KELPEMANCAR">
				<option value="0">-Pilih Kelurahan-</option>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Kode Pos</label>
			<input type="text" name="KODEPOS_AP" class="form-control">
		</div>
		<div class="form-group">
			<label class="control-label">Fax</label>
			<input type="text" name="FAX_AP" class="form-control">
		</div>
		<div class="form-group">
			<label class="control-label">Nomor Telepon</label>
			<input type="text" name="NOTELP_AP" class="form-control">
		</div>
		<div class="form-group">
			<label class="control-label">Tinggi Lokasi</label>
			<input type="text" name="TINGGILOKASI_AP" class="form-control" placeholder="....meter di atas permukaan laut">
		</div>
		<div class="form-group">
			<label class="control-label">Koordinat</label>
			<input type="text" name="KOORDINAT_AP" class="form-control" placeholder="Masukan nilai koordinat">
		</div>
</div>