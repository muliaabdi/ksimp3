<div class="form-group border-form">
	<label>A.	Stasiun Pengendali Penyiaran</label>
	<label>Alamat Pemancar</label>
	<div class="form-group border-form">
			<label class="control-label">Jalan</label>
			<textarea required="required" class="form-control" placeholder="Masukan Alamat Pemancar" name="ALAMATSTASIUN"></textarea>
		</div>
		<div class="form-group">
			<label class="control-label">Provinsi</label>
			<select class="form-control" id="propinsi" name="PROPSTASIUN">
				<option value="0">-Pilih Provinsi-</option>
				<?php 
					foreach ($provinsi as $prov) {
					echo "<option value='$prov[id]'>$prov[name]</option>";
				}
				?>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Kota</label>
			<select class="form-control" id="kabupaten-kota" name="KABSTASIUN">
				<option value="0">-Pilih Kota-</option>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Kecamatan</label>
			<select class="form-control" id="kecamatan" name="KECSTASIUN">
				<option value="0">-Pilih Kecamatan-</option>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Kelurahan</label>
			<select class="form-control" id="kelurahan" name="KELSTASIUN">
				<option value="0">-Pilih Kelurahan-</option>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Kode Pos</label>
			<input type="text" name="KODEPOS_AS" class="form-control">
		</div>
		<div class="form-group">
			<label class="control-label">Nomor Telepon</label>
			<input type="text" name="NOTELP_AS" class="form-control">
		</div>
		<div class="form-group">
			<label class="control-label">Fax</label>
			<input type="text" name="FAX_AS" class="form-control">
		</div>
		<div class="form-group">
			<label class="control-label">Tinggi Lokasi</label>
			<input type="text" name="TINGGILOKASI_AS" class="form-control" placeholder="....meter di atas permukaan laut">
		</div>
		<div class="form-group">
			<label class="control-label">Koordinat</label>
			<input type="text" name="KOORDINAT_AS" class="form-control" placeholder="Masukan nilai koordinat">
		</div>
</div>