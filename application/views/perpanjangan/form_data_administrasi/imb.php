<div class="form-group border-form">
	<label>Izin Mendirikan Bangunan Kantor (IMB) *)</label>
	<div class="form-group">
		<label class="control-label">No</label>
		<input type="text" name="IMB_NO" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tanggal</label>
		<input type="date" name="IMB_TGL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Nama instansi yang menerbitkan</label>
		<input type="text" name="IMB_NM_INSTANSI_PENERBIT" class="form-control">
	</div>
</div>