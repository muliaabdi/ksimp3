<label>2. Komisaris</label>
<div class="form-group border-form">
	<label>Komisaris Utama</label>
	<div class="form-group">
		<label class="control-label">Nama</label>
		<input type="text" name="KU_NAMA" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tempat/Tanggal Lahir</label>
		<input type="date" name="KU_TTL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Kewarganegaraan</label>
		<input type="text" name="KU_KEWARGANEGARAAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Pendidikan</label>
		<input type="text" name="KU_PENDIDIKAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Telepon Kantor</label>
		<input type="text" name="KU_TELP_KANTOR" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">No HP</label>
		<input type="text" name="KU_NOHP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Fax</label>
		<input type="text" name="KU_FAX" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Email</label>
		<input type="text" name="KU_EMAIL" class="form-control">
	</div>
</div>
<div class="form-group border-form">
	<label>Komisaris......(apabila komisaris lebih dari satu agar ditambahkan datanya)</label>
	<div class="form-group">
		<label class="control-label">Nama</label>
		<input type="text" name="KOMISARIS_NAMA" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tempat/Tanggal Lahir</label>
		<input type="date" name="KOMISARIS_TTL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Kewarganegaraan</label>
		<input type="text" name="KOMISARIS_KEWARGANEGARAAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Pendidikan</label>
		<input type="text" name="KOMISARIS_PENDIDIKAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Telepon Kantor</label>
		<input type="text" name="KOMISARIS_TELP_KANTOR" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">No HP</label>
		<input type="text" name="KOMISARIS_NOHP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Fax</label>
		<input type="text" name="KOMISARIS_FAX" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Email</label>
		<input type="text" name="KOMISARIS_EMAIL" class="form-control">
	</div>
</div>