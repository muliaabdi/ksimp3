<div class="form-group border-form">
	<label>Izin Mendirikan Bangunan Menara (IMB Tower) *)</label>
	<div class="form-group">
		<label class="control-label">No</label>
		<input type="text" name="IMBT_NO" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tanggal</label>
		<input type="date" name="IMBT_TGL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Nama instansi yang menerbitkan</label>
		<input type="text" name="IMBT_NM_INSTANSI_PENERBIT" class="form-control">
	</div>
</div>