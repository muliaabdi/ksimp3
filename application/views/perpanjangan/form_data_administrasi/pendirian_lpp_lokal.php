<div class="form-group border-form">
	<label>Pendirian LPP Lokal</label>
	<label>(Badan Hukum yang di bentuk oleh Pemda dg persetujuan DPRD)</label>
	<div class="form-group">
		<label class="control-label">no</label>
		<input type="text" name="PLPP_LOKAL_NO" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tanggal</label>
		<input type="date" name="PLPP_LOKAL_TGL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Nama Penanggungjawab yang tercantum pada Badan Hukum</label>
		<input type="text" name="PLPP_LOKAL_NM_PJ" class="form-control">
	</div>
</div>