<legend>B. Aspek Permodalan *) (berasal dari kontribusi komunitasnya dan sumber lain yang sah dan tidak mengikat)</legend>
<div class="form-group border-form">
	<div class="form-group">
		<label class="control-label">Modal dasar</label>
		<input type="text" name="MODALDASAR" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Banyaknya saham</label>
		<input type="date" name="JMLSAHAM" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Nilai nominal saham per lembar</label>
		<input type="text" name="NOMINALSAHAM" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Modal yang ditempatkan</label>
		<input type="text" name="MODALDITEMPATKAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Nama pemegang saham </label>
		<br>
		<div class="form-group col-md-4">
			<input type="text" name="NAMA_PS" class="form-control" placeholder=".....(nama)"><br><br>
		</div>
		<div class="form-group col-md-4">
			<input type="text" name="BANYAK" class="form-control" placeholder="Pukul....S/D">
		</div>
		<div class="form-group col-md-4">
			<input type="text" name="PRESENTASE" class="form-control" placeholder="Pukul...">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label">Komposisi pemegang saham</label>
		<br>
		<label>a. WNI</label>
		<input type="text" name="KOMPOSISI" class="form-control" placeholder="....%">
		<label>b. WNA</label> 
		<input type="text" name="KOMPOSISI" class="form-control" placeholder="....%">
	</div>
	<div class="form-group">
		<label class="control-label">Jumlah Modal Awal Keseluruhan</label>
		<input type="text" name="MODALSETOR" class="form-control">
	</div>
</div>