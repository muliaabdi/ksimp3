<?php load_partials('header-login.html'); ?>

<body>
	<div class="container">
		<div class="content" style="margin: auto;text-align: center;color: black">
			<br>
			<h1>Pendaftaran</h1>
			<h4>Pilih jenis perizinan yang Anda Kehendaki</h4>
			<br>
			<br>
			<form method="post" action="<?php echo site_url('register/new'); ?>">
			<div class="form-group">
				<label><h4>Permohonan Izin Penyelenggaraan Penyiaran Lembaga Penyiaran</h4></label>
			<select class="form-control" name="id_izin" style="display: inline-block;width: 20%;">
				<?php foreach ($jenis_izin as $row ) { ?>
					<option value="<?php echo $row->id_izin;?>"><?php echo $row->nm_jenis_izin;?></option>
				<?php } ?>
			</select>
			</div>
			<br>
			<button type="submit" class="btn btn-primary" style="width: 30%;height: 40px;margin: auto;">Lanjut</button>
			</form>
			
		</div>
	</div>
</body>
 <!-- Core JavaScript Files -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>

    <!-- bootstrap progress js -->
    <script src="<?php echo base_url(); ?>assets/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url(); ?>assets/js/icheck/icheck.min.js"></script>
    <!-- datatables -->
    <script src="<?php echo base_url(); ?>assets/js/dataTables/jquery.dataTables.js"></script>
    <!-- parsley -->
    <script src="<?php echo base_url(); ?>assets/js/parsley/parsley.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/caroline.js"></script>
    
</html>