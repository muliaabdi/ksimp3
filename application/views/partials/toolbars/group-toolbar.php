<div class="btn-group btn-group-sm nav navbar-right panel_toolbox" role="group" aria-label="...">
    <?php 
        if(is_admin()) {
            if($page == "index"){
                echo anchor('group/add', '<i class="fa fa-plus"></i> New','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="New Group"');
            } else if($page == "create"){
                echo anchor('group', '<i class="fa fa-list"></i> List','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Group List"');
            } else if($page == "edit"){
                echo anchor('group', '<i class="fa fa-list"></i> List','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Group List"');
                // echo anchor('group/add', '<i class="fa fa-plus"></i> New','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="New Group"');
            }
        }
    ?>
    <button type="button" class="btn btn-default collapse-link" data-toggle="tooltip" data-placement="top" title="Collapse/Expand" style="float: right;"><i class="fa fa-chevron-up"></i></button>
</div>
<div class="clearfix"></div>