<div class="btn-group btn-group-sm nav navbar-right panel_toolbox" role="group" aria-label="...">
    <?php 
        if($page == "index"){
            echo anchor('assignment/add', '<i class="fa fa-plus-square"></i> New','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="New Assignment"');
        } else if($page == "create"){
            echo anchor('assignment', '<i class="fa fa-list"></i> List','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Assignment List"');
        } else if($page == "edit"){
            echo anchor('assignment', '<i class="fa fa-list"></i> List','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Assignment List"');
            echo anchor('assignment/add', '<i class="fa fa-plus-square"></i> New','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="New Assignment"');
        }
    ?>
    <button type="button" class="btn btn-default collapse-link" data-toggle="tooltip" data-placement="top" title="Collapse/Expand"><i class="fa fa-chevron-up"></i></button>
</div>
<div class="clearfix"></div>