<div class="btn-group btn-group-sm nav navbar-right panel_toolbox" role="group" aria-label="...">
    <?php echo anchor('dashboard', '<i class="fa fa-home"></i> Dashboard','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Dashboard"')?>
    <button type="button" class="btn btn-default collapse-link" data-toggle="tooltip" data-placement="top" title="Collapse/Expand"><i class="fa fa-chevron-up"></i></button>
</div>
<div class="clearfix"></div>