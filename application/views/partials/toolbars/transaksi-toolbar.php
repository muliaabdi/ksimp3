<div class="btn-group btn-group-sm nav navbar-right panel_toolbox" role="group" aria-label="...">
    <?php 
        if($page == "index"){
            echo anchor('transaksi/new', '<i class="fa fa-plus"></i> New','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="New Transaction"');
        } else if($page == "create"){
            echo anchor('transaksi', '<i class="fa fa-list"></i> List','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Transaction List"');
        } else if($page == "edit"){
            echo anchor('department', '<i class="fa fa-list"></i> List','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Department List"');
            echo anchor('department/add', '<i class="fa fa-plus"></i> New','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="New Department"');
        }
    ?>
    <!-- <button type="button" class="btn btn-default collapse-link" data-toggle="tooltip" data-placement="top" title="Collapse/Expand"><i class="fa fa-chevron-up"></i></button> -->
</div>
<div class="clearfix"></div>