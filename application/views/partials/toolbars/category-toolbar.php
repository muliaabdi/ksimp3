<div class="btn-group btn-group-sm nav navbar-right panel_toolbox" role="group" aria-label="...">
    <?php 
        if($page == "index"){
            echo anchor('category/add', '<i class="fa fa-plus"></i> New','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="New Category"');
        } else if($page == "create"){
            echo anchor('category', '<i class="fa fa-list"></i> List','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Category List"');
        } else if($page == "edit"){
            echo anchor('category', '<i class="fa fa-list"></i> List','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Category List"');
            echo anchor('category/add', '<i class="fa fa-plus"></i> New','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="New Category"');
        }
    ?>
    <button type="button" class="btn btn-default collapse-link" data-toggle="tooltip" data-placement="top" title="Collapse/Expand"><i class="fa fa-chevron-up"></i></button>
</div>
<div class="clearfix"></div>