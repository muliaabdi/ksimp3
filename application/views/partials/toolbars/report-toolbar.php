<div class="btn-group btn-group-sm nav navbar-right panel_toolbox" role="group" aria-label="...">
    <a href="<?= site_url('export/excel') ?>" class="btn btn-default toexcel" data-toggle="tooltip" data-placement="bottom" title="Export to Excel">
        <i class="fa fa-download"></i> Export to Excel
    </a>
    <a href="<?= site_url('export/pdf') ?>" class="btn btn-default topdf" data-toggle="tooltip" data-placement="bottom" title="Export to PDF">
        <i class="fa fa-download"></i> Export to PDF
    </a>
</div>
<div class="clearfix"></div>