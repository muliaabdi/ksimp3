<div class="btn-group btn-group-sm nav navbar-right panel_toolbox" role="group" aria-label="...">
    <?php 
        if($page == "index"){
            echo anchor('approval/add', '<i class="fa fa-plus-square"></i> New','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="New Approval"');
        } else if($page == "create"){
            echo anchor('approval', '<i class="fa fa-list"></i> List','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Approval List"');
        } else if($page == "edit"){
            echo anchor('approval', '<i class="fa fa-list"></i> List','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Approval List"');
            echo anchor('approval/add', '<i class="fa fa-plus-square"></i> New','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="New Approval"');
        }
    ?>
    <button type="button" class="btn btn-default collapse-link" data-toggle="tooltip" data-placement="top" title="Collapse/Expand"><i class="fa fa-chevron-up"></i></button>
</div>
<div class="clearfix"></div>