<div class="btn-group btn-group-sm nav navbar-right panel_toolbox" role="group" aria-label="...">
    <button type="button" class="btn btn-default view-mode" data-toggle="tooltip" data-placement="top" title="View Mode"><i class="glyphicon glyphicon-th-list"></i></button>
    <button type="button" class="btn btn-default sort-mode" data-toggle="tooltip" data-placement="top" title="Sorting Mode"><i class="fa fa-sort-amount-asc"></i></button>
    <ul role="menu" class="dropdown-menu dropdown-menu-right sort-dropdown-menu">
        <li> <a id="name-asc" data-sort-type="name" data-sort-mode="asc" data-user-type="<?php echo $user_type ?>">File Name (Ascending)</a> </li>
        <li> <a id="name-desc" data-sort-type="name" data-sort-mode="desc" data-user-type="<?php echo $user_type ?>">File Name (Descending)</a> </li>
        <!-- mungkin nanti bakal butuh untuk Date Created, Date Approval + Date Permission dan lain-lain -->
        <?php if(is_admin()) { ?>
            <li class="divider"></li>
            <li> <a id="date-asc" data-sort-type="date" data-sort-mode="asc" data-user-type="<?php echo $user_type ?>">Date Created (Ascending)</a> </li>
            <li> <a id="date-desc" data-sort-type="date" data-sort-mode="desc" data-user-type="<?php echo $user_type ?>">Date Created (Descending)</a> </li>
        <?php } else { ?>
            <li class="divider"></li>
            <li> <a id="date-asc" data-sort-type="date" data-sort-mode="asc" data-user-type="<?php echo $user_type ?>">Date Assigned (Ascending)</a> </li>
            <li> <a id="date-desc" data-sort-type="date" data-sort-mode="desc" data-user-type="<?php echo $user_type ?>">Date Assigned (Descending)</a> </li>
        <?php } ?>
    </ul>
    <?php if(have_authority()) { ?>
    <button type="button" class="btn btn-default switch-dashboard" data-toggle="tooltip" data-placement="top" title="Switch Dashboard" data-doc-state="333" data-user-type="empl" data-user-switch="<?php echo $is_switched ?>"><i class="fa fa-random"></i></button>
    <button type="button" class="btn btn-default grouping-dashboard" data-toggle="tooltip" data-placement="top" title="Grouping Dashboard"><i class="fa fa-tasks"></i></button>
    <?php } ?>
    <button type="button" class="btn btn-default hide-detail" data-toggle="tooltip" data-placement="top" title="Show/Hide Detail"><i class="fa fa-exchange"></i></button>
    <button type="button" class="btn btn-default collapse-link" data-toggle="tooltip" data-placement="top" title="Collapse/Expand"><i class="fa fa-chevron-up"></i></button>
</div>
<div class="clearfix"></div>