<div class="btn-group btn-group-sm nav navbar-right panel_toolbox" role="group" aria-label="...">
    <?php if(is_allowed($uid,'402',$orig_doc_id)) { echo anchor(site_url('uploads/doctmp/'.current_user().'/'.$subject.'.pdf'), '<i class="fa fa-download"></i> PDF</a>','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Download PDF" target="_blank"'); } ?>
    <?php // echo anchor(site_url('uploads/doctmp/'.current_user().'/'.$subject.'.docx'), '<i class="fa fa-download"></i> Docx</a>','class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Download Docx" target="_blank"')?>
    <?php if(is_allowed($uid,'402',$orig_doc_id)) : ?>
    <button type="button" class="btn btn-default collapse-link" data-toggle="tooltip" data-placement="top" title="Collapse/Expand"><i class="fa fa-chevron-up"></i></button>
    <?php endif; ?>
</div>
<div class="clearfix"></div>