<?php $this->load->view('_parts/public_header_view'); ?>

<div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_right">
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>KONFIRMASI KANTOR</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <?php if(isset($message)) {?>
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></span>
                        </button>
                        <strong>Information!</strong> <?php echo $message;?>
                    </div>
                <?php } ?>
                <div class="x_content">
                  <?php foreach ($alamat as $row): ?>
                  <form id="demo-form" class="form-horizontal form-label-left" method="post" action="<?php echo base_url().'konfirmasi/kantor/edit/'.$row->NO ?>">
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">ALAMAT
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->ALAMATKANTOR; ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="ALAMAT" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('ALAMAT'); ?>">
                      </div>
                      </div>
                    </div>
                    <div class='form-group'>
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">PROVINSI
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->PROPKANTOR; ?>">
                      </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                        <select class='form-control' name="PROVINSI" id='provinsi' onchange="document.getElementById('text_content').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        <?php 
                        foreach ($provinsi as $prov) {
                        echo "<option value='$prov[id]'>$prov[name]</option>";
                        }
                        ?>
                        </select>
                        </div>
                      </div>
                    </div>
                      <input type="hidden" id="text_content" name="PROPKANTOR" value="" />
                      <div class='form-group'>
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">KABUPATEN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->KABKANTOR; ?>">
                      </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                        <select class='form-control' name="KABUPATEN" id='kabupaten-kota' onchange="document.getElementById('text_content2').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                      </div>
                      <input type="hidden" id="text_content2" name="KABKANTOR" value="" />
                      <div class='form-group'>
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">KECAMATAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->KECKANTOR; ?>">
                      </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                        <select class='form-control' name="KECAMATAN" id='kecamatan' onchange="document.getElementById('text_content3').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                      </div>
                      <input type="hidden" id="text_content3"  name="KECKANTOR" value="" />
                      <div class='form-group'>
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">KELURAHAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->KELKANTOR; ?>">
                      </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                        <select class='form-control' name="KELURAHAN" id='kelurahan-desa'  onchange="document.getElementById('text_content4').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                      </div>
                      <input type="hidden" id="text_content4" name="KELKANTOR" value="" />
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">KODE POS
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->KODEPOS; ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="KODEPOS" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('KODEPOS'); ?>">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">NPWP
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NPWP; ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NPWP" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('NPWP'); ?>">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <button type="submit" class="btn btn-success">Save</button>
                      </div>
                    </div> 
                  </form>
                  <?php endforeach ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer>
          <div class="copyright-info">
            <p class="pull-right">SIMP3</a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
      </div>
      <script type="text/javascript">
    $(function(){

    $.ajaxSetup({
    type:"POST",
    url: "<?php echo base_url('regions') ?>",
    cache: false,
    });

    $("#provinsi").change(function(){

    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kabupaten',id:value},
    success: function(respond){
    $("#kabupaten-kota").html(respond);
    }
    })
    }

    });

    $("#kabupaten-kota").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kecamatan',id:value},
    success: function(respond){
    $("#kecamatan").html(respond);
    }
    })
    }
    })

    $("#kecamatan").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kelurahan',id:value},
    success: function(respond){
    $("#kelurahan-desa").html(respond);
    }
    })
    } 
    })
    })
      </script>
<?php $this->load->view('_parts/public_footer_view'); ?>