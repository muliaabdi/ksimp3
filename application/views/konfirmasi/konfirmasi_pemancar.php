<?php $this->load->view('_parts/public_header_view'); ?>

<div class="right_col" role="main">
        <div class="">

          <div class="page-title">
            <!-- <div class="title_left">
              <h3>KONFIRMASI CONTACT</h3>
            </div> -->
            <div class="title_right">
              <!-- <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
              </div> -->
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>KONFIRMASI PEMANCAR</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <!-- <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li> -->
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <form id="demo-form" data-parsley-validate class="form-horizontal form-label-left">
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">NAMA STASIUN PEMANCAR
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">ALAMAT PEMANCAR
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class='form-group'>
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">PROVINSI
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="">
                      </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                        <select class='form-control' id='provinsi' onchange="document.getElementById('text_content').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        <?php 
                        foreach ($provinsi as $prov) {
                        echo "<option value='$prov[id]'>$prov[name]</option>";
                        }
                        ?>
                        </select>
                        </div>
                      </div>
                    </div>
                      <input type="hidden" id="text_content" name="PROPKANTOR" value="" />
                      <div class='form-group'>
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">KABUPATEN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="">
                      </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                        <select class='form-control' id='kabupaten-kota' onchange="document.getElementById('text_content2').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                      </div>
                      <input type="hidden" id="text_content2" name="KABKANTOR" value="" />
                      <div class='form-group'>
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">KECAMATAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="">
                      </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                        <select class='form-control' id='kecamatan' onchange="document.getElementById('text_content3').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                      </div>
                      <input type="hidden" id="text_content3"  name="KECKANTOR" value="" />

                      <div class='form-group'>
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">KELURAHAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="">
                      </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                        <select class='form-control' id='kelurahan-desa'  onchange="document.getElementById('text_content4').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                      </div>
                      <input type="hidden" id="text_content4" name="KELKANTOR" value="" />
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">KODE POS PEMANCAR
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <button type="submit" class="btn btn-success">Save</button>
                        <button type="submit" class="btn btn-primary">Back</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
<script type="text/javascript">
    $(function(){

    $.ajaxSetup({
    type:"POST",
    url: "<?php echo base_url('pejabat/ambil_data') ?>",
    cache: false,
    });

    $("#provinsi").change(function(){

    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kabupaten',id:value},
    success: function(respond){
    $("#kabupaten-kota").html(respond);
    }
    })
    }

    });


    $("#kabupaten-kota").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kecamatan',id:value},
    success: function(respond){
    $("#kecamatan").html(respond);
    }
    })
    }
    })

    $("#kecamatan").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kelurahan',id:value},
    success: function(respond){
    $("#kelurahan-desa").html(respond);
    }
    })
    } 
    })

    })

</script>
<?php $this->load->view('_parts/public_footer_view'); ?>