<?php $this->load->view('_parts/public_header_view'); ?>
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Konfirmasi Kantor</h3>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Data Alamat Kantor</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <form action="#" id="form" class="form-horizontal">
                    <table class="table table-bordered table-striped table-hover" id="mytable">
                    <thead>
                            <tr>
                              <th>NO</th>
                              <th>COMPANY ID</th>
                              <th>LEMBAGA PENYIARAN</th>
                              <th>LEMBAGASIAR</th>
                              <th>ALAMATKANTOR</th>
                              <th>PROPKANTOR</th>
                              <th>KABKANTOR</th>
                              <th>KECKANTOR</th>
                              <th>KELKANTOR</th>
                              <th>KODEPOS</th>
                              <th>NPWP</th>
                              <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                        <!-- <?php $i=1; foreach ($data as $row): ?>
                            <tr>
                              <th><?php echo $i; ?></th>
                              <th><?php echo $row->IDPERUSAHAANFINAL ?></th>
                              <th><?php echo $row->IDPERUSAHAANFINAL ?></th>
                              <th><?php echo $row->LEMBAGASIAR ?></th>
                              <th><?php echo $row->ALAMATKANTOR ?></th>
                              <th><?php echo $row->PROPKANTOR ?></th>
                              <th><?php echo $row->KABKANTOR ?></th>
                              <th><?php echo $row->KECKANTOR ?></th>
                              <th><?php echo $row->KELKANTOR ?></th>
                              <th><?php echo $row->KODEPOS ?></th>
                              <th><?php echo $row->NPWP ?></th>
                              <th><a href="#">Edit</a></th>
                            </tr>
                        <?php $i++; endforeach ?> -->
                        </tbody>
                    </table>
                  </form>
                </div>
              </div>
            </div>
            <br />
            <br />
            <br />

          </div>
        </div>
        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">SIMP3</p>
          </div>
          <div class="clearfix"></div>
        </footer>
      </div>
<?php $this->load->view('_parts/public_footer_view'); ?>