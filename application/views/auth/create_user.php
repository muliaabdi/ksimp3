<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="icon" href="<?php echo base_url().'assets/public/themes/default/images/logo.png' ?>" sizes="16x16" type="image/png">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>SIMP3 Pemutakhiran</title>

  <!-- Bootstrap core CSS -->

  <link href="<?php echo base_url(); ?>assets/public/themes/default/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/public/themes/default/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/public/themes/default/css/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/public/themes/default/css/custom.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/public/themes/default/css/icheck/flat/green.css" rel="stylesheet">


  <script src="<?php echo base_url(); ?>assets/public/themes/default/js/jquery.min.js"></script>
  <!-- <script src="http://gregpike.net/demos/bootstrap-file-input/bootstrap.file-input.js"></script> -->

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body style="background:#F7F7F7;">

  <div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">
      <div id="login" class="animate form">
      <!-- <p><?php echo lang('login_subheading');?></p> -->
      <div class="row col-md-offset-1 login-logo">
        <div class="col-md-4">
        <img src="<?php echo base_url().'assets/public/themes/default/images/logo.png' ?>" class="img-responsive" width="100" height="100">
        </div>
        <div class="col-md-8">
          <h1>SIMP3</h1>
          <div class="subtitle">
            <p>Pemutakhiran Data</p>
          </div>
        </div>
      </div>
    <?php if(isset($message)) {?>
        <div class="alert alert-info alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></span>
            </button>
            <strong>Information!</strong> <?php echo $message;?>
        </div>
    <?php } ?>
 <section class="login_content">
          <?php echo form_open_multipart("auth/create_user", 'class="form-horizontal form-label-left"');?>
            <h1>Register</h1>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Perusahaan</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <?php echo form_input($nmperusahaan, '', 'class="form-control" placeholder="Nama Perusahaan"');?>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Username</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <?php echo form_input($identity, '', 'class="form-control" placeholder="Username"');?>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Lengkap</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <?php echo form_input($first_name, '', 'class="form-control" placeholder="Nama"');?>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nomor KTP</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <?php echo form_input($ktp, '', 'class="form-control" placeholder="KTP"');?>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nomor Telepon</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <?php echo form_input($phone, '', 'class="form-control" placeholder="Telp"');?>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nomor FAX</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <?php echo form_input($nofax, '', 'class="form-control" placeholder="Fax"');?>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nomor HP</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <?php echo form_input($nohp, '', 'class="form-control" placeholder="HP"');?>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <?php echo form_input($email, '', 'class="form-control" placeholder="Email"');?>
              </div>
            </div>
<!--             <div>
              <?php echo form_input($nmperusahaan, '', 'class="form-control" placeholder="Nama Perusahaan"');?>
            </div>
            <div>
              <?php echo form_input($identity, '', 'class="form-control" placeholder="Username"');?>
            </div>
            <div>
              <?php echo form_input($first_name, '', 'class="form-control" placeholder="Nama"');?>
            </div>
            <div>
              <?php echo form_input($ktp, '', 'class="form-control" placeholder="KTP"');?>
            </div>
            <div>
              <?php echo form_input($phone, '', 'class="form-control" placeholder="Telp"');?>
            </div>
            <div>
              <?php echo form_input($nofax, '', 'class="form-control" placeholder="Fax"');?>
            </div>
            <div>
              <?php echo form_input($nohp, '', 'class="form-control" placeholder="HP"');?>
            </div>
            <div>
              <?php echo form_input($email, '', 'class="form-control" placeholder="Email"');?>
            </div> -->
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Scan KTP</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="file" data-filename-placement="inside" name="scanktp" class="col-md-12 col-sm-12 col-xs-12 btn">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Scan NPWP</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="file" data-filename-placement="inside" name="scanakta" class="col-md-12 col-sm-12 col-xs-12 btn">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Scan Surat Perintah</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="file" data-filename-placement="inside" name="scanipp" class="col-md-12 col-sm-12 col-xs-12 btn">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Scan</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="file" data-filename-placement="inside" name="scansurat" class="col-md-12 col-sm-12 col-xs-12 btn">
              </div>
            </div>
            <div>
              <?php echo form_submit('submit', 'Register', 'class="submit btn btn-lg btn-success col-md-12"');?>
            </div>
            <div class="clearfix"></div>
            <div class="separator">

              <p class="change_link">Already a member ?
                <a href="<?php echo site_url('auth/login'); ?>" >Login ? </a>
              </p>
              <div class="clearfix"></div>
              <br />
<!--               <div>
                <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Gentelella Alela!</h1>

                <p>©2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
              </div> -->
            </div>
          <?php echo form_close();?>
          <!-- form -->
        </section>
        <!-- content -->
      </div>
    </div>
  </div>

</body>

</html>
<script type="text/javascript">
  // $('input[type=file]').bootstrapFileInput();
  // $('.file-inputs').bootstrapFileInput();
</script>

<!-- <h1><?php echo lang('create_user_heading');?></h1>
<p><?php echo lang('create_user_subheading');?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/create_user");?>

      <p>
            <?php echo lang('create_user_fname_label', 'first_name');?> <br />
            <?php echo form_input($first_name);?>
      </p>

      <p>
            <?php echo lang('create_user_lname_label', 'last_name');?> <br />
            <?php echo form_input($last_name);?>
      </p>
      
      <?php
      if($identity_column!=='email') {
          echo '<p>';
          echo lang('create_user_identity_label', 'identity');
          echo '<br />';
          echo form_error('identity');
          echo form_input($identity);
          echo '</p>';
      }
      ?>

      <p>
            <?php echo lang('create_user_company_label', 'company');?> <br />
            <?php echo form_input($company);?>
      </p>

      <p>
            <?php echo lang('create_user_email_label', 'email');?> <br />
            <?php echo form_input($email);?>
      </p>

      <p>
            <?php echo lang('create_user_phone_label', 'phone');?> <br />
            <?php echo form_input($phone);?>
      </p>

      <p>
            <?php echo lang('create_user_password_label', 'password');?> <br />
            <?php echo form_input($password);?>
      </p>

      <p>
            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php echo form_input($password_confirm);?>
      </p>


      <p><?php echo form_submit('submit', lang('create_user_submit_btn'));?></p>

<?php echo form_close();?>
 -->