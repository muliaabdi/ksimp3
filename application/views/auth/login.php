<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="icon" href="<?php echo base_url().'assets/public/themes/default/images/logo.png' ?>" sizes="16x16" type="image/png">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>SIMP3 Pemutakhiran</title>

  <!-- Bootstrap core CSS -->

  <link href="<?php echo base_url(); ?>assets/public/themes/default/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/public/themes/default/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/public/themes/default/css/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/public/themes/default/css/custom.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/public/themes/default/css/icheck/flat/green.css" rel="stylesheet">


  <script src="<?php echo base_url(); ?>assets/public/themes/default/js/jquery.min.js"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body style="background:#F7F7F7;">

  <div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">
      <div id="login" class="animate form">
      <!-- <p><?php echo lang('login_subheading');?></p> -->
      <div class="row col-md-offset-1 login-logo">
        <div class="col-md-4">
        <img src="<?php echo base_url().'assets/public/themes/default/images/logo.png' ?>" class="img-responsive" width="100" height="100">
        </div>
        <div class="col-md-8">
          <h1>SIMP3</h1>
          <div class="subtitle">
            <p>Pemutakhiran Data</p>
          </div>
        </div>
      </div>
    <?php if(isset($message)) {?>
        <div class="alert alert-info alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></span>
            </button>
            <strong>Information!</strong> <?php echo $message;?>
        </div>
    <?php } ?>
    <section class="login_content">
        <?php echo form_open("auth/login");?>
            <h1><?php echo lang('login_heading');?></h1>
            <div class="form-group">
                <?php echo form_input($identity, '', 'class="form-control" placeholder="Email/Username" required=""');?>
            </div>
            <div class="form-group">
                <?php echo form_input($password, '', 'class="form-control" placeholder="Password" required=""');?>
            </div>
            <!--<div class="checkbox">
                <input type="checkbox" class="" id="remember" value="1" name="remember" style="left:30px;">
                <p style="margin-left: -205px;">Remember Me</p>
            </div>-->
            <div>
                <?php echo form_submit('submit', lang('login_submit_btn'), 'class="submit btn btn-lg btn-success col-md-12"');?>
            </div>
            <div class="clearfix"></div>
            <span class="pull-right">
                   <a href="<?php echo site_url('register'); ?>" >Register ? </a>
            </span>
            <div class="separator">
                <div class="clearfix"></div>
                <br />
                <div>
                    <!-- <h1><br><br>Gentelella Alela!</h1>

                    <p>©2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p> -->
                    <p>Mohon mengisi login dan password yang sesuai.</p>
                </div>
            </div>
        <?php echo form_close();?>
        <!-- form -->
    </section>
        <!-- content -->
      </div>
      <div id="register" class="animate form">
        <section class="login_content">
          <form>
            <h1>Create Account</h1>
            <div>
              <input type="text" class="form-control" placeholder="Username" required="" />
            </div>
            <div>
              <input type="email" class="form-control" placeholder="Email" required="" />
            </div>
            <div>
              <input type="password" class="form-control" placeholder="Password" required="" />
            </div>
            <div>
              <a class="btn btn-default submit" href="index.html">Submit</a>
            </div>
            <div class="clearfix"></div>
            <div class="separator">

              <p class="change_link">Already a member ?
                <a href="#tologin" class="to_register"> Log in </a>
              </p>
              <div class="clearfix"></div>
              <br />
              <div>
                <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Gentelella Alela!</h1>

                <p>©2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
              </div>
            </div>
          </form>
          <!-- form -->
        </section>
        <!-- content -->
      </div>
    </div>
  </div>

</body>

</html>

<!-- <h1><?php echo lang('login_heading');?></h1>
<p><?php echo lang('login_subheading');?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/login");?>

  <p>
    <?php echo lang('login_identity_label', 'identity');?>
    <?php echo form_input($identity);?>
  </p>

  <p>
    <?php echo lang('login_password_label', 'password');?>
    <?php echo form_input($password);?>
  </p>

  <p>
    <?php echo lang('login_remember_label', 'remember');?>
    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
  </p>


  <p><?php echo form_submit('submit', lang('login_submit_btn'));?></p>

<?php echo form_close();?>

<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p> -->