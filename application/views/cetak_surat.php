<?php $this->load->view('_parts/public_header_view'); ?>

<div class="right_col" role="main">
        <div class="">

          <div class="page-title">
            <!-- <div class="title_left">
              <h3>KONFIRMASI CONTACT</h3>
            </div> -->
            <div class="title_right">
              <!-- <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
              </div> -->
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>PENCETAKAN SURAT KONFIRMASI</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <!-- <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li> -->
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p>
                    Pengguna mencetak Surat Konfirmasi Administrasi dan Kelengkapan Data Perusahaan dilengkapi Kop Perusahaan dan tanda tangan di atas meterai (Rp. 6000,-) melalui halaman PRINT SURAT KONFIRMASI dan PRINT KELENGKAPAN DATA untuk dikirimkan bersama salinan data pendukung (Salinan Dokumen NPWP dan Akta Perusahaan) kepada :
                    <br/><br/>
                    <b>
                      Direktur Penyiaran<br/>
                      Up. Kasubdit Pemetaan dan Database, Dit. Penyiaran, Ditjen Penyelanggaraan Pos dan Informatika, Kementerian Kominfo<br/>
                      Jl. Medan Merdeka Barat No.17<br/>
                      Jakarta 10110, Indonesia<br/>
                      Tlp. 021-3000 3100<br/>
                      Fax. 021-2957 6486<br/>
                    </b>
                    <br/><br/>
                    Sasaran akhir dari pekerjaan ini yaitu diperolehnya database penyelenggra penyiaran yang mutakhir, akurat dan valid sesuai dengan referensi peraturan-peraturan yang berlaku.
                    <br/>
                    Terima kasih atas kerja sama Anda.
                  </p>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>

<?php $this->load->view('_parts/public_footer_view'); ?>