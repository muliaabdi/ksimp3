<?php $this->load->view('_parts/public_header_view'); ?>
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Konfirmasi Penyelenggara</h3>
            </div>

            <!-- <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
              </div>
            </div> -->
          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Data Penyelenggara</h2>
                  <!-- <ul class="nav navbar-right panel_toolbox">
                    <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-close"></i></a>
                    </li>
                  </ul> -->
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <!-- <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                      <tr class="headings">
                        <th>ID COMPANY </th>
                        <th>COMPANY NAME </th>
                        <th>ID PEMOHON </th>
                        <th>ON AIR NAME </th>
                        <th>LEMBAGA SIAR </th>
                        <th>KEDUDUKAN STASIUN PENYIARAN </th>
                        <th class=" no-link last"><span class="nobr">Action</span>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="even pointer">
                        <td class="a-center "><?php echo $row->IDPERUSAHAANFINAL; ?></td>
                        <td class=" ">121000040</td>
                        <td class=" ">May 23, 2014 11:47:56 PM </td>
                        <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i>
                        </td>
                        <td class=" ">John Blank L</td>
                        <td class=" ">Paid</td>
                        <td class=" last"><a href="#">View</a>
                        </td>
                      </tr>
                    </tbody>
                  </table> -->
                    <table class="table table-bordered table-striped table-hover" id="mytable">
                        <thead>
                            <tr>
                              <th>NO</th>
                              <th>COMPANY ID</th>
                              <th>LEMBAGA PENYIARAN</th>
                              <th>IDPEMOHON</th>
                              <th>NMUDARA</th>
                              <th>LEMBAGASIAR</th>
                              <th>ACTION</th>
                            </tr>
                        </thead>
                    </table>
                </div>
              </div>
            </div>
            <br />
            <br />
            <br />

          </div>
        </div>
        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>

  <script type="text/javascript">
            $(document).ready(function () {
 
                $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };
 
                var t = $('#mytable').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "bSort" : false,
                    "oLanguage": {
                       "sSearch": "Search "
                     },
                    "ajax": "<?php echo base_url('pejabat/ajax_penyelenggara'); ?>",
                    "columns": [
                        {"data": "NO"},
                        {"data": "COMPANY ID"},
                        {"data": "LEMBAGA PENYIARAN"},
                        {"data": "IDPEMOHON"},
                        {"data": "NMUDARA"},
                        {"data": "LEMBAGASIAR"},
                        {
                            "class": "text-center",
                            "data": "ACTION"
                        }
                    ],
                    "order": [[1, 'asc']],
                    "rowCallback": function (row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>
<?php $this->load->view('_parts/public_footer_view'); ?>