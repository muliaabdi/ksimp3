<?php $this->load->view('_parts/public_header_view'); ?>

<div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_right">
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>PENYELENGGARA PENYIARAN</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <form id="demo-form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo base_url().'pejabat/savekronologis' ?>">
                  <?php echo validation_errors(); ?>
                  <?php foreach ($data as $row): ?>
                    <input type="hidden" name="NO" value="<?php echo $row->NO; ?>">
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">COMPANY ID
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->IDPERUSAHAAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="IDPERUSAHAAN" required="required" class="form-control col-md-7 col-xs-12" readonly="readonly" value="<?php echo $row->IDPERUSAHAAN ?>">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">COMPANY NAME
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->LEMBAGAPENYIARAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" required="required" name="LEMBAGAPENYIARAN" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">NO. BERITA ACARA FRB
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NOBERITAACARAFRB ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NOBERITAACARAFRB" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">TANGGAL FRB
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->TANGGALFRB ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="TANGGALFRB" required="required" class="datepicker form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">NOMOR IZIN PRINSIP
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NOMORIZINPRINSIP ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NOMORIZINPRINSIP" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">TANGGAL IZIN PRINSIP
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->TANGGALIZINPRINSIP ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="TANGGALIZINPRINSIP" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">MASA BERLAKU IPP PRINSIP
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->MASABERLAKUIPPPRINSIP ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="MASABERLAKUIPPPRINSIP" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">IPP PRINSIP STATUS
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->IPPPRINSIPSTATUS ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="IPPPRINSIPSTATUS" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">NOMOR IZIN PRINSIP PERPANJANGAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NOMORIZINPRINSIPPERPANJANGAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NOMORIZINPRINSIPPERPANJANGAN" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">TANGGAL IZIN PRINSIP PERPANJANGAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->TANGGALIZINPRINSIPPERPANJANGAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="TANGGALIZINPRINSIPPERPANJANGAN" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">MASA BERLAKU IPP PRINSIP PERPANJANGAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->MASABERLAKUIPPPRINSIPPERPANJANGAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="MASABERLAKUIPPPRINSIPPERPANJANGAN" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">IPP PRINSIP PERPANJANGAN STATUS
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->TANGGALFRB ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="TANGGALFRB" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">STATUS VERIFIKASI
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->STATUSVERIFIKASI ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="STATUSVERIFIKASI" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">NO BERITA ACARA EUCS
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NOBERITAACARAEUCS ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NOBERITAACARAEUCS" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">TANGGAL EUCS
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->TANGGALEUCS ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="TANGGALEUCS" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">NO SURAT PERSETUJUAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NOSURATPERSETUJUAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NOSURATPERSETUJUAN" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">PERUBAHAN DATA SEBELUM
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->PERUBAHANDATASEBELUM ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="PERUBAHANDATASEBELUM" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">PERUBAHAN DATA SESUDAH
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->PERUBAHANDATASESUDAH ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="PERUBAHANDATASESUDAH" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">NOMOR IPP TETAP
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NOMORIPPTETAP ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NOMORIPPTETAP" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">TANGGAL IPP TETAP
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->TANGGALIPPTETAP ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="TANGGALIPPTETAP" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">MASA BERLAKU IPP TETAP
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->MASABERLAKUIPPTETAP ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="MASABERLAKUIPPTETAP" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">IPP TETAP STATUS
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NOSURATPERSETUJUAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NOSURATPERSETUJUAN" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">NO BERITA ACARA FRB IPP PERPANJANGAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NOBERITAACARAFRBIPPER ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NOBERITAACARAFRBIPPER" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">TANGGAL FRB IPP PERPANJANGAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->TANGGALFRBIPPER ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="TANGGALFRBIPPER" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">NOMOR IPP PERPANJANGAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NOMORIPPPERPANJANGAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NOMORIPPPERPANJANGAN" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">MASA BERLAKU IPP PERPANJANGAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->MASABERLAKUIPPPERPANJANGAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="MASABERLAKUIPPPERPANJANGAN" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">IPP TETAP PERPANJANGAN STATUS
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->MASABERLAKUIPPPERPANJANGAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="MASABERLAKUIPPPERPANJANGAN" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">NO BERITA ACARA FRB PENYESUAIAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NOBERITAACARAFRBIPPPENY ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NOBERITAACARAFRBIPPPENY" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">TANGGAL FRB PENYESUAIAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->TANGGALFRBIPPPENY ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="TANGGALFRBIPPPENY" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">NOMOR IPP PENYESUAIAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NOMORIPPPENYESUAIAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NOMORIPPPENYESUAIAN" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">TANGGAL IPP PENYESUAIAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->TANGGALIPPPENYESUAIAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="TANGGALIPPPENYESUAIAN" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">MASA BERLAKUIPPPENYESUAIAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->MASABERLAKUIPPPENYESUAIAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="MASABERLAKUIPPPENYESUAIAN" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">IPP PENYESUAIAN STATUS
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->IPPPENYESUAIANSTATUS ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="IPPPENYESUAIANSTATUS" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">NO BERITA ACARA FRB IPP MIGRASI
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NOBERITAACARAFRBIPPMIG ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NOBERITAACARAFRBIPPMIG" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">TANGGAL FRB IPP MIGRASI
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->TANGGALFRBIPPMIG ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="TANGGALFRBIPPMIG" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">NOMOR IPP MIGRASI (AM/FM)
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NOMORIPPMIGRASIAMFM ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NOMORIPPMIGRASIAMFM" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">TANGGAL IPP MIGRASI
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->TANGGALIPPMIGRASI ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="TANGGALIPPMIGRASI" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">MASA BERLAKU IPP MIGRASI
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->MASABERLAKUIPPMIGRASI ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="MASABERLAKUIPPMIGRASI" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">IPP MIGRASI STATUS
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->IPPMIGRASISTATUS ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="IPPMIGRASISTATUS" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">STATUS AKHIR
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->STATUSAKHIR ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="STATUSAKHIR" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">KETERANGAN
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->KETERANGAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="KETERANGAN" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <button type="submit" class="btn btn-success">Save</button>
                        <a href="javascript:history.go(-1)" class="btn btn-primary">Back</a>
                      </div>
                    </div>
                    <?php endforeach ?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer>
          <div class="copyright-info">
            <p class="pull-right">SIMP3</p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
<script type="text/javascript">
 $(document).ready(function(){
    $('.datepicker').datepicker({dateFormat: 'dd-mm-yy'});
  });
</script>
<?php $this->load->view('_parts/public_footer_view'); ?>