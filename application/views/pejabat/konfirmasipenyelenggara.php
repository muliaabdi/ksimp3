<?php $this->load->view('_parts/public_header_view'); ?>

<div class="right_col" role="main">
        <div class="">

          <div class="page-title">
            <!-- <div class="title_left">
              <h3>KONFIRMASI CONTACT</h3>
            </div> -->
            <div class="title_right">
              <!-- <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
              </div> -->
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>PENYELENGGARA PENYIARAN</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <!-- <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li> -->
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <form id="demo-form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo base_url().'pejabat/save' ?>">
                  <?php echo validation_errors(); ?>
                  <?php foreach ($data as $row): ?>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">COMPANY ID
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->IDPERUSAHAANFINAL ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="IDPERUSAHAANFINAL" required="required" class="form-control col-md-7 col-xs-12" readonly="readonly" value="<?php echo $row->IDPERUSAHAANFINAL ?>">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">COMPANY NAME
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NMPERUSAHAAN ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" required="required" name="NMPERUSAHAAN" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">ID PEMOHON
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->IDPEMOHON ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="IDPEMOHON" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">ON AIR NAME
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $row->NMUDARA ?>">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" name="NMUDARA" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">LEMBAGA SIAR
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <select id="selectbasic" name="selectbasic" class="form-control" disabled>
                          <option><?php echo $row->LEMBAGASIAR ?></option>
                        </select>
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <select id="lembagasiar" name="LEMBAGASIAR" class="form-control" required="required">
                          <option>LPB TELEVISI</option>
                          <option>LPK RADIO</option>
                          <option>LPK TELEVISI</option>
                          <option>LPP LOKAL RADIO</option>
                          <option>LPP LOKAL TELEVISI</option>
                          <option>LPS RADIO</option>
                          <option>LPS TELEVISI</option>
                        </select>
                      </div>
                      </div>
                    </div>
                    <!-- <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">STATUS IPP TERAKHIR
                      </label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <select id="selectbasic" name="selectbasic" class="form-control col-md-7 col-xs-12">
                          <option>PEMOHON</option>
                          <option>PENYESUAIAN</option>
                          <option>PERPANJANG</option>
                          <option>PRINSIP</option>
                          <option>TETAP</option>
                        </select>
                      </div>
                    </div> -->
                    <!-- <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">AREA SERVICE
                      </label>
                      <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12">
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                      </div>
                    </div> -->
                    <!-- <br />
                    <div class="x_title">
                      <h2>TEKNIS</h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-3 col-xs-12" for="first-name">SUB SERVICE
                      </label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <select id="selectbasic" name="selectbasic" class="form-control col-md-7 col-xs-12">
                          <option>AM</option>
                          <option>FM</option>
                          <option>TV</option>
                          <option>DAB+</option>
                          <option>DVB-T2</option>
                        </select>
                      </div>
                    </div> -->
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <button type="submit" class="btn btn-success">Save</button>
                        <a href="javascript:history.go(-1)" class="btn btn-primary">Back</a>
                      </div>
                    </div>
                    <?php endforeach ?>
                  </form>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">SIMP3</p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
<script type="text/javascript">
  document.getElementById("lembagasiar").selectedIndex = -1;
</script>
<?php $this->load->view('_parts/public_footer_view'); ?>