<?php $this->load->view('_parts/public_header_view'); ?>
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Konfirmasi Kantor</h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Data Alamat Kantor</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <!-- <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                      <tr class="headings">
                        <th>ID COMPANY </th>
                        <th>COMPANY NAME </th>
                        <th>ID PEMOHON </th>
                        <th>ON AIR NAME </th>
                        <th>LEMBAGA SIAR </th>
                        <th>KEDUDUKAN STASIUN PENYIARAN </th>
                        <th class=" no-link last"><span class="nobr">Action</span>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="even pointer">
                        <td class="a-center "><?php echo $row->IDPERUSAHAANFINAL; ?></td>
                        <td class=" ">121000040</td>
                        <td class=" ">May 23, 2014 11:47:56 PM </td>
                        <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i>
                        </td>
                        <td class=" ">John Blank L</td>
                        <td class=" ">Paid</td>
                        <td class=" last"><a href="#">View</a>
                        </td>
                      </tr>
                    </tbody>
                  </table> -->
                  <form action="#" id="form" class="form-horizontal">
                    <table class="table table-bordered table-striped table-hover" id="mytable">
                        <thead>
                            <tr>
                              <td>NO</td>
                              <td>COMPANY ID</td>
                              <td>LEMBAGA PENYIARAN</td>
                              <td>LEMBAGASIAR</td>
                              <td>NOBERITAACARAFRB</td>
                              <td>TANGGALFRB</td>
                              <td>NOMORIZINPRINSIP</td>
                              <td>TANGGALIZINPRINSIP</td>
                              <td>MASABERLAKUIPPPRINSIP</td>
                              <td>IPPPRINSIPSTATUS</td>
                              <td>NOMORIZINPRINSIPPERPANJANGAN</td>
                              <td>TANGGALIZINPRINSIPPERPANJANGAN</td>
                              <td>MASABERLAKUIPPPRINSIPPERPANJANGAN</td>
                              <td>IPPPRINSIPPERPANJANGANSTATUS</td>
                              <td>STATUSVERIFIKASI</td>
                              <!-- <td>NOBERITAACARAEUCS</td>
                              <td>TANGGALEUCS</td>
                              <td>NOSURATPERSETUJUAN</td>
                              <td>PERUBAHANDATASEBELUM</td>
                              <td>PERUBAHANDATASESUDAH</td> -->
                              <td>NOMORIPPTETAP</td>
                              <td>TANGGALIPPTETAP</td>
                              <td>MASABERLAKUIPPTETAP</td>
                              <td>IPPTETAPSTATUS</td>
                              <td>NOBERITAACARAFRBIPPER</td>
                              <td>TANGGALFRBIPPER</td>
                              <td>NOMORIPPPERPANJANGAN</td>
                              <td>TANGGALIPPPERPANJANGAN</td>
                              <td>MASABERLAKUIPPPERPANJANGAN</td>
                              <td>IPPTETAPPERPANJANGANSTATUS</td>
                              <td>NOBERITAACARAFRBIPPPENY</td>
                              <td>TANGGALFRBIPPPENY</td>
                              <td>NOMORIPPPENYESUAIAN</td>
                              <td>TANGGALIPPPENYESUAIAN</td>
                              <td>MASABERLAKUIPPPENYESUAIAN</td>
                              <td>IPPPENYESUAIANSTATUS</td>
                              <td>NOBERITAACARAFRBIPPMIG</td>
                              <td>TANGGALFRBIPPMIG</td>
                              <td>NOMORIPPMIGRASI(AM-FM)</td><!--
                              <td>TANGGALIPPMIGRASI</td> 
                              <td>MASABERLAKUIPPMIGRASI</td>
                              <td>IPPMIGRASISTATUS</td>
                              <td>STATUSAKHIR</td>-->
                              <!-- <td>KETERANGAN</td> -->
                              <th>ACTION</th>
                            </tr>
                        </thead>
                    </table>
                  </form>
                </div>
              </div>
            </div>
            <br />
            <br />
            <br />

          </div>
        </div>
        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>

  <script type="text/javascript">
            $(document).ready(function () {
 
                $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };
 
                var t = $('#mytable').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "scrollX": true,
                    "bSort" : false,
                    // "oLanguage": {
                    //   "sProcessing": "<img src='<?php echo base_url() ?>assets/public/themes/default/images/FlipFlop.gif'>"
                    // },
                    "ajax": "<?php echo base_url('pejabat/ajax_kronologis'); ?>",
                    "dataType": 'json',
                    "type": "GET",
                    "columns": [
                        {"data": "NO"},
                        {"data": "COMPANY ID"},
                        {"data": "LEMBAGA PENYIARAN"},
                        {"data": "LEMBAGASIAR"},
                        {"data": "NOBERITAACARAFRB"},
                        {"data": "TANGGALFRB"},
                        {"data": "NOMORIZINPRINSIP"},
                        {"data": "TANGGALIZINPRINSIP"},
                        {"data": "MASABERLAKUIPPPRINSIP"},
                        {"data": "IPPPRINSIPSTATUS"},
                        {"data": "NOMORIZINPRINSIPPERPANJANGAN"},
                        {"data": "TANGGALIZINPRINSIPPERPANJANGAN"},
                        {"data": "MASABERLAKUIPPPRINSIPPERPANJANGAN"},
                        {"data": "IPPPRINSIPPERPANJANGANSTATUS"},
                        {"data": "STATUSVERIFIKASI"},
                        // {"data": "NOBERITAACARAEUCS"},
                        // {"data": "TANGGALEUCS"},
                        // {"data": "NOSURATPERSETUJUAN"},
                        // {"data": "PERUBAHANDATASEBELUM"},
                        // {"data": "PERUBAHANDATASESUDAH"},
                        {"data": "NOMORIPPTETAP"},
                        {"data": "TANGGALIPPTETAP"},
                        {"data": "MASABERLAKUIPPTETAP"},
                        {"data": "IPPTETAPSTATUS"},
                        {"data": "NOBERITAACARAFRBIPPER"},
                        {"data": "TANGGALFRBIPPER"},
                        {"data": "NOMORIPPPERPANJANGAN"},
                        {"data": "TANGGALIPPPERPANJANGAN"},
                        {"data": "MASABERLAKUIPPPERPANJANGAN"},
                        {"data": "IPPTETAPPERPANJANGANSTATUS"},
                        {"data": "NOBERITAACARAFRBIPPPENY"},
                        {"data": "TANGGALFRBIPPPENY"},
                        {"data": "NOMORIPPPENYESUAIAN"},
                        {"data": "TANGGALIPPPENYESUAIAN"},
                        {"data": "MASABERLAKUIPPPENYESUAIAN"},
                        {"data": "IPPPENYESUAIANSTATUS"},
                        {"data": "NOBERITAACARAFRBIPPMIG"},
                        {"data": "TANGGALFRBIPPMIG"},
                        {"data": "NOMORIPPMIGRASI(AM-FM)"},
                        //{"data": "TANGGALIPPMIGRASI"},
                        // {"data": "MASABERLAKUIPPMIGRASI"},
                        // {"data": "IPPMIGRASISTATUS"},
                        // {"data": "STATUSAKHIR"},
                        // {"data": "KETERANGAN"},
                        {
                            "class": "text-center",
                            "data": "ACTION"
                        }
                    ],
                    "order": [[1, 'asc']],
                    "rowCallback": function (row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
            // function update(NO){
            //     // $('#btnSave').text('saving...'); //change button text
            //     // $('#btnSave').attr('disabled',true); //set button disable
             
            //     // ajax adding data to database
            //     $.ajax({
            //         url : "<?php echo base_url('pejabat/ajax_update')?>/" + NO,
            //         type: "POST",
            //         data: $('#form').serialize(),
            //         dataType: "JSON",
            //         success: function()
            //         {
            //             $('#DELETED').attr('checked',true); //set button enable
            //         },
            //         error: function (jqXHR, textStatus, errorThrown)
            //         {
            //             alert('Error adding / update data');
            //         }
            //     });
            // }
        </script>
<?php $this->load->view('_parts/public_footer_view'); ?>