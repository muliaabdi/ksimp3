<div class="form-group border-form">
	<label>Pengesahaan akta pendirian/  badan hukum dari instansi yang berwenang</label>
	<div class="form-group">
		<label class="control-label">No</label>
		<input type="text" name="NOAKTA_PAP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tanggal</label>
		<input type="date" name="TANGGALPENGESAHAN_PAP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Nama instansi yang menerbitkan</label>
		<input type="text" name="NAMAINSTANSI_PAP" class="form-control">
	</div>
</div>