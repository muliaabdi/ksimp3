<div class="form-group border-form">
	<label>Surat Izin Tempat Usaha (SITU) *)</label>
	<div class="form-group">
		<label class="control-label">No</label>
		<input type="text" name="SITU_NO" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tanggal</label>
		<input type="date" name="SITU_TGL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Nama instansi yang menerbitkan</label>
		<input type="text" name="SITU_NM_INSTANSI" class="form-control">
	</div>
</div>