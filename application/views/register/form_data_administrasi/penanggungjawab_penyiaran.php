<label>3.  Penanggung Jawab Penyelenggara Penyiaran</label>
<div class="form-group border-form">
	<label>Penanggung Jawab Bidang Pemberitaan</label>
	<div class="form-group">
		<label class="control-label">Nama</label>
		<input type="text" name="PJBP_NAMA" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tempat/Tanggal Lahir</label>
		<input type="date" name="PJBP_TTL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Kewarganegaraan</label>
		<input type="text" name="PJBP_KEWARGANEGARAAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Pendidikan</label>
		<input type="text" name="PJBP_PENDIDIKAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Telepon Kantor</label>
		<input type="text" name="PJPP_TELPKANTOR" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">No HP</label>
		<input type="text" name="PJBP_NOHP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Fax</label>
		<input type="text" name="PJBP_FAX" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Email</label>
		<input type="text" name="PJBP_EMAIL" class="form-control">
	</div>
</div>
<div class="form-group border-form">
	<label>Penanggung Jawab Bidang Siaran</label>
	<div class="form-group">
		<label class="control-label">Nama</label>
		<input type="text" name="PJBS_NAMA" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tempat/Tanggal Lahir</label>
		<input type="date" name="PJBS_TTL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Kewarganegaraan</label>
		<input type="text" name="PJBS_KEWARGANEGARAAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Pendidikan</label>
		<input type="text" name="PJBS_PENDIDIKAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Telepon Kantor</label>
		<input type="text" name="PJBS_TELPKANTOR" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">No HP</label>
		<input type="text" name="PJBS_NOHP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Fax</label>
		<input type="text" name="PJBS_FAX" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Email</label>
		<input type="text" name="PJBS_EMAIL" class="form-control">
	</div>
</div>
<div class="form-group border-form">
	<label>Penanggung Jawab Bidang Teknik</label>
	<div class="form-group">
		<label class="control-label">Nama</label>
		<input type="text" name="PJBT_NAMA" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tempat/Tanggal Lahir</label>
		<input type="date" name="PJBT_TTL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Kewarganegaraan</label>
		<input type="text" name="PJBT_KEWARGANEGARAAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Pendidikan</label>
		<input type="text" name="PJBT_PENDIDIKAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Telepon Kantor</label>
		<input type="text" name="PJBT_TELPKANTOR" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">No HP</label>
		<input type="text" name="PJBT_NOHP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Fax</label>
		<input type="text" name="PJBT_FAX" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Email</label>
		<input type="text" name="PJBT_EMAIL" class="form-control">
	</div>
</div>
<div class="form-group border-form">
	<label>Penanggung Jawab Bidang Keuangan</label>
	<div class="form-group">
		<label class="control-label">Nama</label>
		<input type="text" name="PJBK_NAMA" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tempat/Tanggal Lahir</label>
		<input type="date" name="PJBK_TTL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Kewarganegaraan</label>
		<input type="text" name="PJBK_KEWARGANEGARAAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Pendidikan</label>
		<input type="text" name="PJBK_PENDIDIKAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Telepon Kantor</label>
		<input type="text" name="PJBK_TELPKANTOR" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">No HP</label>
		<input type="text" name="PJBK_NOHP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Fax</label>
		<input type="text" name="PJBK_FAK" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Email</label>
		<input type="text" name="PJBK_EMAIL" class="form-control">
	</div>
</div>
<div class="form-group border-form">
	<label>Penanggung Jawab Bidang Usaha</label>
	<div class="form-group">
		<label class="control-label">Nama</label>
		<input type="text" name="PJBU_NAMA" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tempat/Tanggal Lahir</label>
		<input type="date" name="PJBI_TTL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Kewarganegaraan</label>
		<input type="text" name="PJBU_KEWARGANEGARAAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Pendidikan</label>
		<input type="text" name="PJBU_PENDIDIKAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Telepon Kantor</label>
		<input type="text" name="PJBU_TELPKANTOR" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">No HP</label>
		<input type="text" name="PJBU_NOHP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Fax</label>
		<input type="text" name="PJBU_FAX" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Email</label>
		<input type="text" name="PJBU_EMAIL" class="form-control">
	</div>
</div>