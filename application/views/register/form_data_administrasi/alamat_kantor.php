<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/jquery.min.js') ?>"></script>
<script type="text/javascript">

	$(function(){

		$.ajaxSetup({
			type:"POST",
			url: "<?php echo base_url('register/alamat_kantor') ?>",
			cache: false,
		});

		$("#provinsi").change(function(){

			var value=$(this).val();
			if(value>0){
				$.ajax({
					data:{modul:'kabupaten',id:value},
					success: function(respond){
						$("#kabupaten-kota").html(respond);
					}
				})
			}

		});


		$("#kabupaten-kota").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
					data:{modul:'kecamatan',id:value},
					success: function(respond){
						$("#kecamatan").html(respond);
					}
				})
			}
		})

		$("#kecamatan").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
					data:{modul:'kelurahan',id:value},
					success: function(respond){
						$("#kelurahan-desa").html(respond);
					}
				})
			} 
		})

	})

</script>
<div class="form-group border-form">
	<label >Alamat Kantor</label>
	<div class="form-group border-form">
		<label class="control-label">Jalan</label>
		<textarea required="required" name="ALAMATKANTOR" class="form-control" placeholder="Masukan Alamat Perusahaan" ng-model="alamat"></textarea>
	</div>
	<div class="form-group">
		<label class="control-label">Provinsi</label>
		<select class="form-control" id="provinsi" name="PROPKANTOR">
			<option value="0">-Pilih Provinsi-</option>
			<?php 
			foreach ($provinsi as $prov) {
			echo "<option value='$prov[id]'>$prov[name]</option>";
			}
			?>
		</select>
	</div>
	<div class="form-group">
		<label class="control-label">Kota</label>
		<select class="form-control" id="kabupaten-kota" name="KABKANTOR">
			<option value="0">-Pilih Kota-</option>
		</select>
	</div>
	<div class="form-group">
		<label class="control-label">Kecamatan</label>
		<select class="form-control" id="kecamatan" name="KECKANTOR">
			<option value="0">-Pilih Kecamatan-</option>
		</select>
	</div>
	<div class="form-group">
		<label class="control-label">Kelurahan</label>
		<select class="form-control" id="kelurahan-desa" name="KELKANTOR">
			<option value="0">-Pilih Kelurahan-</option>
		</select>
	</div>
	<div class="form-group">
		<label class="control-label">Kode Pos</label>
		<input type="text" name="KODEPOSKANTOR" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Nomor Telepon</label>
		<input type="text" name="NOTELPKANTOR" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Email</label>
		<input type="text" name="NOHPKANTOR" class="form-control">
	</div> 
	<div class="form-group">
		<label class="control-label">Website</label>
		<input type="text" name="WEBSITE" class="form-control">
	</div>
</div>