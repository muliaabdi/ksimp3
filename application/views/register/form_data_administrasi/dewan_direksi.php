<label>1. Dewan Direksi</label>
<div class="form-group border-form">
	<label>Direktur Utama(Selaku Penanggung Jawab Umum)</label>
	<div class="form-group">
		<label class="control-label">Nama</label>
		<input type="text" name="DU_NAMA" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tempat/Tanggal Lahir</label>
		<input type="date" name="DU_TTL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Kewarganegaraan</label>
		<input type="text" name="DU_KEWARGANEGARAAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Pendidikan</label>
		<input type="text" name="DU_PENDIDIKAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Telepon Kantor</label>
		<input type="text" name="DU_TELP_KANTOR" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">No HP</label>
		<input type="text" name="DU_NOHP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Fax</label>
		<input type="text" name="DU_FAX" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Email</label>
		<input type="text" name="DU_EMAIL" class="form-control">
	</div>
</div>
<div class="form-group border-form">
	<label>Direktur…(Diisi sesuai nomenklatur)</label>
	<div class="form-group">
		<label class="control-label">Nama</label>
		<input type="text" name="D_NAMA" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tempat/Tanggal Lahir</label>
		<input type="date" name="D_TTL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Kewarganegaraan</label>
		<input type="text" name="D_KEWARGANEGARAAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Pendidikan</label>
		<input type="text" name="D_PENDIDIKAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Telepon Kantor</label>
		<input type="text" name="D_TELP_KANTOR" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">No HP</label>
		<input type="text" name="D_NOHP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Fax</label>
		<input type="text" name="D_FAX" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Email</label>
		<input type="text" name="D_EMAIL" class="form-control">
	</div>
</div>