<div class="form-group border-form">
	<label>Contact Person</label>
	<div class="form-group">
		<label class="control-label">Nama</label>
		<input type="text" name="NAMA_CP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Nomor Telepon</label>
		<input type="text" name="NOTELP_CP" class="form-control" ng-model="telp">
	</div>
	<div class="form-group">
		<label class="control-label">No Fax</label>
		<input type="text" name="NOFAX_CP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Nomor Hp</label>
		<input type="text" name="NOHP_CP" class="form-control" ng-model="contact">
	</div>
	<div class="form-group">
		<label class="control-label">Email</label>
		<input type="text" name="EMAIL_CP" class="form-control" ng-model="email">
	</div>
</div>