<legend>B. Aspek Permodalan *) (berasal dari kontribusi komunitasnya dan sumber lain yang sah dan tidak mengikat)</legend>
<div class="form-group border-form">
	<div class="form-group">
		<label class="control-label">Simpanan Pokok</label>
		<input type="text" name="SIMPANANPOKOK" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Simpanan Wajib</label>
		<input type="date" name="SIMPANANWAJIB" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Simpanan Sukarela</label>
		<input type="text" name="SIMPANANSUKARELA" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Sumber-sumber lain</label>
		<br>
		<label>a. Sumbangan</label>
		<input type="text" name="SUMBANGAN" class="form-control">
		<label>b. Hibah</label>
		<input type="text" name="HIBAH" class="form-control">
		<label>c. Sponsor</label>
		<input type="text" name="SPONSOR" class="form-control">
		<label>d. Lainnya(Sebutkan)</label>
		<input type="text" name="LAINNYA_AP" class="form-control">
		<input type="text" name="SEBUTKAN_AP" class="form-control" placeholder="Sebutkan">
	</div>
	<div class="form-group">
		<label class="control-label">Jumlah anggota komunitas</label>
		<input type="text" name="JMLANGGOTA" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Jumlah Modal Awal Keseluruhan</label>
		<input type="text" name="JMLMODALAWAL" class="form-control">
	</div>
</div>