<script type="text/javascript">

	$(function(){

		$.ajaxSetup({
			type:"POST",
			url: "<?php echo base_url('register/alamat_kantor') ?>",
			cache: false,
		});

		$("#provinsi-studio").change(function(){

			var value=$(this).val();
			if(value>0){
				$.ajax({
					data:{modul:'kabupaten',id:value},
					success: function(respond){
						$("#kabupaten-kota-studio").html(respond);
					}
				})
			}

		});


		$("#kabupaten-kota-studio").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
					data:{modul:'kecamatan',id:value},
					success: function(respond){
						$("#kecamatan-studio").html(respond);
					}
				})
			}
		})

		$("#kecamatan-studio").change(function(){
			var value=$(this).val();
			if(value>0){
				$.ajax({
					data:{modul:'kelurahan',id:value},
					success: function(respond){
						$("#kelurahan-desa-studio").html(respond);
					}
				})
			} 
		})

	})

</script>
<div class="form-group border-form">
	<label >Alamat Studio</label>
	<div class="form-group border-form">
		<label class="control-label">Jalan</label>
		<textarea required="required" class="form-control" placeholder="Masukan Alamat Perusahaan" name="ALAMATSTUDIO"></textarea>
	</div>
	<div class="form-group">
		<label class="control-label">Provinsi</label>
		<select class="form-control" id="provinsi-studio" name="PROPSTUDIO">
			<option value="0">-Pilih Provinsi-</option>
			<?php 
			foreach ($provinsi as $prov) {
			echo "<option value='$prov[id]'>$prov[name]</option>";
			}
			?>
		</select>
	</div>
	<div class="form-group">
		<label class="control-label">Kota</label>
		<select class="form-control" id="kabupaten-kota-studio" name="KABSTUDIO">
			<option value="0">-Pilih Kota-</option>
		</select>
	</div>
	<div class="form-group">
		<label class="control-label">Kecamatan</label>
		<select class="form-control" id="kecamatan-studio" name="KECSTUDIO">
			<option value="0">-Pilih Kecamatan-</option>
		</select>
	</div>
	<div class="form-group">
		<label class="control-label">Kelurahan</label>
		<select class="form-control" id="kelurahan-desa-studio" name="KELSTUDIO">
			<option value="0">-Pilih Kelurahan-</option>
		</select>
	</div>
	<div class="form-group">
		<label class="control-label">Kode Pos</label>
		<input type="text" name="KODEPOSSTUDIO" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Nomor Telepon</label>
		<input type="text" name="NOTELPSTUDIO" class="form-control">
	</div>
</div>