<label>1. Susunan Pendiri dan Anggota Penggurus</label>
<div class="form-group border-form">
	<label>Ketua Pendiri(Selaku Penanggung Jawab Umum)</label>
	<div class="form-group">
		<label class="control-label">Nama</label>
		<input type="text" name="KP_NAMA" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tempat/Tanggal Lahir</label>
		<input type="date" name="KP_TTL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Kewarganegaraan</label>
		<input type="text" name="KP_KEWARGANEGARAAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Pendidikan</label>
		<input type="text" name="KP_PENDIDIKAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Telepon Kantor</label>
		<input type="text" name="KP_TELP_KANTOR" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">No HP</label>
		<input type="text" name="KP_NOHP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Fax</label>
		<input type="text" name="KP_FAX" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Email</label>
		<input type="text" name="KP_EMAIL" class="form-control">
	</div>
</div>
<div class="form-group border-form">
	<label>Direktur…(Diisi sesuai nomenklatur)</label>
	<div class="form-group">
		<label class="control-label">Nama</label>
		<input type="text" name="D_NAMA" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tempat/Tanggal Lahir</label>
		<input type="date" name="D_TTL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Kewarganegaraan</label>
		<input type="text" name="D_KEWARGANEGARAAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Pendidikan</label>
		<input type="text" name="D_PENDIDIKAN" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Telepon Kantor</label>
		<input type="text" name="D_TELP_KANTOR" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">No HP</label>
		<input type="text" name="D_NOHP" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Fax</label>
		<input type="text" name="D_FAX" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Email</label>
		<input type="text" name="D_EMAIL" class="form-control">
	</div>
</div>