<div class="form-group border-form">
	<label>Izin Gangguan (HO) *)</label>
	<div class="form-group">
		<label class="control-label">No</label>
		<input type="text" name="IG_NO" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Tanggal</label>
		<input type="date" name="IG_TGL" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">Nama instansi yang menerbitkan</label>
		<input type="text" name="IG_NM_INSTANSI_PENERBIT" class="form-control">
	</div>
</div>