<div class="form-group border-form col-md-12">
	<label>a. Kelompok usia(dalam tahun)</label>
	<br><br>
	<div class="form-group col-md-4">
		<label class="control-label">Hasil Survey
		</label>
		<input type="radio" name="JENISSURVEY_KU" value="1" class="rb">
		<label class="control-label">Estimasi Manajemen
		</label>
		<input type="radio" name="JENISSURVEY_KU" value="2" class="rb" style="margin-bottom:235px;">
		<label>Jumlah</label>
	</div>
	<div class="form-group col-md-8">
		<div class="col-md-6">
			<input type="text" name="" class="form-control" value="Dibawah 15 Tahun" disabled="disabled">
			<input type="text" name="" class="form-control" value="15 s/d 19 tahun" disabled="disabled">
			<input type="text" name="" class="form-control" value="20 s.d 24 tahun" disabled="disabled">
			<input type="text" name="" class="form-control" value="25 s/d 29 tahun" disabled="disabled">
			<input type="text" name="" class="form-control" value="30 s/d 34 tahun" disabled="disabled">
			<input type="text" name="" class="form-control" value="35 s/d 39 tahun" disabled="disabled">
			<input type="text" name="" class="form-control" value="40 s/d 50 tahun" disabled="disabled">
			<input type="text" name="" class="form-control" value="50 tahun ke atas" disabled="disabled">
		</div>
		<div class="col-md-6">
			<input type="text" name="BAWAH15" class="form-control" placeholder="....%">
			<input type="text" name="15SD19" class="form-control" placeholder="....%">
			<input type="text" name="20SD24" class="form-control" placeholder="....%">
			<input type="text" name="25SD29" class="form-control" placeholder="....%">
			<input type="text" name="30SD34" class="form-control" placeholder="....%">
			<input type="text" name="35SD39" class="form-control" placeholder="....%">
			<input type="text" name="40SD50" class="form-control" placeholder="....%">
			<input type="text" name="ATAS50" class="form-control" placeholder="....%">
		</div>
		<input type="text" name="" value="100%" class="form-control" disabled="disabled">
	</div>
</div>