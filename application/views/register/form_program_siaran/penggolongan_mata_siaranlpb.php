<div class="form-group border-form col-md-12">
	<label>Penggolongan dan Persentase Mata Acara Siaran</label>
		<br><br>
		<div class="form-group col-md-4">
			<label>a. Umum/general entertainment</label><br>
			<label>b. Berita, penerangan, informasi, eksplorasi</label><br>
			<label>c. Pendidikan dan anak-anak</label>
			<label style="margin-bottom:10px;">d. Agama</label><br>
			<label style="margin-bottom:15px;">e. Olahraga</label><br>
			<label style="margin-bottom:15px;">f. Film </label><br>
			<label style="margin-bottom:15px;">g. Musik</label><br>
			<label style="margin-bottom:15px;">h. Lainnya (sebutkan genre)</label><br>
			<label>Jumlah</label>
		</div>
		<div class="form-group col-md-8">
			<input type="text" name="ms_lpb_umum" class="form-control" placeholder="....%">
			<input type="text" name="ms_lpb_berita" class="form-control" placeholder="....%">
			<input type="text" name="ms_lpb_pendidikan" class="form-control" placeholder="....%">
			<input type="text" name="ms_lpb_agama" class="form-control" placeholder="....%">
			<input type="text" name="ms_lpb_olahraga" class="form-control" placeholder="....%">
			<input type="text" name="ms_lpb_film" class="form-control" placeholder="....%">
			<input type="text" name="ms_lpb_musik" class="form-control" placeholder="....%">
			<input type="text" name="ms_lpb_lainnya" class="form-control" placeholder="....%">
			<input type="text" name="" value="100%" class="form-control" disabled="disabled">
		</div>
</div>