
<div class="form-group border-form">
	<label>B.	Stasiun Distribusi Ke Pelanggan</label>
	<div class="form-group border-form">
		<label>Nama Stasiun Pemancar (Stasiun Bumi – uplink)</label>
		<input  maxlength="100%" type="text" name="NAMA_SP" required="required" class="form-control " placeholder="Masukan Nama Stasiun Pemancar" />
	</div>
	<div class="form-group border-form">
		<label>Mulai Beroperasi</label>
		<input  maxlength="100%" type="date" name="MULAI_BEROPERASI" required="required" class="form-control " placeholder="Masukan Nama Perusahaan" />
	</div>
	<?php load_partials('form_data_teknik/alamat_stasiun.php'); ?>
	<div class="form-group border-form">
	<label>Moda penyiaran suara : (pilih salah satu)</label>
		<div class="form-group border-form">
			<label>
				<input type="radio" name="MPS" class="flat" value="1">Mono  
			</label>
			<label>
				<input type="radio" name="MPS" class="flat" value="2">Stereo 
			</label>
		</div>
	</div>
	<div class="form-group border-form col-md-12">
		<label>Peralatan Pemancar</label>
			<br><br>
			<div class="form-group col-md-4">
				<label>a. Merek</label><br><br>
				<label style="margin-bottom:15px;">b. Tipe</label><br>
				<label>c. Nomor seri</label><br><br>
				<label>d. Buatan</label><br><br><br>
				<label style="margin-bottom:20px;">e. Tahun</label><br>
				<label style="margin-bottom:15px;">f. Daya Pemancar Maksimum</label><br>
				<label>g. daya pemancar terpasang(running)</label><br>
			</div>
			<div class="form-group col-md-8">
				<input type="text" name="MEREK_PP" class="form-control" placeholder="....">
				<input type="text" name="TIPE_PP" class="form-control" placeholder="....">
				<input type="text" name="NOSERI_PP" class="form-control" placeholder="....">
				<div class="form-group border-form">
				<label>
					<input type="radio" name="BUATAN_PP" class="form-control" placeholder="....">Pabrikan (sebutkan nama negaranya)
				</label>
				<label>
					<input type="radio" name="BUATAN_PP" class="form-control" placeholder="....">Buatan sendiri
				</label>
				</div>
				<input type="date" name="TAHUN_PP" class="form-control" placeholder="....">
				<input type="text" name="DAYAPEMANCARMAX_PP" class="form-control" placeholder="....watt">
				<input type="text" name="DAYAPEMANCARMIN_PP" class="form-control" placeholder="....watt">
			</div>
	</div>
	<div class="form-group border-form">
		<label>Jumlah transponder</label>
		<input  maxlength="100%" type="text" name="JMLTRANSPONDER" required="required" class="form-control " placeholder="transponder" />
	</div>
	<div class="form-group border-form">
		<label>Bandwidth transponder</label>
		<input  maxlength="100%" type="text" name="BWTRANSPONDERA" required="required" class="form-control " placeholder="................. Mhz sd " />
		<input  maxlength="100%" type="text" name="BWTRANSPONDERZ" required="required" class="form-control " placeholder=" .................. Mhz" />
	</div>
	<div class="form-group border-form">
		<label>Jumlah saluran tiap transponder</label>
		<input  maxlength="100%" type="text" name="JMLSALURAN" required="required" class="form-control " placeholder="………………… program" />
	</div>
	<div class="form-group border-form">
		<label>Ranges Frekuensi (down-link)</label>
		<input  maxlength="100%" type="text" name="FREKDOWNA" required="required" class="form-control " placeholder="................. Mhz sd " />
		<input  maxlength="100%" type="text" name="FREKDOWNZ" required="required" class="form-control " placeholder=" .................. Mhz" />
	</div>
	<div class="form-group border-form">
		<label>Ranges Frekuensi (up-link)</label>
		<input  maxlength="100%" type="text" name="FREKUPA" required="required" class="form-control " placeholder="................. Mhz sd " />
		<input  maxlength="100%" type="text" name="FREKUPZ" required="required" class="form-control " placeholder=" .................. Mhz" />
	</div>
	<script type="text/javascript">

	function yesnoCheck() {
	    if (document.getElementById('yesCheck').checked) {
	        document.getElementById('ifYes').style.display = 'block';
	    }
	    else document.getElementById('ifYes').style.display = 'none';

	}

	</script>

	<div class="form-group border-form col-md-12">
		<label>Feeder</label>
			<br><br>
			<div class="form-group col-md-4">
				<label>a. Jenis (pilih sesuai yang digunakan, boleh lebih dari satu)</label><br><br>
				<label style="margin-bottom:15px;">b. Merek</label><br>
				<label>c. Type dan ukuran</label><br><br>
				<label style="margin-bottom:15px;">d. Panjang kabel</label><br>
				<label style="margin-bottom:20px;">e. Loss kabel per meter</label><br>
				<label style="margin-bottom:15px;">f. Total loss feeder </label><br>
			</div>
			<div class="form-group col-md-8">
				<div class="form-group border-form">
				<label>
					<input type="radio" name="JENIS_FEEDER" value="1" onclick="javascript:yesnoCheck();"  id="noCheck" class="rb">coaxial
				</label>
				<label>
					<input type="radio" name="JENIS_FEEDER" value="1" onclick="javascript:yesnoCheck();"  id="noCheck" class="rb">waveguide
				</label>
				<label>
					<input type="radio" onclick="javascript:yesnoCheck();" name="JENIS" id="yesCheck" class="rb">lainnya (sebutkan)
				</label>
				<br>
			    <div id="ifYes" style="display:none">
			        Masukan Format Siaran lainnya: <input type='text' id='yes' name='yes_feeder'><br>
			    </div>
				</div>
				<input type="text" name="MEREK_FEEDER" class="form-control" placeholder="....">
				<input type="text" name="TIPEUKURAN_FEEDER" class="form-control" placeholder="....">
				<input type="text" name="PANJANGKABEL_FEEDER" class="form-control" placeholder="...... meter">
				<input type="text" name="LOSSKABEL_FEEDER" class="form-control" placeholder="...... dB">
				<input type="text" name="TOTALLOSSKABEL_FEEDER" class="form-control" placeholder="...... dB (kabel + connector)">
			</div>
	</div>
	<div class="form-group border-form">
	<label>Sistem hubungan dari stasiun pengendali ke pemancar (pilih sesuai yang digunakan, boleh lebih dari satu):</label>
		<div class="form-group border-form">
			<label>
				<input type="radio" name="SISHUB" class="flat" value="1">Melalui kabel  
			</label>
			<label>
				<input type="radio" name="SISHUB" class="flat" value="2">Menggunakan radio link (Micro Wave/UHF) 
			</label>
			<label>
				<input type="radio" name="SISHUB" class="flat" value="3">Menggunakan satelit
			</label>
		</div>
	</div>
	<div class="form-group border-form col-md-12">
		<label>Spesifikasi peralatan radio link (diisi jika menggunakan radio link)</label>
			<br><br>
			<div class="form-group col-md-4">
				<label>a. Merek</label><br><br>
				<label style="margin-bottom:15px;">b. Tipe</label><br>
				<label>c. Nomor seri</label><br><br>
				<label>d. Buatan</label><br><br><br>
				<label style="margin-bottom:20px;">e. Tahun</label><br>
				<label style="margin-bottom:15px;">f. Frekuensi</label><br>
				<label>g. Lebar pita frekuensi</label><br>
			</div>
			<div class="form-group col-md-8">
				<input type="text" name="MEREK_SPR" class="form-control" placeholder="....">
				<input type="text" name="TIPE_SPR" class="form-control" placeholder="....">
				<input type="text" name="NOSERI_SPR" class="form-control" placeholder="....">
				<div class="form-group border-form">
				<label>
					<input type="radio" name="JENIS_SPR" class="form-control" placeholder="...." value="1">Pabrikan (sebutkan nama negaranya)
				</label>
				<label>
					<input type="radio" name="JENIS_SPR" class="form-control" placeholder="...." value="2">Buatan sendiri
				</label>
				</div>
				<input type="date" name="tahun_SPR" class="form-control" placeholder="....">
				<input type="text" name="FREK_SPR" class="form-control" placeholder="....watt">
				<input type="text" name="LEBARFREK_SPR" class="form-control" placeholder="....watt">
			</div>
	</div>
	<div class="form-group border-form col-md-12">
		<label>Spesifikasi Satelit (diisi jika menggunakan satelit, satelit yang digunakan harus memilki landing right dari Pemerintah)</label>
			<br><br>
			<div class="form-group col-md-4">
				<label>a. Nama satelit</label><br><br>
				<label style="margin-bottom:15px;">b. Negara pemilik satelit</label><br>
				<label>c. Range frekuensi satelit</label><br><br>
				<label>d. Lebar pita frekuensi</label><br><br><br>
				<label style="margin-bottom:20px;">e. Koordinat</label><br>
			</div>
			<div class="form-group col-md-8">
				<input type="text" name="NAMA_SS" class="form-control" placeholder="....">
				<input type="text" name="NEGARA_SS" class="form-control" placeholder="....">
				<input type="text" name="RANGE_SS" class="form-control" placeholder="....">
				<input type="text" name="LEBARPITA_SS" class="form-control" placeholder="....">
				<input type="text" name="KOORDINAT_SS" class="form-control" placeholder="....">
			</div>
	</div>
	<div class="form-group border-form col-md-12">
		<label>Satelit Penyiaran</label>
			<br><br>
			<div class="form-group col-md-4">
				<label>a. Nama satelit</label><br><br>
				<label style="margin-bottom:15px;">b. Negara pemilik satelit</label><br>
				<label>c. Frekuensi downlink</label><br><br>
				<label>d. Lebar pita frekuensi</label><br><br><br>
				<label style="margin-bottom:20px;">e. Koordinat satelit</label><br>
				<label style="margin-bottom:20px;">f. wilayah jangkauan siaran (footprint satelit)</label><br>
			</div>
			<div class="form-group col-md-8">
				<input type="text" name="NAMA_SATPEN" class="form-control" placeholder="(satelit yang digunakan harus memilki landing right dari Pemerintah)">
				<input type="text" name="NEGARA_SATPEN" class="form-control" placeholder="....">
				<input type="text" name="FREKDOWN_SATPEN" class="form-control" placeholder="....">
				<input type="text" name="LEBARPITA_SATPEN" class="form-control" placeholder="....">
				<input type="text" name="KOORDINAT_SATPEN" class="form-control" placeholder="....">
				<input type="text" name="WILAYAHJANGKAUAN_SATPEN" class="form-control" placeholder="(dilampirkan)">
			</div>
	</div>
	<div class="form-group border-form">
		<label>Wilayah jangkauan siaran (sebutkan nama daerah yang dapat dijangkau) dan peta kontur diagramnya  (dilampirkan):</label>
		<input  maxlength="100%" type="date" name="WILAYAHJANGKUAN_STD" required="required" class="form-control " placeholder="Masukan Nama Perusahaan" />
	</div>
</div>