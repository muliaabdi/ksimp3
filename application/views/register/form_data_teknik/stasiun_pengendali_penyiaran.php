<div class="form-group border-form">
	<label>Alamat Pemancar</label>
	<div class="form-group border-form">
			<label class="control-label">Jalan</label>
			<textarea required="required" class="form-control" placeholder="Masukan Alamat Pemancar" ></textarea>
		</div>
		<div class="form-group">
			<label class="control-label">Provinsi</label>
			<select class="form-control" id="propinsi" name="PROPPEMANCAR">
				<option value="0">-Pilih Provinsi-</option>
				<?php 
				foreach ($provinsi as $prov) {
				echo "<option value='$prov[id]'>$prov[name]</option>";
				}
				?>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Kota</label>
			<select class="form-control" id="kabupaten-kota" name="KABPEMANCAR">
				<option value="0">-Pilih Kota-</option>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Kecamatan</label>
			<select class="form-control" id="kecamatan" name="KECPEMANCAR">
				<option value="0">-Pilih Kecamatan-</option>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Kelurahan</label>
			<select class="form-control" id="kelurahan" name="KELPEMANCAR">
				<option value="0">-Pilih Kelurahan-</option>
			</select>
		</div>
		<div class="form-group">
			<label class="control-label">Kode Pos</label>
			<input type="text" name="KODEPOS_SPP" class="form-control">
		</div>
		<div class="form-group">
			<label class="control-label">Nomor Telepon</label>
			<input type="text" name="NOTELP_SPP" class="form-control">
		</div>
		<div class="form-group">
			<label class="control-label">Email</label>
			<input type="text" name="EMAIL_SPP" class="form-control">
		</div>
</div>
<div class="form-group border-form">
	<label>Stasiun Distribusi Ke Pelanggan (pilih salah satu)</label>
	<div class="form-group border-form">
			<label>
				<input type="radio" name="SDP_LPB" class="flat">LPB Melalui Kabel  
			</label>
			<label>
				<input type="radio" name="SDP_LPB" class="flat">LPB Melalui Teresterial  
			</label>
			<label>
				<input type="radio" name="SDP_LPB" class="flat">LPB Melalui Satelit  
			</label>
		</div>
</div>
<div class="form-group border-form">
	<label for="tags">Wilayah layanan sesuai Master Plan</label>
	<div class="form-group border-form">
			<input type="text" id="tags" name="WILAYAHLAYANAN_STPENG" class="form-control" placeholder="Masukan Wilayah layanan sesuai Master Plan">
	</div>
</div>