	<?php load_partials('header-login.html'); ?>
	<body>
		<div class="container">
			<div class="col-md-4" >
				<h1 style="text-align: center;color: black;margin-left: -20%;">Pendaftaran</h1>
				<div class="stepwizard col-md-offset-3">
					<div class="stepwizard-row setup-panel">
						<div class="stepwizard-step">
							<ul style="text-align: left; font-size: 13pt;margin-left: -20%; list-style:none">
								<li><a href="#step-1"  class="btn btn-primary btn-primary1">1. Data Administrasi</a></li>
								<li style="text-align: left; font-size: 10pt;margin-left: 5%;"><a href="#step-2" class="btn btn btn-default btn-primary1"" disabled="disabled">1.1 Data Manajemen</a></li>
								<li style="text-align: left; font-size: 10pt;margin-left: 5%;"><a href="#step-3"  class="btn btn btn-default btn-primary1"" disabled="disabled">1.2 Data Kepegawaian</a></li>
								<li><a href="#step-4"  class="btn btn-default btn-primary1"" disabled="disabled">2. Program Siaran</a></li>
								<li><a href="#step-5"  class="btn btn-default btn-primary1"" disabled="disabled">3. Data Teknis</a></li>
								<li><a href="#step-6"  class="btn btn-default btn-primary1"" disabled="disabled">4. File yang dilampirkan</a></li>
								<li><a href="#step-7"  class="btn btn-default btn-primary1"" disabled="disabled">5. Konfirmasi</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8" style="margin-left: -9%">
				<form role="form" action="<?php echo site_url('pemohon_baru/add'); ?>" method="post"  enctype="multipart/form-data" ng-app="">
					<div class="row setup-content col-md-12" id="step-1">
						<legend>Data Administrasi</legend>
						<div class="col-md-10">
							<legend>A. <?php echo $nm_jenis_izin ?></legend>
						<input type="hidden" name="id_izin" value="<?php echo $id_izin;?>" >
						<input type="hidden" name="jenis_izin" value="<?php echo $jenis_izin;?>" >
