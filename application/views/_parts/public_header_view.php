<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php
		// Page Title
		if(isset($theme['assets']['header']['title']))
			echo $this->template->get_title() . "\n";

		// Meta Tags
		if(isset($theme['assets']['header']['meta'])) {
			foreach($this->template->get_meta() as $meta_tag) {
				echo $meta_tag . "\n";
			}
		}
    echo '<link rel="icon" href="'.base_url().'assets/public/themes/default/images/logo.png" sizes="16x16" type="image/png">';
		// load css
		echo '<link href="'.base_url().'assets/public/themes/default/css/bootstrap.min.css" rel="stylesheet">
			  <link href="'.base_url().'assets/public/themes/default/fonts/css/font-awesome.min.css" rel="stylesheet">
			  <link href="'.base_url().'assets/public/themes/default/css/animate.min.css" rel="stylesheet">
			  <link href="'.base_url().'assets/public/themes/default/css/custom.css" rel="stylesheet">';

		// Custom CSS Files
		if(isset($theme['assets']['header']['css'])) {
			foreach($this->template->get_css() as $css_file) {
				echo $css_file . "\n";
			}
		}

		//load js
		echo '<script src="'.base_url().'assets/public/themes/default/js/jquery.min.js"></script>';

		// Custom JS Files
		if(isset($theme['assets']['header']['js'])) {
			foreach($this->template->get_js('header') as $js_file) {
				echo $js_file . "\n";
			}
		}
	?>
</head>
<body class="nav-md">

  <div class="container body">

    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <!-- <img src="<?php echo base_url().'assets/public/themes/default/images/logo.png' ?>" class="img-responsive" width="40" height="40"> -->
            <!-- <div class="row login-logo">
              <div class="col-md-4 ">
              <img src="<?php echo base_url().'assets/public/themes/default/images/logo.png' ?>" class="img-responsive" width="50" height="50">
              </div>
              <div class="col-md-8 hidden-xs">
                <h1><b>e-Penyiaran</b></h1>
                <div class="subtitle">
                  <p>Pemutakhiran Data</p>
                </div>
              </div>
            </div> -->
            <a href="<?php echo site_url('welcome'); ?>" class="site_title"><img src="<?php echo base_url('assets/public/themes/default/images/logo.png'); ?>" width="50" height="50">
              <span class="title">
                SIMP3 Pemutakhiran
              </span>
            </a>
          </div>
          <div class="clearfix"></div>


          <!-- menu prile quick info -->
          <!-- <div class="profile">
            <div class="profile_pic">
              <img src="<?php echo base_url() ?>assets/public/themes/default/images/img.jpg" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
              <h2>John Doe</h2>
            </div>
          </div> -->
          <!-- /menu prile quick info -->
          <br />
          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <?php if ($this->ion_auth->logged_in()){ ?>
            <div class="menu_section">
              <ul class="nav side-menu">
                <li><a><i class="fa fa-home"></i>Home <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?php echo site_url('welcome'); ?>">Welcome</a>
                    </li>
                    <!-- <li><a href="<?php echo site_url('konfirmasikontak'); ?>">Konfirmasi Contact</a>
                    </li> -->
                  </ul>
                </li>
                <li><a><i class="fa fa-edit"></i>Konfirmasi Data SIMP3<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?php echo site_url('home/konfirmasipenyelenggara'); ?>">Konfirmasi Penyelenggara</a>
                    </li>
                    <li><a href="<?php echo base_url('konfirmasi/kantor'); ?>">Konfirmasi Kantor</a>
                    </li>
                    <li><a href="<?php echo base_url('konfirmasi/studio'); ?>">Konfirmasi Studio</a>
                    </li>
                    <li><a href="<?php echo base_url('konfirmasi/pemancar'); ?>">Konfirmasi Pemancar</a>
                    </li>
                    <li><a href="<?php echo base_url('home/konfirmasikronologis'); ?>">Konfirmasi Kronologis</a>
                    </li>
                    <li><a href="<?php echo base_url('home/konfirmasispp'); ?>">Konfirmasi SPP</a>
                    </li>
                    <li><a href="<?php echo base_url('home/cetaksurat'); ?>">Cetak Surat Konfirmasi</a>
                    </li>
                  </ul>
                </li>
                <!-- <li><a><i class="fa fa-desktop"></i>Data SIMP3<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="general_elements.html">Contact</a>
                    </li>
                    <li><a href="media_gallery.html">Penyelenggara Penyiaran</a>
                    </li>
                  </ul>
                </li> -->
                <!-- <li><a><i class="fa fa-user-plus"></i>Pejabat<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?php echo base_url('pejabat/penyelenggara'); ?>">Konfirmasi Penyelenggara</a>
                    </li>
                    <li><a href="<?php echo base_url('pejabat/kantor'); ?>">Konfirmasi Kantor</a>
                    </li>
                    <li><a href="<?php echo base_url('pejabat/kronologis'); ?>">Konfirmasi Kronologis</a>
                    </li>
                  </ul>
                </li> -->
                <li><a href="<?php echo base_url('coklit/revisi'); ?>"><i class="fa fa-check-circle-o" aria-hidden="true"></i>COKLIT</a>
                  <!-- <ul class="nav child_menu" style="display: none">
                    <li>
                    </li>
                    <li><a href="<?php echo base_url('pejabat/kantor'); ?>">Konfirmasi Kantor</a>
                    </li>
                    <li><a href="<?php echo base_url('pejabat/kronologis'); ?>">Konfirmasi Kronologis</a>
                    </li>
                  </ul> -->
                </li>
                <li><a><i class="fa fa-plus-circle" aria-hidden="true"></i>EUCS<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?php echo base_url('coklit/eucs'); ?>">Konfirmasi EUCS</a>
                    </li>
                    <li><a href="<?php echo base_url('coklit/perubahan'); ?>">Konfirmasi Perubahan Data</a>
                    </li>
                  </ul>
                </li>
                <!-- <li><a href="<?php echo base_url('coklit/wlayanan'); ?>"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Wilayah Layanan</a>
                </li> -->
              </ul>
            </div>
            <?php } ?>
          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <!-- <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url('auth/logout'); ?>">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div> -->
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
            <?php if ($this->ion_auth->logged_in()){ ?>
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <?php $string = $this->ion_auth->user()->row()->first_name;
                  $expr = '/(?<=\s|^)[a-z]/i';
                  preg_match_all($expr, $string, $matches);
                  $result = implode('', $matches[0]);
                  $result = strtoupper($result);
                  echo "<p class=\"btn\" style=\"color:black;background:white;\">$result</p>"; ?>
                  	<?php echo $this->ion_auth->user()->row()->first_name; ?>
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                  </li>
                  <li><a href="<?php echo base_url(); ?>auth/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                  </li>
                </ul>
              </li>
              <?php } ?>

              <!-- <li role="presentation" class="dropdown">
                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                  <i class="fa fa-envelope-o"></i>
                  <span class="badge bg-green">6</span>
                </a>
                <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                  <li>
                    <a>
                      <span class="image">
                                        <img src="<?php echo base_url() ?>assets/public/themes/default/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="<?php echo base_url() ?>assets/public/themes/default/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="<?php echo base_url() ?>assets/public/themes/default/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <a>
                      <span class="image">
                                        <img src="<?php echo base_url() ?>assets/public/themes/default/images/img.jpg" alt="Profile Image" />
                                    </span>
                      <span>
                                        <span>John Smith</span>
                      <span class="time">3 mins ago</span>
                      </span>
                      <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                    </a>
                  </li>
                  <li>
                    <div class="text-center">
                      <a>
                        <strong>See All Alerts</strong>
                        <i class="fa fa-angle-right"></i>
                      </a>
                    </div>
                  </li>
                </ul>
              </li> -->

            </ul>
          </nav>
        </div>
      </div>
      <!-- /top navigation -->