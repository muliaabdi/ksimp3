<?php
	//load js
	echo '<script src="'.base_url().'assets/public/themes/default/js/bootstrap.min.js"></script>
	      <script src="'.base_url().'assets/public/themes/default/js/custom.js"></script>
	      <script src="'.base_url().'assets/public/themes/default/js/progressbar/bootstrap-progressbar.min.js"></script>';

	// Custom JS Files
	if(isset($theme['assets']['footer']['js'])) {
		foreach($this->template->get_js('footer') as $js_file) {
			echo $js_file . "\n";
		}
	}
?>
	</div>
  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <!-- form validation -->
  <script type="text/javascript">
    // $(document).ready(function() {
    //   $.listen('parsley:field:validate', function() {
    //     validateFront();
    //   });
    //   $('#demo-form .btn').on('click', function() {
    //     $('#demo-form').parsley().validate();
    //     validateFront();
    //   });
    //   var validateFront = function() {
    //     if (true === $('#demo-form').parsley().isValid()) {
    //       $('.bs-callout-info').removeClass('hidden');
    //       $('.bs-callout-warning').addClass('hidden');
    //     } else {
    //       $('.bs-callout-info').addClass('hidden');
    //       $('.bs-callout-warning').removeClass('hidden');
    //     }
    //   };
    // });
    // try {
    //   hljs.initHighlightingOnLoad();
    // } catch (err) {}
  </script>
  <!-- /form validation -->

</body>
</html>