<?php $this->load->view('_parts/public_header_view'); ?>

<div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_right">
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Wilayah Layanan</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <form id="demo-form2" method="post" action="<?php echo base_url().'coklit/wlayanan/edit/'?>" class="form-horizontal form-label-left">
                    <div class='form-group'>
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Provinsi
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='provinsi' onchange="document.getElementById('text_content').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        <?php 
                        foreach ($provinsi as $prov) {
                        echo "<option value='$prov[PROVID]'>$prov[PROV]</option>";
                        }
                        ?>
                        </select>
                      </div>
                    </div>
                      <input type="hidden" id="text_content" name="propinsikantor" value="" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kabupaten/Kota
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kabupaten-kota' onchange="document.getElementById('text_content2').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_content2" name="kabkotakantor" value="" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kecamatan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kecamatan' onchange="document.getElementById('text_content3').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_content3"  name="kecamatankantor" value="" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kelurahan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kelurahan-desa'  onchange="document.getElementById('text_content4').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_content4" name="kelurahankantor" value="" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Pos
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kodepos' name="kodeposkantor">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Zona
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='zona' name="zona">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                    <!-- <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <a href="<?php echo base_url().'coklit/revisi' ?>" class="btn btn-primary">Cancel</a>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div> -->
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer>
          <div class="copyright-info">
            <p class="pull-right">SIMP3</p>
          </div>
          <div class="clearfix"></div>
        </footer
      </div>
<script type="text/javascript">
$(function(){

    $.ajaxSetup({
    type:"POST",
    url: "<?php echo base_url('coklit/revisi/ambil_data') ?>",
    cache: false,
    });

    //kantor
    $("#provinsi").change(function(){

    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kabupaten',id:value},
    success: function(respond){
    $("#kabupaten-kota").html(respond);
    }
    })
    }

    });


    $("#kabupaten-kota").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kecamatan',id:value},
    success: function(respond){
    $("#kecamatan").html(respond);
    }
    })
    }
    })

    $("#kecamatan").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kelurahan',id:value},
    success: function(respond){
    $("#kelurahan-desa").html(respond);
    }
    })
    } 
    })

    $("#kecamatan").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kodepos',id:value},
    success: function(respond){
    $("#kodepos").html(respond);
    }
    })
    } 
    })

    $("#kabupaten-kota").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'zona',id:value},
    success: function(respond){
    $("#zona").html(respond);
    }
    })
    } 
    })
  })
  </script>
<?php $this->load->view('_parts/public_footer_view'); ?>