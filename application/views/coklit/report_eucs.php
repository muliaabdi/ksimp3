<?php $this->load->view('_parts/public_header_view'); ?>
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Konfirmasi EUCS</h3>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Data EUCS</h2>
                  <!-- <div class="pull-right">
            <a href="<?php echo base_url().'coklit/eucs/tambah'; ?>" class="btn btn-primary">Tambah</a>
         </div> -->
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-bordered table-striped table-hover" id="mytable">
                        <thead>
                            <tr>
                              <th width="50px">S/S</th>
                              <th>ACTION</th>
                              <th>NO</th>
                              <th>COMPANY NAME</th>
                              <th>ALAMAT</th>
                              <th>ASAL BERKAS</th>
                              <th>PERUBAHAN DATA</th>
                              <th>TINDAK LANJUT</th>
                            </tr>
                        </thead>
                    </table>
                </div>
              </div>
            </div>
            <br />
            <br />
            <br />

          </div>
        </div>
        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">SIMP3</p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>

  <script type="text/javascript">
  function format ( d ) {
    return  'Source : '+d.SOURCE+'<br>'+
            'Sheet  : '+d.SHEET;
  }
            $(document).ready(function () {
 
                // $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
                // {
                //     return {
                //         "iStart": oSettings._iDisplayStart,
                //         "iEnd": oSettings.fnDisplayEnd(),
                //         "iLength": oSettings._iDisplayLength,
                //         "iTotal": oSettings.fnRecordsTotal(),
                //         "iFilteredTotal": oSettings.fnRecordsDisplay(),
                //         "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                //         "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                //     };
                // };
 
                var dt = $('#mytable').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "oLanguage": {
                       "sSearch": "Search "
                     },
                    "ajax": "<?php echo base_url('coklit/eucs/ajax_eucs'); ?>",
                    "columns": [
                                 {
                            "class":          "details-control",
                            "orderable":      false,
                            "data":           null,
                            "defaultContent": ""
                        },
                        {
                            "class": "text-center",
                            "orderable":      false,
                            "data": "ACTION"
                        },
                        {"data": "NO"},
                        {"data": "COMPANY NAME"},
                        {"data": "ALAMAT"},
                        {"data": "ASAL BERKAS"},
                        {"data": "PERUBAHAN DATA"},
                        {"data": "TINDAK LANJUT"}
                    ],
                    "order": [[1, 'asc']],
                    "order": [[2, 'asc']],
                    // "rowCallback": function (row, data, iDisplayIndex) {
                    //     var info = this.fnPagingInfo();
                    //     var page = info.iPage;
                    //     var length = info.iLength;
                    //     var index = page * length + (iDisplayIndex + 1);
                    //     $('td:eq(0)', row).html(index);
                    // }
                });
                var detailRows = [];
                $('#mytable tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();
 
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );
 
    // On each draw, loop over the `detailRows` array and show any child rows
    dt.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );
            });
        </script>
<?php $this->load->view('_parts/public_footer_view'); ?>