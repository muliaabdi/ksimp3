<?php $this->load->view('_parts/public_header_view'); ?>

<div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_right">
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Konfirmasi Data</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <?php foreach ($data as $row): ?>
                  <form id="demo-form2" method="post" action="<?php echo base_url().'coklit/revisi/edit/'.$this->uri->segment(4); ?>" class="form-horizontal form-label-left">
                    <?php if ($this->ion_auth->user()->row()->username === 'gun'): ?>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ID</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="id" value="<?php echo $row->id; ?>">
                      </div>
                    </div>
                  <?php else: ?>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ID</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" readonly="readonly" class="form-control col-md-7 col-xs-12" name="id" value="<?php echo $row->id; ?>">
                      </div>
                    </div>
                  <?php endif ?>
                    <?php 
                      $group = array('TELEVISI', 'RADIO', 'EUCS');
                      if (!$this->ion_auth->in_group($group)){ ?>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Catatan Konsultan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="middle-name" class="form-control col-md-7 col-xs-12" name="keterangan1"><?php echo $row->internalnote; ?>
                        </textarea>
                      </div>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Company Name</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="nmlembaga" value="<?php echo $row->nmlembaga; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">On Air Name</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="last-name" name="nmudara" class="form-control col-md-7 col-xs-12" value="<?php echo $row->nmudara; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">ID Pemohon</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="idpemohon" value="<?php echo $row->idpemohon; ?>">
                      </div>
                    </div>
                    <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Izin
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='nmijin' name="nmijin">
                        <option><?php echo $row->nmijin; ?></option>
                        <option disabled>──────────</option>
                        <?php foreach ($nmijin as $row2): ?>
                                    <?php if (!empty($row2->nmijin)): ?>
                                    <option value="<?php echo $row2->nmijin; ?>"><?php echo $row2->nmijin; ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Lembaga</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="jnslembaga" class="form-control col-md-7 col-xs-12" type="text" name="jnslembaga" value="<?php echo $row->jnslembaga; ?>">
                      </div>
                    </div><!-- 
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Sub Service</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nmmodulasi" value="<?php echo $row->nmmodulasi; ?>">
                      </div>
                    </div> -->
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Status LP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="statipp" class="form-control col-md-7 col-xs-12" type="text" name="statipp" value="<?php echo $row->statipp; ?>">
                      </div>
                    </div>
                    <?php if ($this->ion_auth->user()->row()->username === 'gun'): ?>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Trouble</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="trouble" value="<?php echo $row->trouble; ?>">
                      </div>
                    </div>
                    <?php endif ?>
                    <!-- <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Wilayah Layanan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="wilayahlayanan" value="<?php echo $row->wilayahlayanan; ?>">
                      </div>
                    </div> -->
                    <div class="form-group">
                        <legend class="col-md-12">Wilayah Layanan</legend>
                    </div>
                    <?php if ($row->radiotv == 'TELEVISI' or $row->radiotv == 'LPPLTV') { ?>
                    <div class='form-group'>
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Provinsi
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select multiple="multiple" class='select2_multiple form-control' id='provinsiwltv'>
                        <option>--pilih--</option>
                        <?php 
                          foreach ($provinsi as $prov) {
                          echo "<option value='$prov[PROVID]'>$prov[PROV]</option>";
                          }
                        ?>
                        </select>
                      </div>
                    </div>
                    <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Wilayah
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select multiple="multiple" class='select2_multiple form-control' id='wilayahlayanantv' name="wilayahlayanan[]">
                        <option selected><?php echo $row->wilayahlayanan; ?></option>
                        </select>
                        </div>
                    </div>
                    <?php }elseif ($row->radiotv == 'RADIO' or $row->radiotv == 'LPPLRADIO') { ?>
                      <div class='form-group'>
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Provinsi
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='provinsiwlradio'>
                        <option>--pilih--</option>
                        <?php 
                          foreach ($provinsi as $prov) {
                          echo "<option value='$prov[PROVID]'>$prov[PROV]</option>";
                          }
                        ?>
                        </select>
                      </div>
                    </div>
                    <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kota
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='wlkabkot'>
                        <option>--pilih--</option>
                        </select>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Wilayah
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='wilayahlayananradio' name="wilayahlayanan">
                        <option>--pilih--</option>
                        </select>
                        </div>
                    </div>
                    <?php }; ?>
                    <!-- <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Saluran
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='salurantv'>
                        <option>--pilih--</option>
                        </select>
                        </div>
                    </div> -->
                    <!-- <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Kedudukan Stasiun Penyiaran</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="kedudukanstasiunpenyiarankhususlpsindukjaringanggotajaringan" value="<?php echo $row->kedudukanstasiunpenyiarankhususlpsindukjaringanggotajaringan; ?>">
                      </div>
                    </div> -->
                    <div class="form-group">
                        <Legend class="col-md-12">Alamat Kantor</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Kantor</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="middle-name" class="form-control col-md-7 col-xs-12" name="jlnkantor"><?php echo $row->jlnkantor; ?></textarea>
                      </div>
                    </div>
<!--                     <div class='form-group'>
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Zona SPP
                      </label>
                      <div class="row">
                     <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="col-md-6 col-sm-6 col-xs-12" style="margin-left: -10px; margin-right:10px;">
                            <select data-placeholder="Pilih Zona" class='form-control chosen-select-no-results' id='zona2' name="zona2">
                            <option value=""></option>
                            <?php foreach ($zona2 as $zn): ?>
                              <option value="<?php echo $zn->NO; ?>"><?php echo $zn->DAERAH; ?></option>
                            <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12" type="text" name="zonaspp" id="znkominfo" readonly="readonly" value="<?php echo $row->zonaspp; ?>">
                        </div>
                      </div>
                      </div>
                      </div> -->
                    <div class='form-group'>
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Provinsi
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='provinsi' onchange="document.getElementById('text_content').value=this.options[this.selectedIndex].text">
                        <option><?php echo $row->propinsikantor; ?></option>
                        <option disabled>──────────</option>
                        <?php 
                        foreach ($provinsi as $prov) {
                        echo "<option value='$prov[PROVID]'>$prov[PROV]</option>";
                        }
                        ?>
                        </select>
                      </div>
                    </div>
                      <input type="hidden" id="text_content" name="propinsikantor" value="<?php echo $row->propinsikantor; ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kabupaten/Kota
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kabupaten-kota' onchange="document.getElementById('text_content2').value=this.options[this.selectedIndex].text">
                        <option><?php echo $row->kabkotakantor; ?></option>
                        <option disabled>──────────</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_content2" name="kabkotakantor" value="<?php echo $row->kabkotakantor; ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kecamatan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kecamatan' onchange="document.getElementById('text_content3').value=this.options[this.selectedIndex].text">
                        <option><?php echo $row->kecamatankantor; ?></option>
                        <option disabled>──────────</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_content3"  name="kecamatankantor" value="<?php echo $row->kecamatankantor; ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kelurahan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kelurahan-desa'  onchange="document.getElementById('text_content4').value=this.options[this.selectedIndex].text">
                        <option><?php echo $row->kelurahankantor; ?></option>
                        <option disabled>──────────</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_content4" name="kelurahankantor" value="<?php echo $row->kelurahankantor; ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Pos
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kodepos' name="kodeposkantor">
                        <option><?php echo $row->kodeposkantor; ?></option>
                        <option disabled>──────────</option>
                        </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Zona
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='zona' name="zona">
                        <option><?php echo $row->zona; ?></option>
                        <option disabled>──────────</option>
                        </select>
                        </div>
                      </div>
                    <div class="form-group">
                        <Legend class="col-md-12">Alamat Studio</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Studio</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea class="form-control col-md-7 col-xs-12" name="jlnstudio"><?php echo $row->jlnstudio; ?></textarea>
                      </div>
                    </div>
                    <div class='form-group'>
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Provinsi
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='provinsistudio' onchange="document.getElementById('text_provinsistudio').value=this.options[this.selectedIndex].text">
                        <option><?php echo $row->propinsistudio; ?></option>
                        <option disabled>──────────</option>
                        <?php 
                        foreach ($provinsi as $prov) {
                        echo "<option value='$prov[PROVID]'>$prov[PROV]</option>";
                        }
                        ?>
                        </select>
                      </div>
                    </div>
                      <input type="hidden" id="text_provinsistudio" name="propinsistudio" value="<?php echo $row->propinsistudio; ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kabupaten/Kota
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kabkotastudio' onchange="document.getElementById('text_kabkotastudio').value=this.options[this.selectedIndex].text">
                        <option><?php echo $row->kabkotastudio; ?></option>
                        <option disabled>──────────</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_kabkotastudio" name="kabkotastudio" value="<?php echo $row->kabkotastudio; ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kecamatan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kecamatanstudio' onchange="document.getElementById('text_kecamatanstudio').value=this.options[this.selectedIndex].text">
                        <option><?php echo $row->kecamatanstudio; ?></option>
                        <option disabled>──────────</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_kecamatanstudio"  name="kecamatanstudio" value="<?php echo $row->kecamatanstudio; ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kelurahan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kelurahanstudio'  onchange="document.getElementById('text_kelurahanstudio').value=this.options[this.selectedIndex].text">
                        <option><?php echo $row->kelurahanstudio; ?></option>
                        <option disabled>──────────</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_kelurahanstudio" name="kelurahanstudio" value="<?php echo $row->kelurahanstudio; ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Pos
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kodeposstudio' name="kodeposstudio">
                        <option><?php echo $row->kodeposstudio; ?></option>
                        <option disabled>──────────</option>
                        </select>
                        </div>
                      </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Telepon</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="notelpstudio" value="<?php echo $row->notelpstudio; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No FAX</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nofaxstudio" value="<?php echo $row->nofaxstudio; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <Legend class="col-md-12">Alamat Stasiun Pemancar</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Stasiun Pemancar</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nmstapemancar" value="<?php echo $row->nmstapemancar; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Pemancar</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="middle-name" class="form-control col-md-7 col-xs-12" name="alamatpemancar"><?php echo $row->alamatpemancar; ?></textarea>
                      </div>
                    </div>
                    <div class='form-group'>
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Provinsi
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='provinsipemanc' onchange="document.getElementById('text_provinsipemanc').value=this.options[this.selectedIndex].text">
                        <option><?php echo $row->provinsipemancar; ?></option>
                        <option disabled>──────────</option>
                        <?php 
                        foreach ($provinsi as $prov) {
                        echo "<option value='$prov[PROVID]'>$prov[PROV]</option>";
                        }
                        ?>
                        </select>
                      </div>
                    </div>
                      <input type="hidden" id="text_provinsipemanc" name="provinsipemancar" value="<?php echo $row->provinsipemancar; ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kabupaten/Kota
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kabkotapemanc' onchange="document.getElementById('text_kabkotapemanc').value=this.options[this.selectedIndex].text">
                        <option><?php echo $row->kabkotapemancar; ?></option>
                        <option disabled>──────────</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_kabkotapemanc" name="kabkotapemancar" value="<?php echo $row->kabkotapemancar; ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kecamatan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kecamatanpemanc' onchange="document.getElementById('text_kecamatanpemanc').value=this.options[this.selectedIndex].text">
                        <option><?php echo $row->kecamatanpemancar; ?></option>
                        <option disabled>──────────</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_kecamatanpemanc"  name="kecamatanpemancar" value="<?php echo $row->kecamatanpemancar; ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kelurahan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kelurahanpemanc'  onchange="document.getElementById('text_kelurahanpemanc').value=this.options[this.selectedIndex].text">
                        <option><?php echo $row->kelurahanpemancar; ?></option>
                        <option disabled>──────────</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_kelurahanpemanc" name="kelurahanpemancar" value="<?php echo $row->kelurahanpemancar; ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Pos
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kodepospemanc' name="kodepospemancar">
                        <option><?php echo $row->kodepospemancar; ?></option>
                        <option disabled>──────────</option>
                        </select>
                        </div>
                      </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">NPWP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="npwp" value="<?php echo $row->npwp; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Frekuensi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="frek" class="form-control col-md-7 col-xs-12" type="text" name="frek" value="<?php echo $row->frek; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">AM/FM</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="amfm" class="form-control col-md-7 col-xs-12" type="text" name="amfm" value="<?php echo $row->amfm; ?>">
                      </div>
                    </div>
                    <!-- <div class="form-group">
                        <Legend class="col-md-12">Domisili</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Surat</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nosuketdomisiliusaha" value="<?php echo $row->nosuketdomisiliusaha; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Surat</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker" class="datepick form-control col-md-7 col-xs-12" type="text" name="tglsuketdomisiliusaha" value="<?php echo $row->tglsuketdomisiliusaha; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Instansi Penerbit</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nminstansipenerbitdomisiliusaha" value="<?php echo $row->nminstansipenerbitdomisiliusaha; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Akta Pendirian</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noaktapendirian" value="<?php echo $row->noaktapendirian; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Akta Pendirian</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker2" class="datepick form-control col-md-7 col-xs-12" type="text" name="tglaktapendirian" value="<?php echo $row->tglaktapendirian; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Notaris</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nmnotarisaktapendirian" value="<?php echo $row->nmnotarisaktapendirian; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Domisili Notaris</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="domisilinotarisaktapendirian" value="<?php echo $row->domisilinotarisaktapendirian; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Pengesahan Akta</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nopengesahanakta" value="<?php echo $row->nopengesahanakta; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Pengesahan Akta</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker3" class="form-control col-md-7 col-xs-12" type="text" name="tglpengesahanakta" value="<?php echo $row->tglpengesahanakta; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Penerbit Pengesahan Akta</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nminstansipenerbitpengesahanakta" value="<?php echo $row->nminstansipenerbitpengesahanakta; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <Legend class="col-md-12">SITU</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No SITU</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nositu" value="<?php echo $row->nositu; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal SITU</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker4" class="form-control col-md-7 col-xs-12" type="text" name="tglsitu" value="<?php echo $row->tglsitu; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Instansi Penerbit SITU</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nminstansipenerbitsitu" value="<?php echo $row->nminstansipenerbitsitu; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <Legend class="col-md-12">TDP</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No TDP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="notdp" value="<?php echo $row->notdp; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal TDP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker5" class="form-control col-md-7 col-xs-12" type="text" name="tgltdp" value="<?php echo $row->tgltdp; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <Legend class="col-md-12">HO</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No HO</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noho" value="<?php echo $row->noho; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal HO</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker6" class="form-control col-md-7 col-xs-12" type="text" name="tglho" value="<?php echo $row->tglho; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Instansi Penerbit HO</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nminstansipenerbitho" value="<?php echo $row->nminstansipenerbitho; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <Legend class="col-md-12">IMB</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IMB</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noimb" value="<?php echo $row->noimb; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IMB</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker7" class="form-control col-md-7 col-xs-12" type="text" name="tglimb" value="<?php echo $row->tglimb; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Instansi Penerbit IMB</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nminstansipenerbitimb" value="<?php echo $row->nminstansipenerbitimb; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IMB Menara</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noimbm" value="<?php echo $row->noimbm; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IMB Menara</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker8" class="form-control col-md-7 col-xs-12" type="text" name="tglimbm" value="<?php echo $row->tglimbm; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Instansi Penerbit IMB Menara</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nminstansipenerbitimbm" value="<?php echo $row->nminstansipenerbitimbm; ?>">
                      </div>
                    </div> -->
                    <div class="form-group">
                        <legend class="col-md-12">Akta Perubahan Akhir</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Akta Perubahan Akhir</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noaktaperubahanakhir" value="<?php echo $row->noaktaperubahanakhir; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Akta Perubahan Akhir</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker9" class="form-control col-md-7 col-xs-12" type="text" name="tglaktaperubahanakhir" value="<?php echo $row->tglaktaperubahanakhir; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Notaris Akta Perubahan Akhir</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nmnotarisaktaperubahanakhir" value="<?php echo $row->nmnotarisaktaperubahanakhir; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Domisili Akta Perubahan Akhir</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="domisiliaktaperubahanakhir" value="<?php echo $row->domisiliaktaperubahanakhir; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Akta Sah Perubahan Akhir</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noaktasahperubahanakhir" value="<?php echo $row->noaktasahperubahanakhir; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Akta Sah Perubahan Akhir</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker10" class="form-control col-md-7 col-xs-12" type="text" name="tglaktasahperubahanakhir" value="<?php echo $row->tglaktasahperubahanakhir; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Instansi Penerbit</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nminstansisahpenerbitakhir" value="<?php echo $row->nminstansisahpenerbitakhir; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Rekomendasi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker11" class="form-control col-md-7 col-xs-12" type="text" name="tglrekomendasi" value="<?php echo $row->tglrekomendasi; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">FRB</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No. dan Tanggal RK</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="nodantanggalrk" class="form-control col-md-7 col-xs-12" type="text" name="nodantanggalrk" value="<?php echo $row->nodantanggalrk; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Pra FRB</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker12" class="form-control col-md-7 col-xs-12" type="text" name="tglprafrb" value="<?php echo $row->tglprafrb; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Usulan Frek</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="usulanfrekfrb" class="form-control col-md-7 col-xs-12" type="text" name="usulanfrekfrb" value="<?php echo $row->usulanfrekfrb; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal FRB</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker13" class="form-control col-md-7 col-xs-12" type="text" name="tglfrb" value="<?php echo $row->tglfrb; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Hasil FRB</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="hasilfrb" class="form-control col-md-7 col-xs-12" type="text" name="hasilfrb" value="<?php echo $row->hasilfrb; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">FRB Pending</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="frbpending" class="form-control col-md-7 col-xs-12" type="text" name="frbpending" value="<?php echo $row->frbpending; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Seleksi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker14" class="form-control col-md-7 col-xs-12" type="text" name="tglseleksi" value="<?php echo $row->tglseleksi; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal EUCS</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker15" class="form-control col-md-7 col-xs-12" type="text" name="tgleucs" value="<?php echo $row->tgleucs; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IPP Expiry</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker16" class="form-control col-md-7 col-xs-12" type="text" name="tglippexpiry" value="<?php echo $row->tglippexpiry; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No BAFRB</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nobafrb" value="<?php echo $row->nobafrb; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">EUCS</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="eucs" value="<?php echo $row->eucs; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Status EUCS</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="statuseucs" value="<?php echo $row->statuseucs; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Status Verifikasi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="statusverifikasi" value="<?php echo $row->statusverifikasi; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Surat Persetujuan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nosuratpersetujuan" value="<?php echo $row->nosuratpersetujuan; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Perubahan Data Sebelum</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="perubahandatasebelum" value="<?php echo $row->perubahandatasebelum; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Perubahan Data Sesudah</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="perubahandatasesudah" value="<?php echo $row->perubahandatasesudah; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Perihal Perubahan Data</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="perihalperubahandata" value="<?php echo $row->perihalperubahandata; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">IPP Prinsip</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IPP Prinsip</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nomorippprinsip" value="<?php echo $row->nomorippprinsip; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IPP Prinsip</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker17" class="form-control col-md-7 col-xs-12" type="text" name="tanggalippprinsip" value="<?php echo $row->tanggalippprinsip; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Masa Berlaku IPP Prinsip</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker24" class="form-control col-md-7 col-xs-12" type="text" name="masaberlakuippprinsip" value="<?php echo $row->masaberlakuippprinsip; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Permohonan perpanjangan IPP Prinsip</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker25" class="form-control col-md-7 col-xs-12" type="text" name="mohonperpanjanganippprinsip" value="<?php echo $row->mohonperpanjanganippprinsip; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Surat Penolakan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="suratpenolakan" class="form-control col-md-7 col-xs-12" type="text" name="suratpenolakan" value="<?php echo $row->suratpenolakan; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Perpanjangan IPP Prinsip</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nomorperpanjanganippprinsip" value="<?php echo $row->nomorperpanjanganippprinsip; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Perpanjangan IPP Prinsip</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker18" class="form-control col-md-7 col-xs-12" type="text" name="tanggalperpanjanganippprinsip" value="<?php echo $row->tanggalperpanjanganippprinsip; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Masa Berlaku Perpanjangan IPP Prinsip</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker25" class="form-control col-md-7 col-xs-12" type="text" name="masaberlakuperpanjanganipprinsip" value="<?php echo $row->masaberlakuperpanjanganipprinsip; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Kebijakan Prinsip mati</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="kebijakanprinsipmati" class="form-control col-md-7 col-xs-12" type="text" name="kebijakanprinsipmati" value="<?php echo $row->kebijakanprinsipmati; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">ISR</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="ISR" class="form-control col-md-7 col-xs-12" type="text" name="ISR" value="<?php echo $row->ISR; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">IPP Tetap</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IPP Tetap</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noipptetap" value="<?php echo $row->noipptetap; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IPP Tetap</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker19" class="form-control col-md-7 col-xs-12" type="text" name="tglipptetap" value="<?php echo $row->tglipptetap; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Masa Berlaku IPP Tetap</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker26" class="form-control col-md-7 col-xs-12" type="text" name="masaberlakuipptetap" value="<?php echo $row->masaberlakuipptetap; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">IPP Perpanjangan</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IPP Perpanjangan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nomorippperpanjangan5tahun" value="<?php echo $row->nomorippperpanjangan5tahun; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Menteri</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker30" class="form-control col-md-7 col-xs-12" type="text" name="tanggalmenteri" value="<?php echo $row->tanggalmenteri; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IPP Perpanjangan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker20" class="form-control col-md-7 col-xs-12" type="text" name="tanggalippperpanjangan5tahun" value="<?php echo $row->tanggalippperpanjangan5tahun; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Masa Berlaku IPP Perpanjangan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker27" class="form-control col-md-7 col-xs-12" type="text" name="masaberlakuippperpanjangan5tahun" value="<?php echo $row->masaberlakuippperpanjangan5tahun; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">IPP Penyesuaian</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IPP Penyesuaian</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noipppenyesuaian" value="<?php echo $row->noipppenyesuaian; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IPP Penyesuaian</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker21" class="form-control col-md-7 col-xs-12" type="text" name="tglipppenyesuaian" value="<?php echo $row->tglipppenyesuaian; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Masa Berlaku IPP Penyesuaian</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker28" class="form-control col-md-7 col-xs-12" type="text" name="masaberlakuipppenyesuaian" value="<?php echo $row->masaberlakuipppenyesuaian; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">IPP Migrasi</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IPP Migrasi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noipppenyesuaianamfm" value="<?php echo $row->noipppenyesuaianamfm; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IPP Migrasi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker22" class="form-control col-md-7 col-xs-12" type="text" name="tglipppenyesuaianamfm" value="<?php echo $row->tglipppenyesuaianamfm; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Masa Berlaku IPP Migrasi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker29" class="form-control col-md-7 col-xs-12" type="text" name="masaberlakuipppenyesuainamfm" value="<?php echo $row->masaberlakuipppenyesuainamfm; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Status Akhir</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="statusterakhir" value="<?php echo $row->statusterakhir; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">SPP</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Kode SPP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="kodebiaya" value="<?php echo $row->kodebiaya; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">ID SPP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="idspp" value="<?php echo $row->idspp; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Saldo Bayar</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="saldobayar" value="<?php echo $row->saldobayar; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Bayar</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker23" class="form-control col-md-7 col-xs-12" type="text" name="tglbayar" value="<?php echo $row->tglbayar; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <Legend class="col-md-12">Informasi Kontak</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Kontak</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nmkontak" value="<?php echo $row->nmkontak; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Telepon Kontak</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="notelpkontak" value="<?php echo $row->notelpkontak; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No FAX Kontak</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nofaxkontak" value="<?php echo $row->nofaxkontak; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No HP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nohpkontak" value="<?php echo $row->nohpkontak; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="emailkontak" value="<?php echo $row->emailkontak; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Bukti Surat Penunjukan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="buktisuratpenunjukan" value="<?php echo $row->buktisuratpenunjukan; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">Note</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Keterangan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="keterangan" class="form-control col-md-7 col-xs-12" type="text" name="keterangan" value="<?php echo $row->keterangan; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tindak Lanjut</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="tindaklanjut" class="form-control col-md-7 col-xs-12" type="text" name="tindaklanjut" value="<?php echo $row->tindaklanjut; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Catatan Kominfo</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="middle-name" class="form-control col-md-7 col-xs-12" name="catatan"></textarea>
                      </div>
                    </div>
                      <div class="form-group">
                      <label class="col-md-3 col-sm-3 col-xs-12 control-label">Sudah Diperbaiki
                      </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="flat" name="status" value="1">
                          </label>
                        </div>
                      </div>
                    </div>
                      <div class="form-group">
                      <label class="col-md-3 col-sm-3 col-xs-12 control-label">Archived 
                      </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" class="flat" name="archived" value="1">
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <a href="<?php echo base_url().'coklit/revisi' ?>" class="btn btn-primary">Cancel</a>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>
                  </form>
                <?php endforeach ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer>
          <div class="copyright-info">
            <p class="pull-right">SIMP3</p>
          </div>
          <div class="clearfix"></div>
        </footer>
      </div>
      <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <!-- <script src="http://code.jquery.com/jquery-1.10.2.js"></script> -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/public/themes/default/css/chosen.css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/public/themes/default/js/chosen.jquery.js" type="text/javascript"></script>
<!-- <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->
<script type="text/javascript">
$(document).ready(function() {
      // $(".select2_single").select2({
      //   placeholder: "Select a state",
      //   allowClear: true
      // });
      // $(".select2_group").select2({});
      $(".select2_multiple").select2({
        maximumSelectionLength: 4,
        placeholder: "With Max Selection limit 4",
        allowClear: true
      });
    });
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
    $("#zona2").on('change',function() {
        var id = $("#zona2").val();
        $.ajax({
            url: "<?php echo base_url('coklit/revisi/get_zona') ?>/" + id,
            dataType: "json",
            success: function (data) {
                $('#znkominfo').val(data[0].ZONA);
            }
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function () {
  $("#loading").hide();
    $(document).ajaxStart(function () {
        $("#loading").show();
    }).ajaxStop(function () {
        $("#loading").hide();
    });
});
$(function(){

    $.ajaxSetup({
    type:"POST",
    url: "<?php echo base_url('coklit/revisi/ambil_data') ?>",
    cache: false,
    });

    //kantor
    $("#provinsi").change(function(){

    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kabupaten',id:value},
    success: function(respond){
    $("#kabupaten-kota").html(respond);
    }
    })
    }

    });


    $("#kabupaten-kota").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kecamatan',id:value},
    success: function(respond){
    $("#kecamatan").html(respond);
    }
    })
    }
    })

    $("#kecamatan").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kelurahan',id:value},
    success: function(respond){
    $("#kelurahan-desa").html(respond);
    }
    })
    } 
    })

    $("#kecamatan").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kodepos',id:value},
    success: function(respond){
    $("#kodepos").html(respond);
    }
    })
    } 
    })

    $("#kabupaten-kota").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'zona',id:value},
    success: function(respond){
    $("#zona").html(respond);
    }
    })
    } 
    })

    //studio
    $("#provinsistudio").change(function(){

    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kabupaten',id:value},
    success: function(respond){
    $("#kabkotastudio").html(respond);
    }
    })
    }

    });


    $("#kabkotastudio").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kecamatan',id:value},
    success: function(respond){
    $("#kecamatanstudio").html(respond);
    }
    })
    }
    })

    $("#kecamatanstudio").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kelurahan',id:value},
    success: function(respond){
    $("#kelurahanstudio").html(respond);
    }
    })
    } 
    })

    $("#kecamatanstudio").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kodepos',id:value},
    success: function(respond){
    $("#kodeposstudio").html(respond);
    }
    })
    } 
    })

    //pemancar
    $("#provinsipemanc").change(function(){

    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kabupaten',id:value},
    success: function(respond){
    $("#kabkotapemanc").html(respond);
    }
    })
    }

    });


    $("#kabkotapemanc").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kecamatan',id:value},
    success: function(respond){
    $("#kecamatanpemanc").html(respond);
    }
    })
    }
    })

    $("#kecamatanpemanc").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kelurahan',id:value},
    success: function(respond){
    $("#kelurahanpemanc").html(respond);
    }
    })
    } 
    })

    $("#kecamatanpemanc").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kodepos',id:value},
    success: function(respond){
    $("#kodepospemanc").html(respond);
    }
    })
    } 
    })

    // $("#provinsiwltv").change(function(){
    //     $('#provinsiwltv option:selected').each(function(){
    //       alert($(this).text());
    //       var value=$(this).val();
    //       if(value>0){
    //       $.ajax({
    //       data:{modul:'wilayahlayanantv',id:value},
    //       success: function(respond){
    //       $("#wilayahlayanantv").html(respond);
    //       }
    //       })
    //       }
    //    });
    // });

    $('#provinsiwltv').on('change',function() {
      var value=$(this).val();
      // alert($(this).val());
      // console.log($(this).val());
      // if(value>0){
          $.ajax({
          data:{modul:'wilayahlayanantv',id:value},
          success: function(respond){
          $("#wilayahlayanantv").html(respond);
          }
          })
          // }
    });
    // $('#provinsiwltv option:selected').click(function() {
    //   alert($(this).val());
    // });

    // $("#wilayahlayanantv").change(function(){
    // var value=$(this).val();
    // if(value>0){
    // $.ajax({
    // data:{modul:'saluran',id:value},
    // success: function(respond){
    // $("#salurantv").html(respond);
    // }
    // })
    // }
    // })

    $("#provinsiwlradio").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'wlkabkot',id:value},
    success: function(respond){
    $("#wlkabkot").html(respond);
    }
    })
    }
    });

    $("#wlkabkot").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'wilayahlayananradio',id:value},
    success: function(respond){
    $("#wilayahlayananradio").html(respond);
    }
    })
    }
    })
    })
$(function() {
    $('#datepicker,#datepicker2,#datepicker3,#datepicker4,#datepicker5,#datepicker6,#datepicker7,#datepicker8,#datepicker9,#datepicker10,#datepicker11,#datepicker12,#datepicker13,#datepicker14,#datepicker15,#datepicker16,#datepicker17,#datepicker18,#datepicker19,#datepicker20,#datepicker21,#datepicker22,#datepicker23,#datepicker24,#datepicker25,#datepicker26,#datepicker27,#datepicker28,#datepicker29,#datepicker30').each(function(){
        $(this).datepicker({
          changeMonth: true,
          changeYear: true,
          yearRange: '1910:2025',
          dateFormat: 'yy-mm-dd'
        });
    });
});
  </script>
<?php $this->load->view('_parts/public_footer_view'); ?>