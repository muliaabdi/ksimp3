<?php $this->load->view('_parts/public_header_view'); ?>
<div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_right">
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Konfirmasi Data</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <form id="demo-form2" class="form-horizontal form-label-left" method="post" action="<?php echo base_url().'coklit/perubahan/edit/'.$this->uri->segment(4); ?>">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Company Name</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="NAMALEMBAGAPENYIARAN" value="<?php echo set_value('NAMALEMBAGAPENYIARAN'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Sebutan Udara</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="last-name" name="SEBUTANUDARA" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('SEBUTANUDARA'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Kode Staf</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="KODESTAF" value="<?php echo set_value('KODESTAF'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Asal Status Disposisi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="ASALSTATUSDISPOSISI" value="<?php echo set_value('ASALSTATUSDISPOSISI'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tahun</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="TAHUN" value="<?php echo set_value('TAHUN'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Input Staf</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="TANGGALINPUTSTAF" value="<?php echo set_value('TANGGALINPUTSTAF'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Surat TUM</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="NOSURATTUM" value="<?php echo set_value('NOSURATTUM'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Surat Ditum</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="TANGGALSURATDITUM" value="<?php echo set_value('TANGGALSURATDITUM'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Disposisi Direktur</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="TANGGALDISPOSISIDIREKTUR" value="<?php echo set_value('TANGGALDISPOSISIDIREKTUR'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Disposisi Kasubdit</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="TANGGALDISPOSISIKASUBDIT" value="<?php echo set_value('TANGGALDISPOSISIKASUBDIT'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Disposisi Kasie</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="TANGGALDISPOSISIKASIE" value="<?php echo set_value('TANGGALDISPOSISIKASIE'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Surat LP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="NOSURATLP" value="<?php echo set_value('NOSURATLP'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Surat LP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="TANGGALSURATLP" value="<?php echo set_value('TANGGALSURATLP'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Periode Permen</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="PERIODEPERMEN" value="<?php echo set_value('PERIODEPERMEN'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Group LP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="GROUPLP" value="<?php echo set_value('GROUPLP'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">LP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="LP" value="<?php echo set_value('LP'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Jenis LP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="JENISLP" value="<?php echo set_value('JENISLP'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Jenis IPP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="JENISIPP" value="<?php echo set_value('JENISIPP'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IPP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="NOIPP" value="<?php echo set_value('NOIPP'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="ALAMATDANKONTAKPERSON" value="<?php echo set_value('ALAMATDANKONTAKPERSON'); ?>">
                      </div>
                    </div>
                    <div class='form-group'>
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Provinsi
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='provinsi' onchange="document.getElementById('text_content').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        <?php 
                        foreach ($provinsi as $prov) {
                        echo "<option value='$prov[PROVID]'>$prov[PROV]</option>";
                        }
                        ?>
                        </select>
                      </div>
                    </div>
                      <input type="hidden" id="text_content" name="PROVINSI" value="" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kabupaten/Kota
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kabupaten-kota' onchange="document.getElementById('text_content2').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_content2" name="KABKOTA" value="" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kecamatan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kecamatan' onchange="document.getElementById('text_content3').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_content3"  name="KEC" value="" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kelurahan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kelurahan-desa'  onchange="document.getElementById('text_content4').value=this.options[this.selectedIndex].text">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_content4" name="KEL" value="" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Pos
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kodepos' name="KODEPOS">
                        <option value='0'>--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_content4" name="KODEPOS" value="" />
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Perihal</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="PERIHAL" value="<?php echo set_value('PERIHAL'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Sebutan Nama Diudara</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="SEBUTANNAMADIUDARA" value="<?php echo set_value('SEBUTANNAMADIUDARA'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Domisili</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="DOMISILI" value="<?php echo set_value('DOMISILI'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Susunan Pengurus</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="SUSUNANPENGURUS" value="<?php echo set_value('SUSUNANPENGURUS'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Anggaran Dasar</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="ANGGARANDASAR" value="<?php echo set_value('ANGGARANDASAR'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Lokasi Pemancar</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="LOKASIPEMANCAR" value="<?php echo set_value('LOKASIPEMANCAR'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Frekuensi Perangkat</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="FREKUENSIPERANGKAT" value="<?php echo set_value('FREKUENSIPERANGKAT'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Wilayah Layanan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="WILAYAHLAYANAN" value="<?php echo set_value('WILAYAHLAYANAN'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Program Siaran</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="PROGRAMSIARAN" value="<?php echo set_value('PROGRAMSIARAN'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">SSJ</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="SSJ" value="<?php echo set_value('SSJ'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Perubahan Data</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="PERUBAHANDATA" value="<?php echo set_value('PERUBAHANDATA'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Data Lama</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="DATALAMA" value="<?php echo set_value('DATALAMA'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Data Baru</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="DATABARU" value="<?php echo set_value('DATABARU'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Lampiran Surat</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="LAMPIRANSURAT" value="<?php echo set_value('LAMPIRANSURAT'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tindak Lanjut Disposisi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="TINDAKLANJUTDISPOSISI" value="<?php echo set_value('TINDAKLANJUTDISPOSISI'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Status TIndak Lanjut</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="STATUSTINDAKLANJUT" value="<?php echo set_value('STATUSTINDAKLANJUT'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Kekurangan Berkas</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="KEKURANGANBERKAS" value="<?php echo set_value('KEKURANGANBERKAS'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Status Kelengkapan Berkas</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="STATUSKELENGKAPANBERKAS" value="<?php echo set_value('STATUSKELENGKAPANBERKAS'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Surat keluar Pemenuhan Kurang Berkas</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="SURATKELUARPEMENUHANKURANGBERKAS" value="<?php echo set_value('SURATKELUARPEMENUHANKURANGBERKAS'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Klarifikasi dan Verifikasi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="KLARIFIKASIDANVERIFIKASI" value="<?php echo set_value('KLARIFIKASIDANVERIFIKASI'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Berita Acara</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="TANGGALBERITAACARAEVALUASIDANVERIFIKASI" value="<?php echo set_value('TANGGALBERITAACARAEVALUASIDANVERIFIKASI'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Tanggapan Surat</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="NOTANGGAPANSURATPPIKELP" value="<?php echo set_value('NOTANGGAPANSURATPPIKELP'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Surat Tanggapan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="TANGGALSURATTANGGAPANSURATPPIKELP" value="<?php echo set_value('TANGGALSURATTANGGAPANSURATPPIKELP'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Tanggapan Surat</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="NOTANGGAPANSURATPPIKESURATSDPPI" value="<?php echo set_value('NOTANGGAPANSURATPPIKESURATSDPPI'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Tanggapan Surat</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="TANGGALTANGGAPANSURATPPIKESURATSDPPI" value="<?php echo set_value('TANGGALTANGGAPANSURATPPIKESURATSDPPI'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Tanggapan Surat</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="NOTANGGAPANSURATSDPPIKESURATPPI" value="<?php echo set_value('NOTANGGAPANSURATSDPPIKESURATPPI'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Tanggapan Surat</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="TANGGALTANGGAPANSURATSDPPIKESURATPPI" value="<?php echo set_value('TANGGALTANGGAPANSURATSDPPIKESURATPPI'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Surat Masuk Terkait</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="SURATMASUKTERKAITTANGGAPANKPIDSDPPI" value="<?php echo set_value('SURATMASUKTERKAITTANGGAPANKPIDSDPPI'); ?>">
                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <a href="<?php echo base_url().'coklit/perubahan' ?>" class="btn btn-primary">Cancel</a>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer>
          <div class="copyright-info">
            <p class="pull-right">SIMP3</p>
          </div>
          <div class="clearfix"></div>
        </footer
      </div>
<script type="text/javascript">
$(function(){

    $.ajaxSetup({
    type:"POST",
    url: "<?php echo base_url('pejabat/ambil_data') ?>",
    cache: false,
    });

    $("#provinsi").change(function(){

    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kabupaten',id:value},
    success: function(respond){
    $("#kabupaten-kota").html(respond);
    }
    })
    }

    });


    $("#kabupaten-kota").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kecamatan',id:value},
    success: function(respond){
    $("#kecamatan").html(respond);
    }
    })
    }
    })

    $("#kecamatan").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kelurahan',id:value},
    success: function(respond){
    $("#kelurahan-desa").html(respond);
    }
    })
    } 
    })

    $("#kecamatan").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kodepos',id:value},
    success: function(respond){
    $("#kodepos").html(respond);
    }
    })
    } 
    })

    })
</script>
<?php $this->load->view('_parts/public_footer_view'); ?>