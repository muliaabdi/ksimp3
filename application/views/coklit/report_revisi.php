<?php $this->load->view('_parts/public_header_view'); ?>
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Konfirmasi Penyelenggara</h3>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Data Penyelenggara</h2>
                   <div class="pull-right">
            <a href="<?php echo base_url().'coklit/revisi/tambah'; ?>" class="btn btn-primary">Tambah</a>
         </div>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <!-- <div class="table-responsive"> -->
                <div class='form-group'>
                        <label class="control-label col-md- col-sm-1 col-xs-12" for="first-name">Permasalahan
                      </label>
                      <div class="col-md-11">
                            <div class="row">
                            <div class="col-md-9 col-sm-12 col-xs-12">
                            <select id="jenis-trouble" name="js-trouble" class="form-control">
                                <option value="">All</option>
                                <?php foreach ($trouble as $row): ?>
                                    <?php if (!empty($row->trouble)): ?>
                                    <option value="<?php echo $row->trouble; ?>"><?php echo $row->trouble; ?></option>                                   
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                            <select id="jenis-status" name="js-status" class="form-control">
                                <option value="">All</option>
                                <option value="0">Belum diperbaiki</option>
                                <option value="1">Sudah Diperbaiki</option>
                            </select>
                            </div>
                        </div>
                        <br /><br />
                      </div>
                </div>
                <div class="row">
                <div class='form-group col-md-6 col-sm-12 col-xs-12'>
                        <label class="control-label col-md-2 col-sm-1 col-xs-12" for="first-name">Provinsi&nbsp;&nbsp;
                      </label>
                      <div class="col-md-10">
                            <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                            <select id="provinsi-kantor" name="prov-kantor" class="form-control">
                                <option value="">All Provinsi</option>
                                <?php foreach ($prov as $row2): ?>
                                    <?php if (!empty($row2->propinsikantor)): ?>
                                    <option value="<?php echo $row2->propinsikantor; ?>"><?php echo $row2->propinsikantor; ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                            </div>
                            <!-- <div class="col-md-6 col-sm-12 col-xs-12">
                            <select id="wilayah-layanan" name="wil-layanan" class="form-control">
                                <option value="">All Wilayah Layanan</option>
                                <?php foreach ($wl as $row3): ?>
                                    <?php if (!empty($row3->wilayahlayanan)): ?>
                                    <option value="<?php echo $row3->wilayahlayanan; ?>"><?php echo $row3->wilayahlayanan; ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                            </div> -->
                        </div>
                        <br /><br />
                      </div>
                </div>
                <div class='form-group col-md-6 col-sm-12 col-xs-12'>
                        <label class="control-label col-md- col-sm-1 col-xs-12" for="first-name">Kirim Surat
                      </label>
                      <div class="col-md-11">
                            <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                            <select id="status-kirim" name="status-kirim" class="form-control">
                                <option value="">All</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                            </div>
                        </div>
                        <br /><br />
                      </div>
                </div>
                </div>
                    <table class="table table-bordered table-striped table-hover" id="mytable">
                        <thead>
                            <tr>
                              <th width="50px">S/S</th>
                              <th>ACTION</th>
                              <th>SYSID</th>
                              <th>ID</th>
                              <th>COMPANY NAME</th>
                              <th>TROUBLE</th>
                              <th>NAMA IZIN</th>
                              <th>ALAMAT</th>
                              <th>KELURAHAN</th>
                              <th>KECAMATAN</th>
                              <th>KOTA</th>
                              <th>PROVINSI</th>
                              <th>ZONA</th>
                              <th>WL</th>
                              <th>STATUS</th>
                              <th>KIRIM</th>
                              <th>AUDITOR</th>
                            </tr>
                        </thead>
                    </table>
                    <!-- </div> -->
                </div>
              </div>
            </div>
            <br />
            <br />
            <br />

          </div>
        </div>
        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">SIMP3</p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
  <script type="text/javascript">
  function format ( d ) {
    return  'Source : '+d.SOURCE+'<br>'+
            'Sheet  : '+d.SHEET;
  }
            $(document).ready(function () {
 
                // $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
                // {
                //     return {
                //         "iStart": oSettings._iDisplayStart,
                //         "iEnd": oSettings.fnDisplayEnd(),
                //         "iLength": oSettings._iDisplayLength,
                //         "iTotal": oSettings.fnRecordsTotal(),
                //         "iFilteredTotal": oSettings.fnRecordsDisplay(),
                //         "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                //         "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                //     };
                // };
                $.fn.dataTable.ext.errMode = 'throw';
                var dt = $('#mytable').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "scrollX": true,
                    // "bSort" : false,
                    "oLanguage": {
                       "sSearch": "Search "
                     },
                    "ajax": "<?php echo base_url('coklit/revisi/ajax_revisi'); ?>",
                    "columns": [
                        {
                            "class":          "details-control",
                            "orderable":      false,
                            "data":           null,
                            "defaultContent": ""
                        },
                        {
                            "class": "text-center",
                            "orderable":      false,
                            "data": "ACTION"
                        },
                        {"data": "SYSID"},
                        {"data": "ID"},
                        {"data": "COMPANY NAME"},
                        {"data": "TROUBLE"},
                        {"data": "NAMA IZIN"},
                        {"data": "ALAMAT"},
                        {"data": "KELURAHAN"},
                        {"data": "KECAMATAN"},
                        {"data": "KOTA"},
                        {"data": "PROVINSI"},
                        {"data": "ZONA"},
                        {
                            "data": "wl",
                            "visible": false
                        },
                        {
                            "data": "STATUS",
                            "visible": false
                        },
                        {
                            "data": "KIRIM",
                            "visible": false
                        },
                        {"data": "AUDITOR"}
                    ],
                    "order": [[1, 'asc']],
                    "order": [[2, 'asc']],
                    // "rowCallback": function (row, data, iDisplayIndex) {
                    //     var info = this.fnPagingInfo();
                    //     var page = info.iPage;
                    //     var length = info.iLength;
                    //     var index = page * length + (iDisplayIndex + 1);
                    //     $('td:eq(0)', row).html(index);
                    // }
                });
                $('#jenis-trouble').change( function () { 
                    $('#mytable').dataTable().fnFilter($('#jenis-trouble').val(), 5, true, false);
                    $('#mytable').dataTable().fnFilter("0", 14, true);
                    document.getElementById('jenis-status').value = '0';
                });
                $('#jenis-status').change( function () { 
                    $('#mytable').dataTable().fnFilter($('#jenis-status').val(), 14, true, false);
                });
                $('#provinsi-kantor').change( function () { 
                    $('#mytable').dataTable().fnFilter($('#provinsi-kantor').val(), 11, true, false);
                });
                $('#wilayah-layanan').change( function () { 
                    $('#mytable').dataTable().fnFilter($('#wilayah-layanan').val(), 13, true, false);
                });
                $('#status-kirim').change( function () { 
                    $('#mytable').dataTable().fnFilter($('#status-kirim').val(), 15, true, false);
                });
                var detailRows = [];
 
    $('#mytable tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();
 
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );
 
    // On each draw, loop over the `detailRows` array and show any child rows
    dt.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );

            });
        </script>
<?php $this->load->view('_parts/public_footer_view'); ?>