<?php $this->load->view('_parts/public_header_view'); ?>

<div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_right">
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Register</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                 <?php if($this->session->flashdata('message')) {?>
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></span>
                        </button>
                        <strong>Information!</strong> <?php echo $this->session->flashdata('message');?>
                    </div>
                <?php } ?>
                  <form id="demo-form2" method="post" action="<?php echo base_url().'register'?>" class="form-horizontal form-label-left">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ID</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="id" value="<?php echo set_value('id'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Company Name</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="nmlembaga" value="<?php echo set_value('nmlembaga'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">On Air Name</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="last-name" name="nmudara" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('nmudara'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">ID Pemohon</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="idpemohon" value="<?php echo set_value('idpemohon'); ?>">
                      </div>
                    </div>
                    <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Izin
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='nmijin' name="nmijin">
                        <option>--pilih--</option>
                        <option>LPP LOKAL RADIO</option>
                        <option>LPP LOKAL TELEVISI</option>
                        <option>LPS RADIO</option>
                        <option>LPS TELEVISI</option>
                        <option>LPK RADIO</option>
                        <option>LPK TELEVISI</option>
                        <option>LPB TELEVISI</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Area of Service</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="wilayahlayanan" value="<?php echo set_value('wilayahlayanan'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Sub Service</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nmmodulasi" value="<?php echo set_value('nmmodulasi'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Kependudukan Stasiun Penyiaran</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="kedudukanstasiunpenyiarankhususlpsindukjaringanggotajaringan" value="<?php echo set_value('kedudukanstasiunpenyiarankhususlpsindukjaringanggotajaringan'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <Legend class="col-md-12">Informasi Kontak</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Kontak</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nmkontak" value="<?php echo set_value('nmkontak'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Telepon Kontak</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="notelpkontak" value="<?php echo set_value('notelpkontak'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No FAX Kontak</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nofaxkontak" value="<?php echo set_value('nofaxkontak'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No HP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nohpkontak" value="<?php echo set_value('nohpkontak'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="emailkontak" value="<?php echo set_value('emailkontak'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Bukti Surat Penunjukan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="buktisuratpenunjukan" value="<?php echo set_value('buktisuratpenunjukan'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <Legend class="col-md-12">Alamat Kantor</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Kantor</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="middle-name" class="form-control col-md-7 col-xs-12" name="jlnkantor"></textarea>
                      </div>
                    </div>
                    <div class='form-group'>
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Provinsi
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='provinsi' onchange="document.getElementById('text_content').value=this.options[this.selectedIndex].text">
                        <option value="0">--pilih--</option>
                        <?php 
                        foreach ($provinsi as $prov) {
                        echo "<option value='$prov[PROVID]'>$prov[PROV]</option>";
                        }
                        ?>
                        </select>
                      </div>
                    </div>
                      <input type="hidden" id="text_content" name="propinsikantor" value="<?php echo set_value('propinsikantor'); ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kabupaten/Kota
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kabupaten-kota' onchange="document.getElementById('text_content2').value=this.options[this.selectedIndex].text">
                        <option value="0">--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_content2" name="kabkotakantor" value="<?php echo set_value('kabkotakantor'); ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kecamatan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kecamatan' onchange="document.getElementById('text_content3').value=this.options[this.selectedIndex].text">
                        <option value="0">--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_content3"  name="kecamatankantor" value="<?php echo set_value('kecamatankantor'); ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kelurahan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kelurahan-desa'  onchange="document.getElementById('text_content4').value=this.options[this.selectedIndex].text">
                        <option value="0">--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_content4" name="kelurahankantor" value="<?php echo set_value('kelurahankantor'); ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Pos
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kodepos' name="kodeposkantor">
                        <option value="0">--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Zona
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='zona' name="zona">
                        <option value="0">--pilih--</option>
                        </select>
                        </div>
                      </div>
                    <div class="form-group">
                        <Legend class="col-md-12">Alamat Studio</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Studio</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea class="form-control col-md-7 col-xs-12" name="jlnstudio"><?php echo set_value('jlnstudio'); ?></textarea>
                      </div>
                    </div>
                    <div class='form-group'>
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Provinsi
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='provinsistudio' onchange="document.getElementById('text_provinsistudio').value=this.options[this.selectedIndex].text">
                        <option value="0">--pilih--</option>
                        <?php 
                        foreach ($provinsi as $prov) {
                        echo "<option value='$prov[PROVID]'>$prov[PROV]</option>";
                        }
                        ?>
                        </select>
                      </div>
                    </div>
                      <input type="hidden" id="text_provinsistudio" name="propinsistudio" value="<?php echo set_value('propinsistudio'); ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kabupaten/Kota
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kabkotastudio' onchange="document.getElementById('text_kabkotastudio').value=this.options[this.selectedIndex].text">
                        <option value="0">--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_kabkotastudio" name="kabkotastudio" value="<?php echo set_value('kabkotastudio'); ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kecamatan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kecamatanstudio' onchange="document.getElementById('text_kecamatanstudio').value=this.options[this.selectedIndex].text">
                        <option value="0">--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_kecamatanstudio"  name="kecamatanstudio" value="<?php echo set_value('kecamatanstudio'); ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kelurahan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kelurahanstudio'  onchange="document.getElementById('text_kelurahanstudio').value=this.options[this.selectedIndex].text">
                        <option value="0">--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_kelurahanstudio" name="kelurahanstudio" value="<?php echo set_value('kelurahanstudio'); ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Pos
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kodeposstudio' name="kodeposstudio">
                        <option value="0">--pilih--</option>
                        </select>
                        </div>
                      </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Telepon</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="notelpstudio" value="<?php echo set_value('notelpstudio'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No FAX</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nofaxstudio" value="<?php echo set_value('nofaxstudio'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <Legend class="col-md-12">Alamat Stasiun Pemancar</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Stasiun Pemancar</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nmstapemancar" value="<?php echo set_value('nmstapemancar'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Pemancar</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="middle-name" class="form-control col-md-7 col-xs-12" name="alamatpemancar"><?php echo set_value('alamatpemancar'); ?></textarea>
                      </div>
                    </div>
                    <div class='form-group'>
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Provinsi
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='provinsipemanc' onchange="document.getElementById('text_provinsipemanc').value=this.options[this.selectedIndex].text">
                        <option value="0">--pilih--</option>
                        <?php 
                        foreach ($provinsi as $prov) {
                        echo "<option value='$prov[PROVID]'>$prov[PROV]</option>";
                        }
                        ?>
                        </select>
                      </div>
                    </div>
                      <input type="hidden" id="text_provinsipemanc" name="provinsipemancar" value="<?php echo set_value('provinsipemancar'); ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kabupaten/Kota
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kabkotapemanc' onchange="document.getElementById('text_kabkotapemanc').value=this.options[this.selectedIndex].text">
                        <option value="0">--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_kabkotapemanc" name="kabkotapemancar" value="<?php echo set_value('kabkotapemancar'); ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kecamatan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kecamatanpemanc' onchange="document.getElementById('text_kecamatanpemanc').value=this.options[this.selectedIndex].text">
                        <option value="0">--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_kecamatanpemanc"  name="kecamatanpemancar" value="<?php echo set_value('kecamatanpemancar'); ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kelurahan
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kelurahanpemanc'  onchange="document.getElementById('text_kelurahanpemanc').value=this.options[this.selectedIndex].text">
                        <option value="0">--pilih--</option>
                        </select>
                        </div>
                      </div>
                      <input type="hidden" id="text_kelurahanpemanc" name="kelurahanpemancar" value="<?php echo set_value('kelurahanpemancar'); ?>" />
                      <div class='form-group'>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Pos
                      </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class='form-control' id='kodepospemanc' name="kodepospemancar">
                        <option value="0">--pilih--</option>
                        </select>
                        </div>
                      </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">NPWP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="npwp" value="<?php echo set_value('npwp'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <Legend class="col-md-12">Domisili</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Surat</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nosuketdomisiliusaha" value="<?php echo set_value('nosuketdomisiliusaha'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Surat</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker" class="datepick form-control col-md-7 col-xs-12" type="text" name="tglsuketdomisiliusaha" value="<?php echo set_value('tglsuketdomisiliusaha'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Instansi Penerbit</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nminstansipenerbitdomisiliusaha" value="<?php echo set_value('nminstansipenerbitdomisiliusaha'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Akta Pendirian</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noaktapendirian" value="<?php echo set_value('noaktapendirian'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Akta Pendirian</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker2" class="datepick form-control col-md-7 col-xs-12" type="text" name="tglaktapendirian" value="<?php echo set_value('tglaktapendirian'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Notaris</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nmnotarisaktapendirian" value="<?php echo set_value('nmnotarisaktapendirian'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Domisili Notaris</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="domisilinotarisaktapendirian" value="<?php echo set_value('domisilinotarisaktapendirian'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Pengesahan Akta</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nopengesahanakta" value="<?php echo set_value('nopengesahanakta'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Pengesahan Akta</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker3" class="form-control col-md-7 col-xs-12" type="text" name="tglpengesahanakta" value="<?php echo set_value('tglpengesahanakta'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Penerbit Pengesahan Akta</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nminstansipenerbitpengesahanakta" value="<?php echo set_value('nminstansipenerbitpengesahanakta'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <Legend class="col-md-12">SITU</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No SITU</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nositu" value="<?php echo set_value('nositu'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal SITU</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker4" class="form-control col-md-7 col-xs-12" type="text" name="tglsitu" value="<?php echo set_value('tglsitu'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Instansi Penerbit SITU</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nminstansipenerbitsitu" value="<?php echo set_value('nminstansipenerbitsitu'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <Legend class="col-md-12">TDP</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No TDP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="notdp" value="<?php echo set_value('notdp'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal TDP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker5" class="form-control col-md-7 col-xs-12" type="text" name="tgltdp" value="<?php echo set_value('tgltdp'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <Legend class="col-md-12">HO</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No HO</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noho" value="<?php echo set_value('noho'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal HO</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker6" class="form-control col-md-7 col-xs-12" type="text" name="tglho" value="<?php echo set_value('tglho'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Instansi Penerbit HO</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nminstansipenerbitho" value="<?php echo set_value('nminstansipenerbitho'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <Legend class="col-md-12">IMB</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IMB</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noimb" value="<?php echo set_value('noimb'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IMB</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker7" class="form-control col-md-7 col-xs-12" type="text" name="tglimb" value="<?php echo set_value('tglimb'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Instansi Penerbit IMB</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nminstansipenerbitimb" value="<?php echo set_value('nminstansipenerbitimb'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IMB Menara</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noimbm" value="<?php echo set_value('noimbm'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IMB Menara</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker8" class="form-control col-md-7 col-xs-12" type="text" name="tglimbm" value="<?php echo set_value('tglimbm'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Instansi Penerbit IMB Menara</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nminstansipenerbitimbm" value="<?php echo set_value('nminstansipenerbitimbm'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">Akta Perubahan Akhir</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Akta Perubahan Akhir</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noaktaperubahanakhir" value="<?php echo set_value('noaktaperubahanakhir'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Akta Perubahan Akhir</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker9" class="form-control col-md-7 col-xs-12" type="text" name="tglaktaperubahanakhir" value="<?php echo set_value('tglaktaperubahanakhir'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Notaris Akta Perubahan Akhir</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nmnotarisaktaperubahanakhir" value="<?php echo set_value('nmnotarisaktaperubahanakhir'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Domisili Akta Perubahan Akhir</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="domisiliaktaperubahanakhir" value="<?php echo set_value('domisiliaktaperubahanakhir'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Akta Sah Perubahan Akhir</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noaktasahperubahanakhir" value="<?php echo set_value('noaktasahperubahanakhir'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Akta Sah Perubahan Akhir</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker10" class="form-control col-md-7 col-xs-12" type="text" name="tglaktasahperubahanakhir" value="<?php echo set_value('tglaktasahperubahanakhir'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Instansi Penerbit</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nminstansisahpenerbitakhir" value="<?php echo set_value('nminstansisahpenerbitakhir'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Rekomendasi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker11" class="form-control col-md-7 col-xs-12" type="text" name="tglrekomendasi" value="<?php echo set_value('tglrekomendasi'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">FRB</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Pra FRB</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker12" class="form-control col-md-7 col-xs-12" type="text" name="tglprafrb" value="<?php echo set_value('tglprafrb'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal FRB</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker13" class="form-control col-md-7 col-xs-12" type="text" name="tglfrb" value="<?php echo set_value('tglfrb'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Seleksi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker14" class="form-control col-md-7 col-xs-12" type="text" name="tglseleksi" value="<?php echo set_value('tglseleksi'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal EUCS</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker15" class="form-control col-md-7 col-xs-12" type="text" name="tgleucs" value="<?php echo set_value('tgleucs'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IPP Expiry</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker16" class="form-control col-md-7 col-xs-12" type="text" name="tglippexpiry" value="<?php echo set_value('tglippexpiry'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No BAFRB</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nobafrb" value="<?php echo set_value('nobafrb'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">EUCS</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="eucs" value="<?php echo set_value('eucs'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Status EUCS</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="statuseucs" value="<?php echo set_value('statuseucs'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Status Verifikasi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="statusverifikasi" value="<?php echo set_value('statusverifikasi'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Surat Persetujuan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nosuratpersetujuan" value="<?php echo set_value('nosuratpersetujuan'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Perubahan Data Sebelum</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="perubahandatasebelum" value="<?php echo set_value('perubahandatasebelum'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Perubahan Data Sesudah</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="perubahandatasesudah" value="<?php echo set_value('perubahandatasesudah'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Perihal Perubahan Data</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="perihalperubahandata" value="<?php echo set_value('perihalperubahandata'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">IPP Prinsip</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IPP Prinsip</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nomorippprinsip" value="<?php echo set_value('nomorippprinsip'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IPP Prinsip</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker17" class="form-control col-md-7 col-xs-12" type="text" name="tanggalippprinsip" value="<?php echo set_value('tanggalippprinsip'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Masa Berlaku IPP Prinsip</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker24" class="form-control col-md-7 col-xs-12" type="text" name="masaberlakuippprinsip" value="<?php echo set_value('masaberlakuippprinsip'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No Perpanjangan IPP Prinsip</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nomorperpanjanganippprinsip" value="<?php echo set_value('nomorperpanjanganippprinsip'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Perpanjangan IPP Prinsip</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker18" class="form-control col-md-7 col-xs-12" type="text" name="tanggalperpanjanganippprinsip" value="<?php echo set_value('tanggalperpanjanganippprinsip'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Masa Berlaku Perpanjangan IPP Prinsip</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker25" class="form-control col-md-7 col-xs-12" type="text" name="masaberlakuperpanjanganipprinsip" value="<?php echo set_value('masaberlakuperpanjanganipprinsip'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">IPP Tetap</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IPP Tetap</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noipptetap" value="<?php echo set_value('noipptetap'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IPP Tetap</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker19" class="form-control col-md-7 col-xs-12" type="text" name="tglipptetap" value="<?php echo set_value('tglipptetap'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Masa Berlaku IPP Tetap</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker26" class="form-control col-md-7 col-xs-12" type="text" name="masaberlakuipptetap" value="<?php echo set_value('masaberlakuipptetap'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">IPP Perpanjangan</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IPP Perpanjangan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="nomorippperpanjangan5tahun" value="<?php echo set_value('nomorippperpanjangan5tahun'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IPP Perpanjangan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker20" class="form-control col-md-7 col-xs-12" type="text" name="tanggalippperpanjangan5tahun" value="<?php echo set_value('tanggalippperpanjangan5tahun'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Masa Berlaku IPP Perpanjangan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker27" class="form-control col-md-7 col-xs-12" type="text" name="masaberlakuippperpanjangan5tahun" value="<?php echo set_value('masaberlakuippperpanjangan5tahun'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">IPP Penyesuaian</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IPP Penyesuaian</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noipppenyesuaian" value="<?php echo set_value('noipppenyesuaian'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IPP Penyesuaian</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker21" class="form-control col-md-7 col-xs-12" type="text" name="tglipppenyesuaian" value="<?php echo set_value('tglipppenyesuaian'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Masa Berlaku IPP Penyesuaian</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker28" class="form-control col-md-7 col-xs-12" type="text" name="masaberlakuipppenyesuaian" value="<?php echo set_value('masaberlakuipppenyesuaian'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">IPP Migrasi</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">No IPP Migrasi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="noipppenyesuaianamfm" value="<?php echo set_value('noipppenyesuaianamfm'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal IPP Migrasi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker22" class="form-control col-md-7 col-xs-12" type="text" name="tglipppenyesuaianamfm" value="<?php echo set_value('tglipppenyesuaianamfm'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Masa Berlaku IPP Migrasi</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker29" class="form-control col-md-7 col-xs-12" type="text" name="masaberlakuipppenyesuainamfm" value="<?php echo set_value('masaberlakuipppenyesuainamfm'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Status Akhir</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="statusterakhir" value="<?php echo set_value('statusterakhir'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">SPP</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Kode SPP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="kodebiaya" value="<?php echo set_value('kodebiaya'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">ID SPP</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="idspp" value="<?php echo set_value('idspp'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Saldo Bayar</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="saldobayar" value="<?php echo set_value('saldobayar'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Bayar</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="datepicker23" class="form-control col-md-7 col-xs-12" type="text" name="tglbayar" value="<?php echo set_value('tglbayar'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Catatan Konsultan</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="keterangan1" value="<?php echo set_value('internalnote'); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                        <legend class="col-md-12">Note</legend>
                    </div>
                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Catatan Kominfo</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="middle-name" class="form-control col-md-7 col-xs-12" name="catatan"></textarea>
                      </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <a href="<?php echo base_url().'coklit/revisi' ?>" class="btn btn-primary">Cancel</a>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer>
          <div class="copyright-info">
            <p class="pull-right">SIMP3</p>
          </div>
          <div class="clearfix"></div>
        </footer
      </div>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">
$(function(){

    $.ajaxSetup({
    type:"POST",
    url: "<?php echo base_url('register/ambil_data') ?>",
    cache: false,
    });

    //kantor
    $("#provinsi").change(function(){

    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kabupaten',id:value},
    success: function(respond){
    $("#kabupaten-kota").html(respond);
    }
    })
    }

    });


    $("#kabupaten-kota").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kecamatan',id:value},
    success: function(respond){
    $("#kecamatan").html(respond);
    }
    })
    }
    })

    $("#kecamatan").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kelurahan',id:value},
    success: function(respond){
    $("#kelurahan-desa").html(respond);
    }
    })
    } 
    })

    $("#kecamatan").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kodepos',id:value},
    success: function(respond){
    $("#kodepos").html(respond);
    }
    })
    } 
    })

    $("#kabupaten-kota").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'zona',id:value},
    success: function(respond){
    $("#zona").html(respond);
    }
    })
    } 
    })

    //studio
    $("#provinsistudio").change(function(){

    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kabupaten',id:value},
    success: function(respond){
    $("#kabkotastudio").html(respond);
    }
    })
    }

    });


    $("#kabkotastudio").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kecamatan',id:value},
    success: function(respond){
    $("#kecamatanstudio").html(respond);
    }
    })
    }
    })

    $("#kecamatanstudio").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kelurahan',id:value},
    success: function(respond){
    $("#kelurahanstudio").html(respond);
    }
    })
    } 
    })

    $("#kecamatanstudio").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kodepos',id:value},
    success: function(respond){
    $("#kodeposstudio").html(respond);
    }
    })
    } 
    })

    //pemancar
    $("#provinsipemanc").change(function(){

    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kabupaten',id:value},
    success: function(respond){
    $("#kabkotapemanc").html(respond);
    }
    })
    }

    });


    $("#kabkotapemanc").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kecamatan',id:value},
    success: function(respond){
    $("#kecamatanpemanc").html(respond);
    }
    })
    }
    })

    $("#kecamatanpemanc").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kelurahan',id:value},
    success: function(respond){
    $("#kelurahanpemanc").html(respond);
    }
    })
    } 
    })

    $("#kecamatanpemanc").change(function(){
    var value=$(this).val();
    if(value>0){
    $.ajax({
    data:{modul:'kodepos',id:value},
    success: function(respond){
    $("#kodepospemanc").html(respond);
    }
    })
    } 
    })
    })
$(function() {
    $('#datepicker,#datepicker2,#datepicker3,#datepicker4,#datepicker5,#datepicker6,#datepicker7,#datepicker8,#datepicker9,#datepicker10,#datepicker11,#datepicker12,#datepicker13,#datepicker14,#datepicker15,#datepicker16,#datepicker17,#datepicker18,#datepicker19,#datepicker20,#datepicker21,#datepicker22,#datepicker23,#datepicker24,#datepicker25,#datepicker26,#datepicker27,#datepicker28,#datepicker29').each(function(){
        $(this).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd'
        });
    });
});
  </script>
<?php $this->load->view('_parts/public_footer_view'); ?>