<?php $this->load->view('_parts/public_header_view'); ?>
<style type="text/css">
@import url(http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic);
html {
  box-sizing: border-box;
}

*, *:before, *:after {
  box-sizing: inherit;
}

body {
  font-family: lato;
}

.section-header, .steps-header, .steps-name {
  color: #3498DB;
  font-weight: 400;
  font-size: 1.4em;
}

.steps-header {
  margin-bottom: 20px;
  text-align: center;
}

.steps-timeline {
  outline: 1px dashed rgba(255, 0, 0, 0);
}
@media screen and (max-width: 500px) {
  .steps-timeline {
    border-left: 2px solid #3498DB;
    margin-left: 25px;
  }
}
@media screen and (min-width: 500px) {
  .steps-timeline {
    border-top: 2px solid #3498DB;
    padding-top: 24px;
    margin-top: 40px;
    margin-left: 16.65%;
    margin-right: 16.65%;
  }
}
.steps-timeline:after {
  content: "";
  display: table;
  clear: both;
}

.steps-one,
.steps-two,
.steps-three {
  outline: 1px dashed rgba(0, 128, 0, 0);
}
@media screen and (max-width: 500px) {
  .steps-one,
  .steps-two,
  .steps-three {
    margin-left: -25px;
  }
}
@media screen and (min-width: 500px) {
  .steps-one,
  .steps-two,
  .steps-three {
    float: left;
    width: 33%;
    margin-top: -50px;
  }
}

@media screen and (max-width: 500px) {
  .steps-one,
  .steps-two {
    padding-bottom: 40px;
  }
}

@media screen and (min-width: 500px) {
  .steps-one {
    margin-left: -16.65%;
    margin-right: 16.65%;
  }
}

@media screen and (max-width: 500px) {
  .steps-three {
    margin-bottom: -100%;
  }
}
@media screen and (min-width: 500px) {
  .steps-three {
    margin-left: 16.65%;
    margin-right: -16.65%;
  }
}

.steps-img {
  display: block;
  margin: auto;
  width: 50px;
  height: 50px;
  border-radius: 50%;
}
@media screen and (max-width: 500px) {
  .steps-img {
    float: left;
    margin-right: 20px;
  }
}

.steps-name,
.steps-description {
  margin: 0;
}

@media screen and (min-width: 500px) {
  .steps-name {
    text-align: center;
  }
}

.steps-description {
  overflow: hidden;
}
@media screen and (min-width: 500px) {
  .steps-description {
    text-align: center;
  }
}
</style>
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Selamat Datang</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <div class="content-wrapper">
  <h2>Cara melakukan pemutakhiran data COKLIT (Data TV dan Radio)</h2>
<!-- <div class="jumbotron">
  <p>Selamat Datang di SIMP3 Pemutakhiran</p>
</div> -->
  <!-- STEPS -->
  <section id="Steps" class="steps-section">

    <!-- <h2 class="steps-header">
      Responsive Semantic Timeline
    </h2> -->

    <div class="steps-timeline">

      <div class="steps-one">
        <img class="steps-img" src="https://cdn1.iconfinder.com/data/icons/basic-ui-elements-color/700/01_arrow_right-128.png" alt="" />
        <h3 class="steps-name">
          <a href="<?php echo base_url(); ?>coklit/revisi">COKLIT</a>
        </h3>
        <!-- <p class="steps-description">
          The timeline is created using negative margins and a top border.
        </p> -->
      </div>

      <div class="steps-two">
        <img class="steps-img" src="https://cdn0.iconfinder.com/data/icons/rcorners/512/Pin-128.png" alt="" />
        <h3 class="steps-name">
           <a href="#" class="btn disabled" >Pilih Permasalahan</a>
        </h3>
        <!-- <p class="steps-description">
           All elements are positioned realtive to the parent. No absolute positioning.
        </p> -->
      </div>

      <div class="steps-three">
        <img class="steps-img" src="http://www.xs-labs.com/uploads/image/clang-kit/icon-circle.png" alt="" />
        <h3 class="steps-name">
           <a href="#" class="btn disabled">Edit Data Perusahaan</a>
        </h3>
        <!-- <p class="steps-description">
           The timeline does not extend past the first and last elements.
        </p> -->
      </div>

    </div><!-- /.steps-timeline -->

  </section>
  <hr>
  <h2>Cara melakukan pemutakhiran data EUCS</h2>
  <section id="Steps" class="steps-section">
    <div class="steps-timeline">
      <div class="steps-one">
        <img class="steps-img" src="https://cdn1.iconfinder.com/data/icons/basic-ui-elements-color/700/01_arrow_right-128.png" alt="" />
        <h3 class="steps-name">
          <a href="<?php echo base_url(); ?>coklit/eucs">EUCS</a>
        </h3>
<!--         <p class="steps-description">
          The timeline is created using negative margins and a top border.
        </p> -->
      </div>

      <div class="steps-two">
        <img class="steps-img" src="https://cdn1.iconfinder.com/data/icons/basic-ui-elements-color/700/01_arrow_right-128.png" alt="" />
        <h3 class="steps-name">
           <a href="#" class="btn disabled" >Pilih Perusahaan</a>
        </h3>
        <!-- <p class="steps-description">
           All elements are positioned realtive to the parent. No absolute positioning.
        </p> -->
      </div>

      <div class="steps-three">
        <img class="steps-img" src="https://cdn1.iconfinder.com/data/icons/basic-ui-elements-color/700/01_arrow_right-128.png" alt="" />
        <h3 class="steps-name">
           <a href="#" class="btn disabled">Edit Data</a>
        </h3>
        <!-- <p class="steps-description">
           The timeline does not extend past the first and last elements.
        </p> -->
      </div>

    </div><!-- /.steps-timeline -->

  </section>

</div>
                </div>
              </div>
            </div>
            <br />
            <br />
            <br />

          </div>
        </div>
        <!-- footer content -->
        <footer>
          <div class="copyright-info">
            <p class="pull-right">SIMP3</p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
<?php $this->load->view('_parts/public_footer_view'); ?>