<?php
function load_partials($file)
{
    if(file_exists(APPPATH . 'views/partials/' . $file)) {
            require_once APPPATH . 'views/partials/' . $file;
    }
}

function logged_in()
{
    $session = $_SESSION;

    if(!empty($session['identity'])) {
            return TRUE;
    }

    return FALSE;
}

function is_admin() {
    $ci =& get_instance();
    $session = $_SESSION;
    
    if($ci->ion_auth->is_admin($session['user_id'])) {
        return TRUE;
    }
    
    return FALSE;
}

function is_editor() {
    $ci =& get_instance();
    $session = $_SESSION;
    
    if($ci->ion_auth->in_group(4,$session['user_id'])) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function is_contributor() {
    $ci =& get_instance();
    $session = $_SESSION;
    
    if($ci->ion_auth->in_group(3,$session['user_id'])) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function is_reviewer() {
    $ci =& get_instance();
    $session = $_SESSION;
    
    if($ci->ion_auth->in_group(5,$session['user_id'])) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function is_member() {
    $ci =& get_instance();
    $session = $_SESSION;
    
    if($ci->ion_auth->in_group(2,$session['user_id'])) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function current_user() {
    $session = $_SESSION;
    
    return $session['user_id'];
}

function current_company() {
    $session = $_SESSION;
    
    return $session['comp_id'];
}

function current_department() {
    $session = $_SESSION;
    
    return $session['dep_id'];
}

function dashboard_count_document($user_id, $comp_id, $dep_id, $type, $is_read='-99') {
    $ci =& get_instance();
    
    $document_num_rows = "";
    // type
    // 1. contributor, author or admin
    // 2. employee
    if($type == 1) {
        // is_read
        // 300 : unread
        // 301 : read
        // 302 : new
        switch ($is_read) {
            case '302' : 
                $document_num_rows = $ci->db->where("approvals.status","502")
                    ->get("approvals")->num_rows();
                break;
            case '301' : 
                $document_num_rows = $ci->db->where("approvals.status","504")
                    ->get("approvals")->num_rows();
                break;
            case '300' : 
                $document_num_rows = $ci->db->join("approvals","documents.doc_id = approvals.doc_id","left")
                    ->where("approvals.doc_id is null")
                    ->get("documents")->num_rows();
                break;
            default : 
                $document_num_rows = $ci->db->get("documents")->num_rows();
                break;
        }
    } else {
        // is_read
        // 300 : unread
        // 301 : read
        // 302 : new
        switch ($is_read) {
            case '-99' : $auxWhere = " permissions.is_read IN ('300','301','302')"; break;
            case '300' : $auxWhere = "(permissions.is_read = '300' OR permissions.is_read = '302')"; break;// " = '300'"; break;
            case '301' : $auxWhere = " permissions.is_read = '301'"; break;
            case '302' : $auxWhere = "(permissions.is_read = '300' OR permissions.is_read = '302')"; break;// " = '302'"; break;
            default : $auxWhere = " permissions.is_read = '" . $is_read . "'"; break;
        }
        
        $query_num_rows = $ci->db->query("SELECT COUNT(perm_id) AS crow FROM (
                                            SELECT 
                                                    perm_id
                                            FROM 
                                                    permissions 
                                            WHERE 
                                                    " . $auxWhere ." AND 
                                                    (permissions.user_id = " . $user_id ." OR permissions.user_id = 0) AND
                                                    (permissions.dep_id = " . $dep_id . " OR permissions.dep_id = 0) AND
                                                    (permissions.comp_id = ". $comp_id ." OR permissions.comp_id = NULL)
                                            UNION
                                            SELECT 
                                                    perm_id
                                            FROM 
                                                    permissions 
                                            WHERE 
                                                    " . $auxWhere ." AND 
                                                    (permissions.user_id = " . $user_id ." OR permissions.user_id = 0)) AS subquery_tbl")
                                ->row();
        
        $document_num_rows = $query_num_rows->crow;
    }
    
    return $document_num_rows;
}

function trans_user($user_id) {
    $ci =& get_instance();
    
    $user = $ci->db->select('CONCAT(first_name," ",last_name) AS name')
                ->where('id',$user_id)->get('users')->result();
    
    return $user[0]->name;
}

// DEPRECATED POSIBILITY
function have_authority() {
    $ci =& get_instance();
    $session = $_SESSION;
    
    $result = $ci->db->select('id')->where('user_id = ' . $session['user_id'] . ' AND group_id IN (1,2,3,4,5)')->get('users_groups')->num_rows();
    
    $result > 0 ? $have_authority = TRUE : $have_authority = FALSE;
    
    return $have_authority;
}

function is_allowed($user_id,$permission,$doc_id) {
    $ci =& get_instance();
    
    $is_allowed = $ci->db->where(array('user_id'=>$user_id,'permission'=>$permission,'doc_id'=>$doc_id))
            ->get('permissions')->num_rows();
    
    $is_allowed > 0 ? $result = TRUE : $result = FALSE;
    
    return $result;
}

function get_buttons($id,$controller) {
    $ci = & get_instance();
        
    $html = "<span class='actions pull-right'>";
    $html .= "<a title='Edit Data' data-placement='right' data-toggle='tooltip' class='btn btn-default btn-xs' href='" . site_url($controller.'/edit/'.$id) . "'><i class='fa fa-pencil'></i></a>";
    $html .= "&nbsp;&nbsp;";
    $html .= "<button type='button' title='Delete Data' data-placement='right' data-toggle='tooltip' class='btn btn-warning btn-xs delete-btn' onClick='toggleModal($id,\"$controller\")'><i class='fa fa-times'></i></button>";
    $html .= "</span>";
 
    return $html;
}

function get_permission_buttons($id,$controller) {
    $ci = & get_instance();
        
    $html = "<span class='actions pull-right'>";
    $html .= "<a title='Edit Data' data-placement='right' data-toggle='tooltip' class='btn btn-default btn-xs' href='" . site_url($controller.'/edit/'.$id) . "' style='width: 50px;'><i class='fa fa-pencil'></i></a>";
//    $html .= "&nbsp;&nbsp;";
//    $html .= "<button type='button' title='Delete Data' data-placement='right' data-toggle='tooltip' class='btn btn-warning btn-xs delete-btn' onClick='toggleModal($id,\"$controller\")'><i class='fa fa-times'></i></button>";
    $html .= "</span>";
 
    return $html;
}

function trans_status($status) {
    // ngambil dari database nantinya
    switch($status) {
        case '501' : $sStatus = 'Unpublished'; break;
        case '502' : $sStatus = 'Published'; break;
        case '503' : $sStatus = 'Rejected'; break;
        case '504' : $sStatus = 'Reviewed'; break;
        case '505' : $sStatus = 'Draft'; break;
    }
    
    return $sStatus;
}

function trans_permission($permission) {
    // ngambil dari database nantinya
    switch($permission) {
        case '401' : $sPermission = 'Edit'; break;
        case '402' : $sPermission = 'Download'; break;
        case '403' : $sPermission = 'View'; break;
    }
    
    return $sPermission;
}

function trans_filename($doc_id) {
    $ci = & get_instance();
    
    $file_name = $ci->db->select('documents.doc_name')->where('documents.doc_id',$doc_id)->get('documents')->row();
    
    return $file_name->doc_name;
}

function trans_filename_noext($doc_id) {
    $ci = & get_instance();
    
    $file_name = $ci->db->select('documents.doc_name')->where('documents.doc_id',$doc_id)->get('documents')->row();
    
    $arr_fileName = explode('.', $file_name->doc_name);
    
    if(sizeof($arr_fileName) > 1) {
        return $arr_fileName[0];
    } else {
        return $file_name->doc_name;
    }
}

function dropdown_builder($default,$arr_data) {
    $dd_array = array('' => $default);
    
    foreach($arr_data as $_data) {
        $dd_array[$_data->id] = $_data->name;
    }
    
    return $dd_array;
}

function trans_department($dep_id) {
    $ci = & get_instance();
    
    $department_name = $ci->db->select('departments.dep_name')->where('departments.dep_id',$dep_id)->get('departments')->row();
    
    return $department_name->dep_name;
}

function trans_company($comp_id) {
    $ci = & get_instance();
    
    $company_name = $ci->db->select('company.comp_name')->where('company.comp_id',$comp_id)->get('company')->row();
    
    return $company_name->comp_name;
}

function trans_category($cat_id) {
    $ci = & get_instance();
    
    $category = $ci->db->select('categories.cat_name AS name')->where('categories.cat_id',$cat_id)->get('categories')->row();
    
    return $category->name;
}

function count_assigned_documents() {
    $ci = & get_instance();
    
    return $ci->db->where(array('notif_to'=>$ci->session->userdata('user_id'),'is_read'=>'700'))->get('users_notifs')->num_rows();
}

function read_content($id) {
    $ci = & get_instance();
    
    $file = 'uploads/docint/'.$id.'.php';
    if(is_file($file)) {
        return file_get_contents($file);
    }
}

function is_created($doc_id) {
    $ci = & get_instance();
    
    $is_created = $ci->db->where(array('doc_id'=>$doc_id,'is_created'=>'100'))->get('documents')->num_rows();
    
    if($is_created >= 1) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function is_approved($doc_id) {
    $ci = & get_instance();
    
    $is_approved = $ci->db->where(array('doc_id'=>$doc_id))->get('approvals')->num_rows();
    
    if($is_approved >= 1) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function is_forbidden_group($id) {
    $ci = & get_instance();
    
    $result = $ci->db->where('id IN (1,3,4,5)')->get('groups')->num_rows();
    
    $result > 0 ? $is_forbidden_group = TRUE : $is_forbidden_group = FALSE;
    
    return $is_forbidden_group;
}

function get_user_by_doc_id($doc_id,$permission) {
    $ci = & get_instance();
    
    $user = $ci->db->select('users_metas.user_id,CONCAT(users_metas.first_name," ",users_metas.last_name) AS full_name, users_metas.dep_id')
                    ->join('permissions','users_metas.user_id = permissions.user_id')
                    ->where(array('permissions.doc_id'=>$doc_id,'permissions.permission'=>$permission))
                    ->get('users_metas')->result();
    
    return $user;
}

function get_dep_by_doc_id($doc_id,$permission) {
    $ci = & get_instance();
    
    $department = $ci->db->select('departments.dep_id,departments.dep_name')
                    ->join('permissions','departments.dep_id = permissions.dep_id')
                    ->where(array('permissions.doc_id'=>$doc_id,'permissions.permission'=>$permission))
                    ->group_by('departments.dep_id')
                    ->get('departments')->result();
    
    return $department;
}

function have_revision($doc_id) {
    $ci = & get_instance();
    
    $last = $ci->db->select('last_revision AS revision')
                    ->where('documents.doc_id',$doc_id)
                    ->get('documents')->row();
    
    if($last->revision > 0) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function get_doc_ext($doc_id) {
    $ci = & get_instance();
    
    $extension = $ci->db->select('doc_type')
                    ->where('documents.doc_id',$doc_id)
                    ->get('documents')->row();
    
    return $extension->doc_type;
}

function get_last_revision($doc_id) {
    $ci = & get_instance();
    
    $revision = $ci->db->select('last_revision')
                    ->where('documents.doc_id',$doc_id)
                    ->get('documents')->row();
    
    return $revision->last_revision;
}

function is_read($doc_id) {
    $ci = & get_instance();
    
    $result = $ci->db->select('is_read')
                    ->where(array('doc_id'=>$doc_id))
                    ->get('permissions')->row();
    
    return $result->is_read;
}

function get_doc_num($doc_id) {
    $ci = & get_instance();
    
    $result = $ci->db->select('doc_num')
                    ->where(array('doc_id'=>$doc_id))
                    ->get('documents')->row();
    
    return $result->doc_num;
}

function is_owner($doc_id) {
    $ci = & get_instance();
    $session = $_SESSION;
    
    $result = $ci->db->where("doc_id = '".$doc_id."' AND user_id = ".$session['user_id']." AND ACTION IN('document_create','document_upload')")
                    ->get("history")->num_rows();
    
    if($result == 1){
        return TRUE;
    } else {
        return FALSE;
    }
}

function get_comp_logo_by_docid($doc_id) {
    $ci = & get_instance();
    
    $comp_logo = $ci->db->select('company.comp_logo')
            ->join('company','documents.comp_id = company.comp_id')
            ->where('documents.doc_id',$doc_id)->get('documents')->row();
    
    return $comp_logo->comp_logo;
}

function get_user_avatar($user_id) {
    $ci = & get_instance();
    
    $user = $ci->db->select('avatar')
            ->where('user_id',$user_id)->get('users_metas')->row();
    
    return $user->avatar;
}

function isset_location_path() {
    $ci = & get_instance();
    
    $location_path = $ci->db->select("CASE key_value_str WHEN '' THEN 'empty' ELSE key_value_str END AS key_val")->where('key_config','backup_location_path')->get('settings')->row();
    
    if($location_path->key_val == "empty"){
        return FALSE;
    } else {
        return TRUE;
    }
}

function get_backup_locpath() {
    $ci = & get_instance();
    
    $locpath = $ci->db->select("CASE key_value_str WHEN '' THEN 'empty' ELSE key_value_str END AS key_val")->where('key_config','backup_location_path')->get('settings')->row();
    
    return $locpath->key_val;
}

function get_file_icon($doc_type) {
    if($doc_type == "doc" || $doc_type == ".doc") {
        $file_icon = "docs.png";
    } else if($doc_type == "docx" || $doc_type == ".docx") {
        $file_icon = "docxs.png";
    } else if($doc_type == "pdf" || $doc_type == ".pdf") {
        $file_icon = "pdfs.png";
    } else if($doc_type == "ppt" || $doc_type == ".ppt" || $doc_type == "pptx" || $doc_type == ".pptx") {
        $file_icon = "ppts.png";
    } else if($doc_type == "xls" || $doc_type == ".xls") {
        $file_icon = "xlss.png";
    } else if($doc_type == "xlsx" || $doc_type == ".xlsx") {
        $file_icon = "xlsxs.png";
    } else if($doc_type == "3g2" || $doc_type == ".3g2" || $doc_type == "3gp" || $doc_type == ".3gp" || $doc_type == "3gpp" || $doc_type == ".3gpp" || $doc_type == "asf" || $doc_type == ".asf" || $doc_type == "avi" || $doc_type == ".avi" || $doc_type == "dat" || $doc_type == ".dat" || $doc_type == "divx" || $doc_type == ".divx" || $doc_type == "dv" || $doc_type == ".dv" || $doc_type == "flv" || $doc_type == ".flv" || $doc_type == "m2ts" || $doc_type == ".m2ts" || $doc_type == "m4v" || $doc_type == ".m4v" || $doc_type == "mkv" || $doc_type == ".mkv" || $doc_type == "mod" || $doc_type == ".mod" || $doc_type == "mp4" || $doc_type == ".mp4" || $doc_type == "mpe" || $doc_type == ".mpe" || $doc_type == "mpeg" || $doc_type == ".mpeg" || $doc_type == "mpeg4" || $doc_type == ".mpeg4" || $doc_type == "mpg" || $doc_type == ".mpg" || $doc_type == "mts" || $doc_type == ".mts" || $doc_type == "nsv" || $doc_type == ".nsv" || $doc_type == "ogm" || $doc_type == ".ogm" || $doc_type == "ogv" || $doc_type == ".ogv" || $doc_type == "qt" || $doc_type == ".qt" || $doc_type == "tod" || $doc_type == ".tod" || $doc_type == "ts" || $doc_type == ".ts" || $doc_type == "vob" || $doc_type == ".vob" || $doc_type == "wmv" || $doc_type == ".wmv") {
        $file_icon = "vids.png";
    } else if($doc_type == "ani" || $doc_type == ".ani" || $doc_type == "bmp" || $doc_type == ".bmp" || $doc_type == "cal" || $doc_type == ".cal" || $doc_type == "fax" || $doc_type == ".fax" || $doc_type == "gif" || $doc_type == ".gif" || $doc_type == "img" || $doc_type == ".img" || $doc_type == "jbg" || $doc_type == ".jbg" || $doc_type == "jpe" || $doc_type == ".jpe" || $doc_type == "jpeg" || $doc_type == ".jpeg" || $doc_type == "jpg" || $doc_type == ".jpg" || $doc_type == "mac" || $doc_type == ".mac" || $doc_type == "pbm" || $doc_type == ".pbm" || $doc_type == "pcd" || $doc_type == ".pcd" || $doc_type == "pcx" || $doc_type == ".pcx" || $doc_type == "pct" || $doc_type == ".pct" || $doc_type == "pgm" || $doc_type == ".pgm" || $doc_type == "png" || $doc_type == ".png" || $doc_type == "ppm" || $doc_type == ".ppm" || $doc_type == "psd" || $doc_type == ".psd" || $doc_type == "ras" || $doc_type == ".ras" || $doc_type == "tga" || $doc_type == ".tga" || $doc_type == "tiff" || $doc_type == ".tiff" || $doc_type == "wmf" || $doc_type == ".wmf") {
        $file_icon = "imgs.png";
    } else {
        $file_icon = "pres.png";
    }
    
    return $file_icon;
}