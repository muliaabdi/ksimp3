<?php defined('BASEPATH') OR exit('No direct script access allowed');
 /**
 * 
 */
 class Register extends CI_Controller
 {
 	
 	function __construct()
 	{
 		parent::__construct();
        $this->load->database();
 		// $this->load->model('candidate/M_candidate','candidate');
 		// $this->load->model('pemohon/pemohon_model','pemohon');
        $this->load->helper(array('url'));
        // $this->load->model('General_model','general');
        $this->load->model('Regions_model','regions');
        $this->load->helper('template');
 	}

 	public function index()
 	{
 		$data['jenis_izin'] = $this->pemohon->get_jenis_izin();
 		$this->load->view('themes/modules/register/register',$data);
 	}

public function perpanjangan()
 	{
 		if (!$this->ion_auth->logged_in()) {
                    // redirect them to the login page
                    redirect('login', 'refresh');
        } else {
        $id = $this->session->userdata('user_id');
        $data['get_user'] = 1;
 		$data['id_izin'] = 1;
 		// if ($data['id_izin'] == 1 || $data['id_izin'] == 2) {
 			$data['nm_jenis_izin'] = 'Data LPP Lokal';
 		// } elseif ($data['id_izin'] == 3 || $data['id_izin'] == 4 || $data['id_izin'] == 7) {
 		// 	$data['nm_jenis_izin'] = 'Data Perusahaan';
 		// } else {
 		// 	$data['nm_jenis_izin'] = 'Data Lembaga';
 		// }
 		$data['provinsi']=$this->regions->provinsi();

 		// //Begin from data administrasi
 		// $this->load->view('perpanjangan/new_register',$data);
 		
 		// $data['jum_menu'] = $this->ion_auth->get_menus();
            // var_dump($data['jum_menu']);
            //     exit();
            // $this->template->set_template('default',$data);
 			$this->load->view('_parts/public_header_view');
            $this->load->view('perpanjangan/new_register', $data);
            $this->load->view('perpanjangan/form_data_administrasi/nama_perusahaan',$data);
	 		$this->load->view('perpanjangan/form_data_administrasi/npwp',$data);
	 		$this->load->view('perpanjangan/form_data_administrasi/nm_stasiun_udara',$data);
	 		$this->load->view('perpanjangan/form_data_administrasi/alamat_kantor',$data);
	 		$this->load->view('perpanjangan/form_data_administrasi/alamat_studio',$data);
	 		
	 		if ($data['id_izin'] == 1 || $data['id_izin'] == 2) {
		 		$this->load->view('perpanjangan/form_data_administrasi/contact_person',$data);
		 		$this->load->view('perpanjangan/form_data_administrasi/pendirian_lpp_lokal',$data);
		 		$this->load->view('perpanjangan/form_data_administrasi/izin_penyelenggaraan_penyiaran',$data);
		 		$this->load->view('perpanjangan/form_data_administrasi/bukti_pembayaran_terakhir',$data);
		 		$this->load->view('perpanjangan/form_data_administrasi/surat_keterangan_domisili_lpp_lokal',$data);
	 		}
	 		if ($data['id_izin'] == 3 || $data['id_izin'] == 4 || $data['id_izin'] == 5 || $data['id_izin'] == 6 || $data['id_izin'] == 7) {
	 			$this->load->view('perpanjangan/form_data_administrasi/contact_person',$data);
	 			$this->load->view('perpanjangan/form_data_administrasi/akta_pendirian',$data);
	 			$this->load->view('perpanjangan/form_data_administrasi/pengesahan_akta_pendirian',$data);
	 			$this->load->view('perpanjangan/form_data_administrasi/izin_penyelenggaraan_penyiaran',$data);
	 			if ($data['id_izin'] == 5 || $data['id_izin'] == 6 || $data['id_izin'] == 7) {
	 				$this->load->view('perpanjangan/form_data_administrasi/bukti_pembayaran_terakhir',$data);
	 			}
	 			$this->load->view('perpanjangan/form_data_administrasi/akta_perubahan_terakhir',$data);
	 			$this->load->view('perpanjangan/form_data_administrasi/pengesahan_akta_perubahan_terakhir',$data);
	 		}
	 		$this->load->view('perpanjangan/button1',$data);
	 		if ($data['id_izin'] == 3 || $data['id_izin'] == 4 || $data['id_izin'] == 1 || $data['id_izin'] == 2 || $data['id_izin'] == 7) {
	 			$this->load->view('perpanjangan/form_data_administrasi/dewan_direksi',$data);
	 		}
	 		if ($data['id_izin'] == 1 || $data['id_izin'] == 2) {
	 			$this->load->view('perpanjangan/form_data_administrasi/dewan_pengawas',$data);
	 		}
	 		if ($data['id_izin'] == 3 || $data['id_izin'] == 4 || $data['id_izin'] == 7 ) {
	 			$this->load->view('perpanjangan/form_data_administrasi/komisaris',$data);
	 		}
	 		if ($data['id_izin'] == 5 || $data['id_izin'] == 6) {
	 			$this->load->view('perpanjangan/form_data_administrasi/pengurus',$data);
	 			$this->load->view('perpanjangan/form_data_administrasi/DPK',$data);
	 		}
	 		$this->load->view('perpanjangan/form_data_administrasi/penanggungjawab_penyiaran',$data);
	 		if ($data['id_izin'] == 3 || $data['id_izin'] == 4 || $data['id_izin'] == 7 ) {
		 		$this->load->view('perpanjangan/form_data_administrasi/aspek_permodalan2',$data);
	 		}
	 		if ($data['id_izin'] == 5 || $data['id_izin'] == 6) {
		 		$this->load->view('perpanjangan/form_data_administrasi/aspek_permodalan',$data);
	 		}
	 		if ($data['id_izin'] == 3 || $data['id_izin'] == 4 || $data['id_izin'] == 7 ) {
		 		$this->load->view('perpanjangan/form_data_administrasi/pemusatan_kepemilikan_saham',$data);
	 		}
	 		$this->load->view('perpanjangan/button2',$data);
	 		$this->load->view('perpanjangan/form_data_administrasi/data_kepegawaian',$data);
	 		$this->load->view('perpanjangan/button3',$data);
	 		//End form data administrasi

	 		$this->load->view('perpanjangan/header1',$data);
	 		//Begin form program siaran
	 		if ($data['id_izin'] == 3 || $data['id_izin'] == 4 || $data['id_izin'] == 1 || $data['id_izin'] == 2 || $data['id_izin'] == 5 || $data['id_izin'] == 6) {
		 		$this->load->view('perpanjangan/form_program_siaran/format_siaran',$data);
		 		$this->load->view('perpanjangan/form_program_siaran/presentase_materi',$data);
		 		$this->load->view('perpanjangan/form_program_siaran/sumber_materi',$data);
		 		$this->load->view('perpanjangan/form_program_siaran/waktu_siaran',$data);
		 		$this->load->view('perpanjangan/form_program_siaran/penggolongan_mata_siaran',$data);
		 		$this->load->view('perpanjangan/form_program_siaran/presentase_siaran_musik',$data);
		 		$this->load->view('perpanjangan/form_program_siaran/khalayak_sasaran',$data);
		 		
	 		}
	 		if ($data['id_izin'] == 7) {
	 			$this->load->view('perpanjangan/form_program_siaran/waktu_siaran_lpb',$data);
		 		$this->load->view('perpanjangan/form_program_siaran/total_kanal',$data);
		 		$this->load->view('perpanjangan/form_program_siaran/persentase_siaran',$data);
		 		$this->load->view('perpanjangan/form_program_siaran/penggolongan_mata_siaranlpb',$data);
		 		$this->load->view('perpanjangan/form_program_siaran/iklan_komersial',$data);
		 		$this->load->view('perpanjangan/form_program_siaran/sumber_materilpb',$data);
		 		$this->load->view('perpanjangan/form_program_siaran/khalayak_sasaranlpb',$data);
		 		$this->load->view('perpanjangan/form_program_siaran/jumlah_pelanggan',$data);
	 		}
	 		$this->load->view('perpanjangan/button4',$data);
	 		//End form program siaran

	 		//Begin form data teknik
	 		$this->load->view('perpanjangan/header2',$data);
		 	$this->load->view('perpanjangan/form_data_teknik/alamat_pemancar',$data);
		 	if ($data['id_izin'] == 3 ||  $data['id_izin'] == 1 || $data['id_izin'] == 5 ) {
		 		$this->load->view('perpanjangan/form_data_teknik/izin_stasiun_radio',$data);
		 	}
	 		if ($data['id_izin'] == 1 || $data['id_izin'] == 5) {
	 			$this->load->view('perpanjangan/form_data_teknik/bukti_pembayaran_terakhir_radio',$data);
	 		}
		 	if ($data['id_izin'] == 3 ||  $data['id_izin'] == 1 || $data['id_izin'] == 2 || $data['id_izin'] == 5 || $data['id_izin'] == 6) {
		 		$this->load->view('perpanjangan/form_data_teknik/sistem_modulasi',$data);
		 	}
		 	if ($data['id_izin'] == 4 ||  $data['id_izin'] == 7) {
		 		$this->load->view('perpanjangan/form_data_teknik/sistem_modulasi',$data);
		 	}
		 	if ($data['id_izin'] == 4) {
		 		$this->load->view('perpanjangan/form_data_teknik/stasiun_pengendali_penyiaran',$data);
		 		$this->load->view('perpanjangan/form_data_teknik/stasiun_distribusi_pelanggan',$data);
		 	}
		 	// var_dump($data['nama']);
		 	// exit();
	 		$this->load->view('perpanjangan/button5',$data);
	 		//End form data teknik

	 		$this->load->view('perpanjangan/file_yang_dilampirkan',$data);
	 		$this->load->view('perpanjangan/footer',$data);
            
        }
 	}

	public function PerubahanData()
 	{
 		if (!$this->ion_auth->logged_in()) {
                    // redirect them to the login page
                    redirect('login', 'refresh');
        } else {
 		$id = $this->session->userdata('user_id');
        $get_user = $this->pemohon->get_user($id)->row();
 		$data['id_izin'] = $get_user->id_jenis_izin;
 		if ($data['id_izin'] == 1 || $data['id_izin'] == 2) {
 			$data['nm_jenis_izin'] = 'Data LPP Lokal';
 		} elseif ($data['id_izin'] == 3 || $data['id_izin'] == 4 || $data['id_izin'] == 7) {
 			$data['nm_jenis_izin'] = 'Data Perusahaan';
 		} else {
 			$data['nm_jenis_izin'] = 'Data Lembaga';
 		}
 		$data['provinsi']=$this->general->provinsi_kantor();

 		// //Begin from data administrasi
 		// $this->load->view('themes/modules/register/new_register',$data);
 		
 		$data['jum_menu'] = $this->ion_auth->get_menus();
            // var_dump($data['jum_menu']);
            //     exit();
            $this->template->set_template('default',$data);
            $this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/new_register', $data);
            $this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/nama_perusahaan', $data);
            $this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/nama_perusahaan',$data);
	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/npwp',$data);
	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/nm_stasiun_udara',$data);
	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/alamat_kantor',$data);
	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/alamat_studio',$data);
	 		
	 		if ($data['id_izin'] == 1 || $data['id_izin'] == 2) {
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/contact_person',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/pendirian_lpp_lokal',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/surat_keterangan_domisili_lpp_lokal',$data);
	 		}
	 		if ($data['id_izin'] == 3 || $data['id_izin'] == 4 || $data['id_izin'] == 5 || $data['id_izin'] == 6 || $data['id_izin'] == 7) {
	 			$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/contact_person',$data);
	 			$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/akta_pendirian',$data);
	 			$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/pengesahan_akta_pendirian',$data);
	 			$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/akta_perubahan_terakhir',$data);
	 			$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/pengesahan_akta_perubahan_terakhir',$data);
	 		}
	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/button1',$data);
	 		if ($data['id_izin'] == 3 || $data['id_izin'] == 4 || $data['id_izin'] == 1 || $data['id_izin'] == 2 || $data['id_izin'] == 7) {
	 			$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/dewan_direksi',$data);
	 		}
	 		if ($data['id_izin'] == 1 || $data['id_izin'] == 2) {
	 			$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/dewan_pengawas',$data);
	 		}
	 		if ($data['id_izin'] == 3 || $data['id_izin'] == 4 || $data['id_izin'] == 7 ) {
	 			$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/komisaris',$data);
	 		}
	 		if ($data['id_izin'] == 5 || $data['id_izin'] == 6) {
	 			$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/pengurus',$data);
	 			$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/DPK',$data);
	 		}
	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/penanggungjawab_penyiaran',$data);
	 		if ($data['id_izin'] == 3 || $data['id_izin'] == 4 || $data['id_izin'] == 7 ) {
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/aspek_permodalan2',$data);
	 		}
	 		if ($data['id_izin'] == 5 || $data['id_izin'] == 6) {
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/aspek_permodalan',$data);
	 		}
	 		if ($data['id_izin'] == 3 || $data['id_izin'] == 4 || $data['id_izin'] == 7 ) {
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/pemusatan_kepemilikan_saham',$data);
	 		}
	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/button2',$data);
	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_administrasi/data_kepegawaian',$data);
	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/button3',$data);
	 		//End form data administrasi

	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/header1',$data);
	 		//Begin form program siaran
	 		if ($data['id_izin'] == 3 || $data['id_izin'] == 4 || $data['id_izin'] == 1 || $data['id_izin'] == 2 || $data['id_izin'] == 5 || $data['id_izin'] == 6) {
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/format_siaran',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/presentase_materi',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/sumber_materi',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/waktu_siaran',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/penggolongan_mata_siaran',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/presentase_siaran_musik',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/khalayak_sasaran',$data);
		 		
	 		}
	 		if ($data['id_izin'] == 7) {
	 			$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/waktu_siaran_lpb',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/total_kanal',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/persentase_siaran',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/penggolongan_mata_siaranlpb',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/iklan_komersial',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/sumber_materilpb',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/khalayak_sasaranlpb',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_program_siaran/jumlah_pelanggan',$data);
	 		}
	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/button4',$data);
	 		//End form program siaran

	 		//Begin form data teknik
	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/header2',$data);
		 	$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_teknik/alamat_pemancar',$data);
		 	if ($data['id_izin'] == 3 ||  $data['id_izin'] == 1 || $data['id_izin'] == 2 || $data['id_izin'] == 5 || $data['id_izin'] == 6) {
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_teknik/sistem_modulasi',$data);
		 	}
		 	if ($data['id_izin'] == 4 ||  $data['id_izin'] == 7) {
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_teknik/sistem_modulasi',$data);
		 	}
		 	if ($data['id_izin'] == 4) {
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_teknik/stasiun_pengendali_penyiaran',$data);
		 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/form_data_teknik/stasiun_distribusi_pelanggan',$data);
		 	}
	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/button5',$data);
	 		//End form data teknik

	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/file_yang_dilampirkan',$data);
	 		$this->template->write_view('content', MODULE_VIEW_PATH . 'perpanjangan/footer',$data);
            $this->template->render();
        }
 	} 	

 	public function RiwayatIzin()
 	{
 		if (!$this->ion_auth->logged_in()) {
                    // redirect them to the login page
                    redirect('login', 'refresh');
        } else {
 		$data['provinsi']=$this->general->provinsi_kantor();

 		// //Begin from data administrasi
 		// $this->load->view('themes/modules/register/new_register',$data);
 		
 		$data['jum_menu'] = $this->ion_auth->get_menus();
        $this->template->set_template('default',$data);
        $this->template->write_view('content', MODULE_VIEW_PATH . 'pemohon/riwayat_izin/index',$data);
        $this->template->render();
    	}
 	}
 }