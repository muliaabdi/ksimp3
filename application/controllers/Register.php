<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		//$this->load->library('template');
		$this->template->set_platform('public');
		$this->template->set_theme('default');
		$this->load->model('coklit/M_revisi', 'revisi');
        $this->load->library('form_validation');
	}

    public function index(){
        $this->form_validation->set_rules('nmlembaga', 'Company Name', 'required');
        if ($this->form_validation->run() === TRUE){
            $user = $this->ion_auth->user()->row()->first_name;
            $data = array(
                'id'    => $this->input->post('id'),
                'radiotv'    => $user,
                'nmlembaga'    => $this->input->post('nmlembaga'),
                'nmudara' => $this->input->post('nmudara'),
                'idpemohon' => $this->input->post('idpemohon'),
                'nmijin' => $this->input->post('nmijin'),
                'wilayahlayanan'   => $this->input->post('wilayahlayanan'),
                'nmmodulasi'    => $this->input->post('nmmodulasi'),
                'kedudukanstasiunpenyiarankhususlpsindukjaringanggotajaringan'     => $this->input->post('kedudukanstasiunpenyiarankhususlpsindukjaringanggotajaringan'),
                'nmkontak'     => $this->input->post('nmkontak'),
                'notelpkontak'     => $this->input->post('notelpkontak'),
                'nofaxkontak'     => $this->input->post('nofaxkontak'),
                'nohpkontak'     => $this->input->post('nohpkontak'),
                'emailkontak'     => $this->input->post('emailkontak'),
                'buktisuratpenunjukan'     => $this->input->post('buktisuratpenunjukan'),
                'jlnkantor'     => $this->input->post('jlnkantor'),
                'kelurahankantor'     => $this->input->post('kelurahankantor'),
                'kecamatankantor'     => $this->input->post('kecamatankantor'),
                'kabkotakantor'     => $this->input->post('kabkotakantor'),
                'propinsikantor'     => $this->input->post('propinsikantor'),
                'kodeposkantor'     => $this->input->post('kodeposkantor'),
                'zona'     => $this->input->post('zona'),
                'jlnstudio'     => $this->input->post('jlnstudio'),
                'kelurahanstudio'     => $this->input->post('kelurahanstudio'),
                'kecamatanstudio'     => $this->input->post('kecamatanstudio'),
                'kabkotastudio'     => $this->input->post('kabkotastudio'),
                'propinsistudio'     => $this->input->post('propinsistudio'),
                'kodeposstudio'     => $this->input->post('kodeposstudio'),
                'notelpstudio'     => $this->input->post('notelpstudio'),
                'nofaxstudio'     => $this->input->post('nofaxstudio'),
                'nmstapemancar'     => $this->input->post('nmstapemancar'),
                'alamatpemancar'     => $this->input->post('alamatpemancar'),
                'kelurahanpemancar'     => $this->input->post('kelurahanpemancar'),
                'kecamatanpemancar'     => $this->input->post('kecamatanpemancar'),
                'kabkotapemancar'     => $this->input->post('kabkotapemancar'),
                'provinsipemancar'     => $this->input->post('provinsipemancar'),
                'kodepospemancar'     => $this->input->post('kodepospemancar'),
                'npwp'     => $this->input->post('npwp'),
                'nosuketdomisiliusaha'     => $this->input->post('nosuketdomisiliusaha'),
                'tglsuketdomisiliusaha'     => $this->input->post('tglsuketdomisiliusaha'),
                'nminstansipenerbitdomisiliusaha'     => $this->input->post('nminstansipenerbitdomisiliusaha'),
                'noaktapendirian'     => $this->input->post('noaktapendirian'),
                'tglaktapendirian'     => $this->input->post('tglaktapendirian'),
                'nmnotarisaktapendirian'     => $this->input->post('nmnotarisaktapendirian'),
                'domisilinotarisaktapendirian'     => $this->input->post('domisilinotarisaktapendirian'),
                'nopengesahanakta'     => $this->input->post('nopengesahanakta'),
                'tglpengesahanakta'     => $this->input->post('tglpengesahanakta'),
                'nminstansipenerbitpengesahanakta'     => $this->input->post('nminstansipenerbitpengesahanakta'),
                'nositu'     => $this->input->post('nositu'),
                'tglsitu'     => $this->input->post('tglsitu'),
                'nminstansipenerbitsitu'     => $this->input->post('nminstansipenerbitsitu'),
                'notdp'     => $this->input->post('notdp'),
                'tgltdp'     => $this->input->post('tgltdp'),
                'noho'     => $this->input->post('noho'),
                'tglho'     => $this->input->post('tglho'),
                'nminstansipenerbitho'     => $this->input->post('nminstansipenerbitho'),
                'noimb'     => $this->input->post('noimb'),
                'tglimb'     => $this->input->post('tglimb'),
                'nminstansipenerbitimb'     => $this->input->post('nminstansipenerbitimb'),
                'noimbm'     => $this->input->post('noimbm'),
                'tglimbm'     => $this->input->post('tglimbm'),
                'nminstansipenerbitimbm'     => $this->input->post('nminstansipenerbitimbm'),
                'noaktaperubahanakhir'     => $this->input->post('noaktaperubahanakhir'),
                'tglaktaperubahanakhir'     => $this->input->post('tglaktaperubahanakhir'),
                'nmnotarisaktaperubahanakhir'     => $this->input->post('nmnotarisaktaperubahanakhir'),
                'domisiliaktaperubahanakhir'     => $this->input->post('domisiliaktaperubahanakhir'),
                'noaktasahperubahanakhir'     => $this->input->post('noaktasahperubahanakhir'),
                'tglaktasahperubahanakhir'     => $this->input->post('tglaktasahperubahanakhir'),
                'nminstansisahpenerbitakhir'     => $this->input->post('nminstansisahpenerbitakhir'),
                'tglrekomendasi'     => $this->input->post('tglrekomendasi'),
                'tglprafrb'     => $this->input->post('tglprafrb'),
                'tglfrb'     => $this->input->post('tglfrb'),
                'tglseleksi'     => $this->input->post('tglseleksi'),
                'tgleucs'     => $this->input->post('tgleucs'),
                'tglippexpiry'     => $this->input->post('tglippexpiry'),
                'nobafrb'     => $this->input->post('nobafrb'),
                'eucs'     => $this->input->post('eucs'),
                'statuseucs'     => $this->input->post('statuseucs'),
                'statusverifikasi'     => $this->input->post('statusverifikasi'),
                'nosuratpersetujuan'     => $this->input->post('nosuratpersetujuan'),
                'perubahandatasebelum'     => $this->input->post('perubahandatasebelum'),
                'perubahandatasesudah'     => $this->input->post('perubahandatasesudah'),
                'perihalperubahandata'     => $this->input->post('perihalperubahandata'),
                'nomorippprinsip'     => $this->input->post('nomorippprinsip'),
                'tanggalippprinsip'     => $this->input->post('tanggalippprinsip'),
                'masaberlakuippprinsip'     => $this->input->post('masaberlakuippprinsip'),
                'nomorperpanjanganippprinsip'     => $this->input->post('nomorperpanjanganippprinsip'),
                'tanggalperpanjanganippprinsip'     => $this->input->post('tanggalperpanjanganippprinsip'),
                'masaberlakuperpanjanganipprinsip'     => $this->input->post('masaberlakuperpanjanganipprinsip'),
                'noipptetap'     => $this->input->post('noipptetap'),
                'tglipptetap'     => $this->input->post('tglipptetap'),
                'masaberlakuipptetap'     => $this->input->post('masaberlakuipptetap'),
                'nomorippperpanjangan5tahun'     => $this->input->post('nomorippperpanjangan5tahun'),
                'tanggalippperpanjangan5tahun'     => $this->input->post('tanggalippperpanjangan5tahun'),
                'masaberlakuippperpanjangan5tahun'     => $this->input->post('masaberlakuippperpanjangan5tahun'),
                'noipppenyesuaian'     => $this->input->post('noipppenyesuaian'),
                'tglipppenyesuaian'     => $this->input->post('tglipppenyesuaian'),
                'masaberlakuipppenyesuaian'     => $this->input->post('masaberlakuipppenyesuaian'),
                'noipppenyesuaianamfm'     => $this->input->post('noipppenyesuaianamfm'),
                'tglipppenyesuaianamfm'     => $this->input->post('tglipppenyesuaianamfm'),
                'masaberlakuipppenyesuainamfm'     => $this->input->post('masaberlakuipppenyesuainamfm'),
                'statusterakhir'     => $this->input->post('statusterakhir'),
                'kodebiaya'     => $this->input->post('kodebiaya'),
                'idspp'     => $this->input->post('idspp'),
                'saldobayar'     => $this->input->post('saldobayar'),
                'tglbayar'     => $this->input->post('tglbayar'),
                'internalnote'     => $this->input->post('keterangan1'),
                'note'     => $this->input->post('catatan'),
                );
        }
        if ($this->form_validation->run() === TRUE && $this->revisi->insert('final',$data)){
            $this->session->set_flashdata('message', 'Registered');
            redirect("register", 'refresh');
        }
        else{
            $data['provinsi'] = $this->revisi->provinsi();
            $this->template->set_title('Konfirmasi Data Penyelenggara');
            $this->template->set_js('icheck/icheck.min.js','footer');
            $this->template->set_js('parsley/parsley.min.js','footer');
            $this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
            $this->template->set_meta('author','Turan Karatuğ');
            $this->template->set_layout('coklit/tambah_revisi');
            $this->template->render($data);
        }
    }

    function ambil_data(){
        $modul=$this->input->post('modul');
        $id=$this->input->post('id');

        if($modul=="kabupaten"){
        echo $this->revisi->kabupaten($id);
        }
        else if($modul=="kecamatan"){
        echo $this->revisi->kecamatan($id);
        }
        else if($modul=="kelurahan"){
        echo $this->revisi->kelurahan($id);
        }
        else if($modul=="kodepos"){
        echo $this->revisi->kodepos($id);
        }
        else if($modul=="zona"){
        echo $this->revisi->zona($id);
        }
    }

}