<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regions extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('Regions_model','regions');
    }

    function index(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $modul=$this->input->post('modul');
            $id=$this->input->post('id');

            if($modul=="kabupaten"){
            echo $this->regions->kabupaten($id);
            }
            else if($modul=="kecamatan"){
            echo $this->regions->kecamatan($id);
            }
            else if($modul=="kelurahan"){
            echo $this->regions->kelurahan($id);
            }
        }
    }
}