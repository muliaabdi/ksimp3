<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wlayanan extends CI_Controller{

  function __construct(){
        parent::__construct();
        if (!$this->ion_auth->logged_in())redirect('auth/login', 'refresh');
        $this->template->set_platform('public');
        $this->template->set_theme('default');
        $this->load->model('coklit/M_revisi', 'revisi');
  }

  public function index(){
        $data['provinsi']=$this->revisi->provinsi();
        $this->template->set_title('Konfirmasi Data Penyelenggara');
        $this->template->set_js('icheck/icheck.min.js','footer');
        $this->template->set_js('parsley/parsley.min.js','footer');
        $this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
        $this->template->set_meta('author','Turan Karatuğ');
        $this->template->set_layout('coklit/wilayah_layanan');
        $this->template->render($data);
    }
}