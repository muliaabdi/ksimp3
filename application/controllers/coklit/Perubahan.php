<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perubahan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		//$this->load->library('template');
		if (!$this->ion_auth->logged_in())redirect('auth/login', 'refresh');
		$this->template->set_platform('public');
		$this->template->set_theme('default');
		$this->load->model('coklit/M_revisi', 'revisi');
        $this->load->library('form_validation');
	}

	public function ajax_perubahan(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $this->load->library('datatables_ssp');
            $table = 'perubahantvradio';
            $primaryKey = 'NO';
 
            // Array of database columns which should be read and sent back to DataTables.
            // The `db` parameter represents the column name in the database, while the `dt`
            // parameter represents the DataTables column identifier. In this case simple
            // indexes
 
            $columns = array(
                array('db' => 'NO', 'dt' => 'NO'),
                array('db' => 'SOURCE', 'dt' => 'SOURCE'),
                array('db' => 'SHEET', 'dt' => 'SHEET'),
                array('db' => 'NAMALEMBAGAPENYIARAN', 'dt' => 'COMPANY NAME'),
                array('db' => 'ALAMATDANKONTAKPERSON', 'dt' => 'ALAMAT'),
                array('db' => 'PERUBAHANDATA', 'dt' => 'PERUBAHAN DATA'),
                array('db' => 'DATALAMA', 'dt' => 'DATA LAMA'),
                array('db' => 'DATABARU', 'dt' => 'DATA BARU'),
                array('db' => 'TINDAKLANJUTDISPOSISI', 'dt' => 'TINDAK LANJUT'),
                array(
                    'db' => 'NO',
                    'dt' => 'ACTION',
                    'formatter' => function( $d ) {
                        return '<a href="' . site_url('coklit/perubahan/edit/' . $d) . '">Edit</a>';
                    }
                ),
            );

            // if ($this->ion_auth->in_group('TELEVISI')){
            //     $where = "LEMBAGASIAR LIKE '%TELEVISI%' or LEMBAGASIAR = '' or LEMBAGASIAR = '-'";
            // }elseif ($this->ion_auth->in_group('RADIO')) {
            //     $where = "LEMBAGASIAR LIKE '%RADIO%' or LEMBAGASIAR = '' or LEMBAGASIAR = '-'";
            // }else{
                $where = null;
            // }

 
            // SQL server connection information
            $sql_details = array(
                'user' => 'root',
                'pass' => '',
                'db' => 'simp3_db',
                'host' => 'localhost'
            );
 
            echo json_encode(
                    Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, null, $where)
            );
        }
    }

	public function index(){
		$this->template->set_title('REVISI ALL UNIQ DATA');
		$this->template->set_css('datatables/tools/css/dataTables.tableTools.css');
		$this->template->set_js('datatables/js/jquery.dataTables.js','footer');
		$this->template->set_js('datatables/js/dataTables.bootstrap.js','footer');
		$this->template->set_js('datatables/tools/js/dataTables.tableTools.js','footer');
		$this->template->set_js('icheck/icheck.min.js','footer');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('coklit/report_perubahan');
		$this->template->render();
	}

    public function edit($id){
        if ($id==NULL) {
            redirect_back();
        }

        $this->form_validation->set_rules('NAMALEMBAGAPENYIARAN', 'Company Name', 'required');

        if ($this->form_validation->run() === TRUE){
            $user = $this->ion_auth->user()->row()->username;
            $data = array(
                'KODESTAF'    => $this->input->post('KODESTAF'),
                'ASALSTATUSDISPOSISI'=> $this->input->post('ASALSTATUSDISPOSISI'),
                'TAHUN' => $this->input->post('TAHUN'),
                'TANGGALINPUTSTAF' => $this->input->post('TANGGALINPUTSTAF'),
                'NOSURATTUM' => $this->input->post('NOSURATTUM'),
                'TANGGALSURATDITUM'   => $this->input->post('TANGGALSURATDITUM'),
                'TANGGALDISPOSISIDIREKTUR'      => $this->input->post('TANGGALDISPOSISIDIREKTUR'),
                'TANGGALDISPOSISIKASUBDIT'      => $this->input->post('TANGGALDISPOSISIKASUBDIT'),
                'TANGGALDISPOSISIKASIE'      => $this->input->post('TANGGALDISPOSISIKASIE'),
                'NOSURATLP'      => $this->input->post('NOSURATLP'),
                'TANGGALSURATLP'      => $this->input->post('TANGGALSURATLP'),
                'PERIODEPERMEN'      => $this->input->post('PERIODEPERMEN'),
                'GROUPLP'      => $this->input->post('GROUPLP'),
                'LP'      => $this->input->post('LP'),
                'JENISLP'      => $this->input->post('JENISLP'),
                'NAMALEMBAGAPENYIARAN'      => $this->input->post('NAMALEMBAGAPENYIARAN'),
                'SEBUTANUDARA'      => $this->input->post('SEBUTANUDARA'),
                'JENISIPP'      => $this->input->post('JENISIPP'),
                'NOIPP'      => $this->input->post('NOIPP'),
                'PROVINSI'      => $this->input->post('PROVINSI'),
                'KABKOTA'      => $this->input->post('KABKOTA'),
                'KEC'      => $this->input->post('KEC'),
                'KEL'      => $this->input->post('KEL'),
                'KODEPOS'      => $this->input->post('KODEPOS'),
                'ALAMATDANKONTAKPERSON'      => $this->input->post('ALAMATDANKONTAKPERSON'),
                'PERIHAL'      => $this->input->post('PERIHAL'),
                'SEBUTANNAMADIUDARA'      => $this->input->post('SEBUTANNAMADIUDARA'),
                'DOMISILI'      => $this->input->post('DOMISILI'),
                'SUSUNANPENGURUS'      => $this->input->post('SUSUNANPENGURUS'),
                'ANGGARANDASAR'      => $this->input->post('ANGGARANDASAR'),
                'LOKASIPEMANCAR'      => $this->input->post('LOKASIPEMANCAR'),
                'FREKUENSIPERANGKAT'      => $this->input->post('FREKUENSIPERANGKAT'),
                'WILAYAHLAYANAN'      => $this->input->post('WILAYAHLAYANAN'),
                'PROGRAMSIARAN'      => $this->input->post('PROGRAMSIARAN'),
                'SSJ'      => $this->input->post('SSJ'),
                'PERUBAHANDATA'      => $this->input->post('PERUBAHANDATA'),
                'DATALAMA'      => $this->input->post('DATALAMA'),
                'DATABARU'      => $this->input->post('DATABARU'),
                'LAMPIRANSURAT'      => $this->input->post('LAMPIRANSURAT'),
                'TINDAKLANJUTDISPOSISI'      => $this->input->post('TINDAKLANJUTDISPOSISI'),
                'STATUSTINDAKLANJUT'      => $this->input->post('STATUSTINDAKLANJUT'),
                'KEKURANGANBERKAS'      => $this->input->post('KEKURANGANBERKAS'),
                'STATUSKELENGKAPANBERKAS'      => $this->input->post('STATUSKELENGKAPANBERKAS'),
                'SURATKELUARPEMENUHANKURANGBERKAS'      => $this->input->post('SURATKELUARPEMENUHANKURANGBERKAS'),
                'KLARIFIKASIDANVERIFIKASI'      => $this->input->post('KLARIFIKASIDANVERIFIKASI'),
                'TANGGALBERITAACARAEVALUASIDANVERIFIKASI'      => $this->input->post('TANGGALBERITAACARAEVALUASIDANVERIFIKASI'),
                'NOTANGGAPANSURATPPIKELP'      => $this->input->post('NOTANGGAPANSURATPPIKELP'),
                'TANGGALSURATTANGGAPANSURATPPIKELP'      => $this->input->post('TANGGALSURATTANGGAPANSURATPPIKELP'),
                'NOTANGGAPANSURATPPIKESURATSDPPI'      => $this->input->post('NOTANGGAPANSURATPPIKESURATSDPPI'),
                'TANGGALTANGGAPANSURATPPIKESURATSDPPI'      => $this->input->post('TANGGALTANGGAPANSURATPPIKESURATSDPPI'),
                'NOTANGGAPANSURATSDPPIKESURATPPI'      => $this->input->post('NOTANGGAPANSURATSDPPIKESURATPPI'),
                'TANGGALTANGGAPANSURATSDPPIKESURATPPI'      => $this->input->post('TANGGALTANGGAPANSURATSDPPIKESURATPPI'),
                'SURATMASUKTERKAITTANGGAPANKPIDSDPPI'      => $this->input->post('SURATMASUKTERKAITTANGGAPANKPIDSDPPI'),
                );
        }
        if ($this->form_validation->run() === TRUE && $this->revisi->update('perubahantvradio', array('NO' => $id), $data)){
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("coklit/perubahan", 'refresh');
        }
        else{
            $data['data'] = $this->revisi->getWhere('perubahantvradio',array('NO' => $id));
            $data['provinsi']=$this->revisi->provinsi();
            $this->template->set_title('Konfirmasi Data Penyelenggara');
            $this->template->set_js('icheck/icheck.min.js','footer');
            $this->template->set_js('parsley/parsley.min.js','footer');
            $this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
            $this->template->set_meta('author','Turan Karatuğ');
            $this->template->set_layout('coklit/konfirmasi_perubahan');
            $this->template->render($data);
        }
    }

    public function tambah(){
        $this->form_validation->set_rules('NAMALEMBAGAPENYIARAN', 'Company Name', 'required');

        if ($this->form_validation->run() === TRUE){
            $user = $this->ion_auth->user()->row()->username;
            $data = array(
                'KODESTAF'    => $this->input->post('KODESTAF'),
                'ASALSTATUSDISPOSISI'=> $this->input->post('ASALSTATUSDISPOSISI'),
                'TAHUN' => $this->input->post('TAHUN'),
                'TANGGALINPUTSTAF' => $this->input->post('TANGGALINPUTSTAF'),
                'NOSURATTUM' => $this->input->post('NOSURATTUM'),
                'TANGGALSURATDITUM'   => $this->input->post('TANGGALSURATDITUM'),
                'TANGGALDISPOSISIDIREKTUR'      => $this->input->post('TANGGALDISPOSISIDIREKTUR'),
                'TANGGALDISPOSISIKASUBDIT'      => $this->input->post('TANGGALDISPOSISIKASUBDIT'),
                'TANGGALDISPOSISIKASIE'      => $this->input->post('TANGGALDISPOSISIKASIE'),
                'NOSURATLP'      => $this->input->post('NOSURATLP'),
                'TANGGALSURATLP'      => $this->input->post('TANGGALSURATLP'),
                'PERIODEPERMEN'      => $this->input->post('PERIODEPERMEN'),
                'GROUPLP'      => $this->input->post('GROUPLP'),
                'LP'      => $this->input->post('LP'),
                'JENISLP'      => $this->input->post('JENISLP'),
                'NAMALEMBAGAPENYIARAN'      => $this->input->post('NAMALEMBAGAPENYIARAN'),
                'SEBUTANUDARA'      => $this->input->post('SEBUTANUDARA'),
                'JENISIPP'      => $this->input->post('JENISIPP'),
                'NOIPP'      => $this->input->post('NOIPP'),
                'PROVINSI'      => $this->input->post('PROVINSI'),
                'KABKOTA'      => $this->input->post('KABKOTA'),
                'KEC'      => $this->input->post('KEC'),
                'KEL'      => $this->input->post('KEL'),
                'KODEPOS'      => $this->input->post('KODEPOS'),
                'ALAMATDANKONTAKPERSON'      => $this->input->post('ALAMATDANKONTAKPERSON'),
                'PERIHAL'      => $this->input->post('PERIHAL'),
                'SEBUTANNAMADIUDARA'      => $this->input->post('SEBUTANNAMADIUDARA'),
                'DOMISILI'      => $this->input->post('DOMISILI'),
                'SUSUNANPENGURUS'      => $this->input->post('SUSUNANPENGURUS'),
                'ANGGARANDASAR'      => $this->input->post('ANGGARANDASAR'),
                'LOKASIPEMANCAR'      => $this->input->post('LOKASIPEMANCAR'),
                'FREKUENSIPERANGKAT'      => $this->input->post('FREKUENSIPERANGKAT'),
                'WILAYAHLAYANAN'      => $this->input->post('WILAYAHLAYANAN'),
                'PROGRAMSIARAN'      => $this->input->post('PROGRAMSIARAN'),
                'SSJ'      => $this->input->post('SSJ'),
                'PERUBAHANDATA'      => $this->input->post('PERUBAHANDATA'),
                'DATALAMA'      => $this->input->post('DATALAMA'),
                'DATABARU'      => $this->input->post('DATABARU'),
                'LAMPIRANSURAT'      => $this->input->post('LAMPIRANSURAT'),
                'TINDAKLANJUTDISPOSISI'      => $this->input->post('TINDAKLANJUTDISPOSISI'),
                'STATUSTINDAKLANJUT'      => $this->input->post('STATUSTINDAKLANJUT'),
                'KEKURANGANBERKAS'      => $this->input->post('KEKURANGANBERKAS'),
                'STATUSKELENGKAPANBERKAS'      => $this->input->post('STATUSKELENGKAPANBERKAS'),
                'SURATKELUARPEMENUHANKURANGBERKAS'      => $this->input->post('SURATKELUARPEMENUHANKURANGBERKAS'),
                'KLARIFIKASIDANVERIFIKASI'      => $this->input->post('KLARIFIKASIDANVERIFIKASI'),
                'TANGGALBERITAACARAEVALUASIDANVERIFIKASI'      => $this->input->post('TANGGALBERITAACARAEVALUASIDANVERIFIKASI'),
                'NOTANGGAPANSURATPPIKELP'      => $this->input->post('NOTANGGAPANSURATPPIKELP'),
                'TANGGALSURATTANGGAPANSURATPPIKELP'      => $this->input->post('TANGGALSURATTANGGAPANSURATPPIKELP'),
                'NOTANGGAPANSURATPPIKESURATSDPPI'      => $this->input->post('NOTANGGAPANSURATPPIKESURATSDPPI'),
                'TANGGALTANGGAPANSURATPPIKESURATSDPPI'      => $this->input->post('TANGGALTANGGAPANSURATPPIKESURATSDPPI'),
                'NOTANGGAPANSURATSDPPIKESURATPPI'      => $this->input->post('NOTANGGAPANSURATSDPPIKESURATPPI'),
                'TANGGALTANGGAPANSURATSDPPIKESURATPPI'      => $this->input->post('TANGGALTANGGAPANSURATSDPPIKESURATPPI'),
                'SURATMASUKTERKAITTANGGAPANKPIDSDPPI'      => $this->input->post('SURATMASUKTERKAITTANGGAPANKPIDSDPPI'),
                );
        }
        if ($this->form_validation->run() === TRUE && $this->revisi->insert('perubahantvradio', $data)){
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("coklit/perubahan", 'refresh');
        }
        else{
            $data['provinsi']=$this->revisi->provinsi();
            $this->template->set_title('Konfirmasi Data Penyelenggara');
            $this->template->set_js('icheck/icheck.min.js','footer');
            $this->template->set_js('parsley/parsley.min.js','footer');
            $this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
            $this->template->set_meta('author','Turan Karatuğ');
            $this->template->set_layout('coklit/tambah_perubahan');
            $this->template->render($data);
        }
    }
}