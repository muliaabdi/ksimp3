<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eucs extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		//$this->load->library('template');
		if (!$this->ion_auth->logged_in())redirect('auth/login', 'refresh');
		$this->template->set_platform('public');
		$this->template->set_theme('default');
		$this->load->model('coklit/M_revisi', 'revisi');
        $this->load->library('form_validation');
	}

	public function ajax_eucs(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $this->load->library('datatables_ssp');
            $table = 'rekapkekuranganeucs';
            $primaryKey = 'NO';
 
            // Array of database columns which should be read and sent back to DataTables.
            // The `db` parameter represents the column name in the database, while the `dt`
            // parameter represents the DataTables column identifier. In this case simple
            // indexes
 
            $columns = array(
                array('db' => 'NO', 'dt' => 'NO'),
                array('db' => 'SOURCE', 'dt' => 'SOURCE'),
                array('db' => 'SHEET', 'dt' => 'SHEET'),
                array('db' => 'NAMALEMBAGAPENYIARAN', 'dt' => 'COMPANY NAME'),
                array('db' => 'ALAMATKANTORLEMBAGAPENYIARAN', 'dt' => 'ALAMAT'),
                array('db' => 'ASALBERKASPENGAJUAN', 'dt' => 'ASAL BERKAS'),
                array('db' => 'PERUBAHANDATA', 'dt' => 'PERUBAHAN DATA'),
                array('db' => 'TINDAKLANJUT', 'dt' => 'TINDAK LANJUT'),
                array(
                    'db' => 'NO',
                    'dt' => 'ACTION',
                    'formatter' => function( $d ) {
                        return '<a href="' . site_url('coklit/eucs/edit/' . $d) . '">Edit</a>';
                    }
                ),
            );

            // if ($this->ion_auth->in_group('TELEVISI')){
            //     $where = "LEMBAGASIAR LIKE '%TELEVISI%' or LEMBAGASIAR = '' or LEMBAGASIAR = '-'";
            // }elseif ($this->ion_auth->in_group('RADIO')) {
            //     $where = "LEMBAGASIAR LIKE '%RADIO%' or LEMBAGASIAR = '' or LEMBAGASIAR = '-'";
            // }else{
                $where = null;
            // }

 
            // SQL server connection information
            $sql_details = array(
                'user' => 'root',
                'pass' => '',
                'db' => 'simp3_db',
                'host' => 'localhost'
            );
 
            echo json_encode(
                    Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, null, $where)
            );
        }
    }

	public function index(){
		$this->template->set_title('REVISI ALL UNIQ DATA');
		$this->template->set_css('datatables/tools/css/dataTables.tableTools.css');
		$this->template->set_js('datatables/js/jquery.dataTables.js','footer');
		$this->template->set_js('datatables/js/dataTables.bootstrap.js','footer');
		$this->template->set_js('datatables/tools/js/dataTables.tableTools.js','footer');
		$this->template->set_js('icheck/icheck.min.js','footer');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('coklit/report_eucs');
		$this->template->render();
	}

    public function edit($id){
        if ($id==NULL) {
            redirect_back();
        }
        $this->form_validation->set_rules('NAMALEMBAGAPENYIARAN', 'Company Name', 'required');

        if ($this->form_validation->run() === TRUE){
            $user = $this->ion_auth->user()->row()->username;
            $data = array(
                'KATEGORI'    => $this->input->post('KATEGORI'),
                'PROVINSI'=> $this->input->post('PROVINSI'),
                'NAMALEMBAGAPENYIARAN' => $this->input->post('NAMALEMBAGAPENYIARAN'),
                'ALAMATKANTORLEMBAGAPENYIARAN' => $this->input->post('ALAMATKANTORLEMBAGAPENYIARAN'),
                'KABKOTA'      => $this->input->post('KABKOTA'),
                'KEC'      => $this->input->post('KEC'),
                'KEL'      => $this->input->post('KEL'),
                'KODEPOS'      => $this->input->post('KODEPOS'),
                'ASALBERKASPENGAJUAN' => $this->input->post('ASALBERKASPENGAJUAN'),
                'TANGGALMASUKDITSUBDIT'   => $this->input->post('TANGGALMASUKDITSUBDIT'),
                'CONTACTPERSON'      => $this->input->post('CONTACTPERSON'),
                'EMAIL'      => $this->input->post('EMAIL'),
                'TAHUNPENGAJUANEUCS'      => $this->input->post('TAHUNPENGAJUANEUCS'),
                'PENGAJUANEUCS'      => $this->input->post('PENGAJUANEUCS'),
                'TANGGALPENGAJUAN'      => $this->input->post('TANGGALPENGAJUAN'),
                'IPP'      => $this->input->post('IPP'),
                'TANGGALIPP'      => $this->input->post('TANGGALIPP'),
                'IPPPERPANJANGAN'      => $this->input->post('IPPPERPANJANGAN'),
                'TANGGALPERPANJANGAN'      => $this->input->post('TANGGALPERPANJANGAN'),
                'MASALAKU'      => $this->input->post('MASALAKU'),
                'ISR'      => $this->input->post('ISR'),
                'PERSYARATAN'      => $this->input->post('PERSYARATAN'),
                'TINDAKLANJUT'      => $this->input->post('TINDAKLANJUT'),
                'PRIORITASPELAKSANAAN'      => $this->input->post('PRIORITASPELAKSANAAN'),
                'PERUBAHANDATA'      => $this->input->post('PERUBAHANDATA'),
                'TANGGALVERIFIKASI'      => $this->input->post('TANGGALVERIFIKASI'),
                'KETERANGANSCANBERKASVERIFIKASI'      => $this->input->post('KETERANGANSCANBERKASVERIFIKASI'),
                'LP'      => $this->input->post('LP'),
                'JENISLP'      => $this->input->post('JENISLP'),
                'TAHUNPELAKSANAANEUCS'      => $this->input->post('TAHUNPELAKSANAANEUCS'),
                'TANGGALPELAKSANAAN'      => $this->input->post('TANGGALPELAKSANAAN'),
                'LAPORANEUCSKEMENTERI'      => $this->input->post('LAPORANEUCSKEMENTERI'),
                'NOBERITAACARA'      => $this->input->post('NOBERITAACARA'),
                'TGLSURATPEMBERITAHUANHASILEUCS'      => $this->input->post('TGLSURATPEMBERITAHUANHASILEUCS'),
                'ADMINISTRASI'      => $this->input->post('ADMINISTRASI'),
                'WAKTUPENYELESAIAN'      => $this->input->post('WAKTUPENYELESAIAN'),
                'TGLJATUHTEMPO'      => $this->input->post('TGLJATUHTEMPO'),
                'KELENGKAPAN'      => $this->input->post('KELENGKAPAN'),
                'SURATKELUARADMIN'      => $this->input->post('SURATKELUARADMIN'),
                'PROGRESADM'      => $this->input->post('PROGRESADM'),
                'TEKNIS'      => $this->input->post('TEKNIS'),
                'WAKTUPENYELESAIANADM'      => $this->input->post('WAKTUPENYELESAIANADM'),
                'TGLJATUHTEMPOADM'      => $this->input->post('TGLJATUHTEMPOADM'),
                'NOTADINASDITOPSD'      => $this->input->post('NOTADINASDITOPSD'),
                'SURATKELUARTEKNIS'      => $this->input->post('SURATKELUARTEKNIS'),
                'PROGRESTNK'      => $this->input->post('PROGRESTNK'),
                'PROGRAMSIARAN'      => $this->input->post('PROGRAMSIARAN'),
                'WAKTUPENYELESAIANTNK'      => $this->input->post('WAKTUPENYELESAIANTNK'),
                'TGLJATUHTEMPOTNK'      => $this->input->post('TGLJATUHTEMPOTNK'),
                'SURATKPIKPID'      => $this->input->post('SURATKPIKPID'),
                'SURATKELUARPROGRAM'      => $this->input->post('SURATKELUARPROGRAM'),
                'PROGRESPRGM'      => $this->input->post('PROGRESPRGM'),
                'KETERANGAN'      => $this->input->post('KETERANGAN'),
                'NONOTADINAS'      => $this->input->post('NONOTADINAS'),
                'STATUS'      => $this->input->post('STATUS'),
                'STATUSLAPORAN'      => $this->input->post('STATUSLAPORAN'),
                'TAHUN'      => $this->input->post('TAHUN'),
                'TANGGALPELAKSANAAN2'      => $this->input->post('TANGGALPELAKSANAAN2'),
                'NOBERITAACARA2'      => $this->input->post('NOBERITAACARA2'),
                'TGLSURATPEMBERITAHUANHASILEUCS2'      => $this->input->post('TGLSURATPEMBERITAHUANHASILEUCS2'),
                'ADMINISTRASI2'      => $this->input->post('ADMINISTRASI2'),
                'WAKTUPENYELESAIAN2'      => $this->input->post('WAKTUPENYELESAIAN2'),
                'TGLJATUHTEMPO2'      => $this->input->post('TGLJATUHTEMPO2'),
                'KELENGKAPAN2'      => $this->input->post('KELENGKAPAN2'),
                'PROGRESADM2'      => $this->input->post('PROGRESADM2'),
                'TEKNIS2'      => $this->input->post('TEKNIS2'),
                'WAKTUPENYELESAIAN21'      => $this->input->post('WAKTUPENYELESAIAN21'),
                'TGLJATUHTEMPO21'      => $this->input->post('TGLJATUHTEMPO21'),
                'NOTADINASDITOPSD2'      => $this->input->post('NOTADINASDITOPSD2'),
                'PROGRESTNK2'      => $this->input->post('PROGRESTNK2'),
                'PROGRAMSIARAN2'      => $this->input->post('PROGRAMSIARAN2'),
                'WAKTUPENYELESAIAN22'      => $this->input->post('WAKTUPENYELESAIAN22'),
                'TGLJATUHTEMPO22'      => $this->input->post('TGLJATUHTEMPO22'),
                'SURATKPIKPID2'      => $this->input->post('SURATKPIKPID2'),
                'PROGRESPRGM2'      => $this->input->post('PROGRESPRGM2'),
                'KETERANGAN2'      => $this->input->post('KETERANGAN2'),
                'TAHUN3'      => $this->input->post('TAHUN3'),
                'TANGGALPELAKSANAAN3'      => $this->input->post('TANGGALPELAKSANAAN3'),
                'NOBERITAACARA3'      => $this->input->post('NOBERITAACARA3'),
                'TGLSURATPEMBERITAHUANHASILEUCS3'      => $this->input->post('TGLSURATPEMBERITAHUANHASILEUCS3'),
                'ADMINISTRASI3'      => $this->input->post('ADMINISTRASI3'),
                'WAKTUPENYELESAIAN31'      => $this->input->post('WAKTUPENYELESAIAN31'),
                'TGLJATUHTEMPO31'      => $this->input->post('TGLJATUHTEMPO31'),
                'KELENGKAPAN3'      => $this->input->post('KELENGKAPAN3'),
                'PROGRESADM3'      => $this->input->post('PROGRESADM3'),
                'TEKNIS3'      => $this->input->post('TEKNIS3'),
                'WAKTUPENYELESAIAN32'      => $this->input->post('WAKTUPENYELESAIAN32'),
                'TGLJATUHTEMPO32'      => $this->input->post('TGLJATUHTEMPO32'),
                'NOTADINASDITOPSD3'      => $this->input->post('NOTADINASDITOPSD3'),
                'SURATKELUARTEKNIS3'      => $this->input->post('SURATKELUARTEKNIS3'),
                'PROGRESTNK3'      => $this->input->post('PROGRESTNK3'),
                'PROGRAMSIARAN3'      => $this->input->post('PROGRAMSIARAN3'),
                'WAKTUPENYELESAIAN33'      => $this->input->post('WAKTUPENYELESAIAN33'),
                'TGLJATUHTEMPO33'      => $this->input->post('TGLJATUHTEMPO33'),
                'SURATKPIKPID3'      => $this->input->post('SURATKPIKPID3'),
                'SURATKELUARPROGRAM3'      => $this->input->post('SURATKELUARPROGRAM3'),
                'PROGRESPRGM3'      => $this->input->post('PROGRESPRGM3'),
                'KETERANGAN3'      => $this->input->post('KETERANGAN3'),
                );
        }
        if ($this->form_validation->run() === TRUE && $this->revisi->update('rekapkekuranganeucs', array('NO' => $id), $data)){
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("coklit/eucs", 'refresh');
        }
        else{
            $data['data'] = $this->revisi->getWhere('rekapkekuranganeucs',array('NO' => $id));
            $data['provinsi']=$this->revisi->provinsi();
            $this->template->set_title('Konfirmasi Data EUCS');
            $this->template->set_js('icheck/icheck.min.js','footer');
            $this->template->set_js('parsley/parsley.min.js','footer');
            $this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
            $this->template->set_meta('author','Turan Karatuğ');
            $this->template->set_layout('coklit/konfirmasi_eucs');
            $this->template->render($data);
        }
    }

    public function tambah(){
        $this->form_validation->set_rules('NAMALEMBAGAPENYIARAN', 'Company Name', 'required');

        if ($this->form_validation->run() === TRUE){
            $user = $this->ion_auth->user()->row()->username;
            $data = array(
                'KATEGORI'    => $this->input->post('KATEGORI'),
                'PROVINSI'=> $this->input->post('PROVINSI'),
                'NAMALEMBAGAPENYIARAN' => $this->input->post('NAMALEMBAGAPENYIARAN'),
                'ALAMATKANTORLEMBAGAPENYIARAN' => $this->input->post('ALAMATKANTORLEMBAGAPENYIARAN'),
                'KABKOTA'      => $this->input->post('KABKOTA'),
                'KEC'      => $this->input->post('KEC'),
                'KEL'      => $this->input->post('KEL'),
                'KODEPOS'      => $this->input->post('KODEPOS'),
                'ASALBERKASPENGAJUAN' => $this->input->post('ASALBERKASPENGAJUAN'),
                'TANGGALMASUKDITSUBDIT'   => $this->input->post('TANGGALMASUKDITSUBDIT'),
                'CONTACTPERSON'      => $this->input->post('CONTACTPERSON'),
                'EMAIL'      => $this->input->post('EMAIL'),
                'TAHUNPENGAJUANEUCS'      => $this->input->post('TAHUNPENGAJUANEUCS'),
                'PENGAJUANEUCS'      => $this->input->post('PENGAJUANEUCS'),
                'TANGGALPENGAJUAN'      => $this->input->post('TANGGALPENGAJUAN'),
                'IPP'      => $this->input->post('IPP'),
                'TANGGALIPP'      => $this->input->post('TANGGALIPP'),
                'IPPPERPANJANGAN'      => $this->input->post('IPPPERPANJANGAN'),
                'TANGGALPERPANJANGAN'      => $this->input->post('TANGGALPERPANJANGAN'),
                'MASALAKU'      => $this->input->post('MASALAKU'),
                'ISR'      => $this->input->post('ISR'),
                'PERSYARATAN'      => $this->input->post('PERSYARATAN'),
                'TINDAKLANJUT'      => $this->input->post('TINDAKLANJUT'),
                'PRIORITASPELAKSANAAN'      => $this->input->post('PRIORITASPELAKSANAAN'),
                'PERUBAHANDATA'      => $this->input->post('PERUBAHANDATA'),
                'TANGGALVERIFIKASI'      => $this->input->post('TANGGALVERIFIKASI'),
                'KETERANGANSCANBERKASVERIFIKASI'      => $this->input->post('KETERANGANSCANBERKASVERIFIKASI'),
                'LP'      => $this->input->post('LP'),
                'JENISLP'      => $this->input->post('JENISLP'),
                'TAHUNPELAKSANAANEUCS'      => $this->input->post('TAHUNPELAKSANAANEUCS'),
                'TANGGALPELAKSANAAN'      => $this->input->post('TANGGALPELAKSANAAN'),
                'LAPORANEUCSKEMENTERI'      => $this->input->post('LAPORANEUCSKEMENTERI'),
                'NOBERITAACARA'      => $this->input->post('NOBERITAACARA'),
                'TGLSURATPEMBERITAHUANHASILEUCS'      => $this->input->post('TGLSURATPEMBERITAHUANHASILEUCS'),
                'ADMINISTRASI'      => $this->input->post('ADMINISTRASI'),
                'WAKTUPENYELESAIAN'      => $this->input->post('WAKTUPENYELESAIAN'),
                'TGLJATUHTEMPO'      => $this->input->post('TGLJATUHTEMPO'),
                'KELENGKAPAN'      => $this->input->post('KELENGKAPAN'),
                'SURATKELUARADMIN'      => $this->input->post('SURATKELUARADMIN'),
                'PROGRESADM'      => $this->input->post('PROGRESADM'),
                'TEKNIS'      => $this->input->post('TEKNIS'),
                'WAKTUPENYELESAIANADM'      => $this->input->post('WAKTUPENYELESAIANADM'),
                'TGLJATUHTEMPOADM'      => $this->input->post('TGLJATUHTEMPOADM'),
                'NOTADINASDITOPSD'      => $this->input->post('NOTADINASDITOPSD'),
                'SURATKELUARTEKNIS'      => $this->input->post('SURATKELUARTEKNIS'),
                'PROGRESTNK'      => $this->input->post('PROGRESTNK'),
                'PROGRAMSIARAN'      => $this->input->post('PROGRAMSIARAN'),
                'WAKTUPENYELESAIANTNK'      => $this->input->post('WAKTUPENYELESAIANTNK'),
                'TGLJATUHTEMPOTNK'      => $this->input->post('TGLJATUHTEMPOTNK'),
                'SURATKPIKPID'      => $this->input->post('SURATKPIKPID'),
                'SURATKELUARPROGRAM'      => $this->input->post('SURATKELUARPROGRAM'),
                'PROGRESPRGM'      => $this->input->post('PROGRESPRGM'),
                'KETERANGAN'      => $this->input->post('KETERANGAN'),
                'NONOTADINAS'      => $this->input->post('NONOTADINAS'),
                'STATUS'      => $this->input->post('STATUS'),
                'STATUSLAPORAN'      => $this->input->post('STATUSLAPORAN'),
                'TAHUN'      => $this->input->post('TAHUN'),
                'TANGGALPELAKSANAAN2'      => $this->input->post('TANGGALPELAKSANAAN2'),
                'NOBERITAACARA2'      => $this->input->post('NOBERITAACARA2'),
                'TGLSURATPEMBERITAHUANHASILEUCS2'      => $this->input->post('TGLSURATPEMBERITAHUANHASILEUCS2'),
                'ADMINISTRASI2'      => $this->input->post('ADMINISTRASI2'),
                'WAKTUPENYELESAIAN2'      => $this->input->post('WAKTUPENYELESAIAN2'),
                'TGLJATUHTEMPO2'      => $this->input->post('TGLJATUHTEMPO2'),
                'KELENGKAPAN2'      => $this->input->post('KELENGKAPAN2'),
                'PROGRESADM2'      => $this->input->post('PROGRESADM2'),
                'TEKNIS2'      => $this->input->post('TEKNIS2'),
                'WAKTUPENYELESAIAN21'      => $this->input->post('WAKTUPENYELESAIAN21'),
                'TGLJATUHTEMPO21'      => $this->input->post('TGLJATUHTEMPO21'),
                'NOTADINASDITOPSD2'      => $this->input->post('NOTADINASDITOPSD2'),
                'PROGRESTNK2'      => $this->input->post('PROGRESTNK2'),
                'PROGRAMSIARAN2'      => $this->input->post('PROGRAMSIARAN2'),
                'WAKTUPENYELESAIAN22'      => $this->input->post('WAKTUPENYELESAIAN22'),
                'TGLJATUHTEMPO22'      => $this->input->post('TGLJATUHTEMPO22'),
                'SURATKPIKPID2'      => $this->input->post('SURATKPIKPID2'),
                'PROGRESPRGM2'      => $this->input->post('PROGRESPRGM2'),
                'KETERANGAN2'      => $this->input->post('KETERANGAN2'),
                'TAHUN3'      => $this->input->post('TAHUN3'),
                'TANGGALPELAKSANAAN3'      => $this->input->post('TANGGALPELAKSANAAN3'),
                'NOBERITAACARA3'      => $this->input->post('NOBERITAACARA3'),
                'TGLSURATPEMBERITAHUANHASILEUCS3'      => $this->input->post('TGLSURATPEMBERITAHUANHASILEUCS3'),
                'ADMINISTRASI3'      => $this->input->post('ADMINISTRASI3'),
                'WAKTUPENYELESAIAN31'      => $this->input->post('WAKTUPENYELESAIAN31'),
                'TGLJATUHTEMPO31'      => $this->input->post('TGLJATUHTEMPO31'),
                'KELENGKAPAN3'      => $this->input->post('KELENGKAPAN3'),
                'PROGRESADM3'      => $this->input->post('PROGRESADM3'),
                'TEKNIS3'      => $this->input->post('TEKNIS3'),
                'WAKTUPENYELESAIAN32'      => $this->input->post('WAKTUPENYELESAIAN32'),
                'TGLJATUHTEMPO32'      => $this->input->post('TGLJATUHTEMPO32'),
                'NOTADINASDITOPSD3'      => $this->input->post('NOTADINASDITOPSD3'),
                'SURATKELUARTEKNIS3'      => $this->input->post('SURATKELUARTEKNIS3'),
                'PROGRESTNK3'      => $this->input->post('PROGRESTNK3'),
                'PROGRAMSIARAN3'      => $this->input->post('PROGRAMSIARAN3'),
                'WAKTUPENYELESAIAN33'      => $this->input->post('WAKTUPENYELESAIAN33'),
                'TGLJATUHTEMPO33'      => $this->input->post('TGLJATUHTEMPO33'),
                'SURATKPIKPID3'      => $this->input->post('SURATKPIKPID3'),
                'SURATKELUARPROGRAM3'      => $this->input->post('SURATKELUARPROGRAM3'),
                'PROGRESPRGM3'      => $this->input->post('PROGRESPRGM3'),
                'KETERANGAN3'      => $this->input->post('KETERANGAN3'),
                );
        }
        if ($this->form_validation->run() === TRUE && $this->revisi->insert('rekapkekuranganeucs', $data)){
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("coklit/eucs", 'refresh');
        }
        else{
            $data['provinsi']=$this->revisi->provinsi();
            $this->template->set_title('Konfirmasi Data EUCS');
            $this->template->set_js('icheck/icheck.min.js','footer');
            $this->template->set_js('parsley/parsley.min.js','footer');
            $this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
            $this->template->set_meta('author','Turan Karatuğ');
            $this->template->set_layout('coklit/tambah_eucs');
            $this->template->render($data);
        }
    }
}