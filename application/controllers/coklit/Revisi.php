<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Revisi extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		//$this->load->library('template');
		if (!$this->ion_auth->logged_in())redirect('auth/login', 'refresh');
		$this->template->set_platform('public');
		$this->template->set_theme('default');
		$this->load->model('coklit/M_revisi', 'revisi');
        $this->load->library('form_validation');
	}

	public function ajax_revisi(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $this->load->library('datatables_ssp');
            $table = 'final';
            $primaryKey = 'sysid';
 
            // Array of database columns which should be read and sent back to DataTables.
            // The `db` parameter represents the column name in the database, while the `dt`
            // parameter represents the DataTables column identifier. In this case simple
            // indexes
 
            $columns = array(
                array('db' => 'ID', 'dt' => 'ID'),
                array('db' => 'sysid', 'dt' => 'SYSID'),
                array('db' => 'source', 'dt' => 'SOURCE'),
                array('db' => 'sheet', 'dt' => 'SHEET'),
                array('db' => 'nmlembaga', 'dt' => 'COMPANY NAME'),
                array('db' => 'trouble', 'dt' => 'TROUBLE'),
                array('db' => 'status', 'dt' => 'STATUS'),
                array('db' => 'nmijin', 'dt' => 'NAMA IZIN'),
                array('db' => 'jlnkantor', 'dt' => 'ALAMAT'),
                array('db' => 'kelurahankantor', 'dt' => 'KELURAHAN'),
                array('db' => 'kecamatankantor', 'dt' => 'KECAMATAN'),
                array('db' => 'kabkotakantor', 'dt' => 'KOTA'),
                array('db' => 'propinsikantor', 'dt' => 'PROVINSI'),
                array('db' => 'zona', 'dt' => 'ZONA'),
                array('db' => 'wilayahlayanan', 'dt' => 'wl'),
                array('db' => 'kirim', 'dt' => 'KIRIM'),
                array('db' => 'user', 'dt' => 'AUDITOR'),
                array(
                    'db' => 'sysid',
                    'dt' => 'ACTION',
                    'formatter' => function( $d ) {
                        return '<a href="' . site_url('coklit/revisi/edit/' . $d) . '">Edit</a><br /><a href="' . site_url('coklit/revisi/delete/' . $d) . '">Delete</a>';
                    }
                ),
            );

            if ($this->ion_auth->in_group('TELEVISI')){
                $where = "radiotv = 'TELEVISI'";
            }elseif ($this->ion_auth->in_group('RADIO')) {
                $where = "radiotv = 'RADIO'";
            }elseif ($this->ion_auth->in_group('LPPLR')) {
                $where = "radiotv = 'LPPLRADIO'";
            }elseif ($this->ion_auth->in_group('LPPLT')) {
                $where = "radiotv = 'LPPLTV'";
            }else{
                $where = null;
            }

 
            // SQL server connection information
            $sql_details = array(
                'user' => 'k8421072_simp3',
                'pass' => '4br4k4d4br4',
                'db' => 'k8421072_ksimp3',
                'host' => 'tritechapps.co.id'
            );
 
            echo json_encode(
                    Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, "", $where)
            );
        }
    }


    public function index(){
        $data['trouble'] = $this->revisi->getDistinct('final', 'trouble');
        $data['prov'] = $this->revisi->getDistinct('final', 'propinsikantor');
        $data['wl'] = $this->revisi->getDistinct('final', 'wilayahlayanan');
        $this->template->set_title('REVISI ALL UNIQ DATA');
        $this->template->set_css('datatables/tools/css/dataTables.tableTools.css');
        $this->template->set_js('datatables/js/jquery.dataTables.js','footer');
        $this->template->set_js('datatables/js/dataTables.bootstrap.js','footer');
        $this->template->set_js('datatables/tools/js/dataTables.tableTools.js','footer');
        $this->template->set_js('icheck/icheck.min.js','footer');
        $this->template->set_js('parsley/parsley.min.js','footer');
        $this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
        $this->template->set_meta('author','Turan Karatuğ');
        $this->template->set_layout('coklit/report_revisi');
        $this->template->render($data);
    }

    public function tambah(){
        $this->form_validation->set_rules('nmlembaga', 'Company Name', 'required');
        if ($this->form_validation->run() === TRUE){
            $user = $this->ion_auth->user()->row()->first_name;
            $data = array(
                'id'    => $this->input->post('id'),
                'radiotv'    => $this->input->post('id'),
                'nmlembaga'    => $this->input->post('nmlembaga'),
                'nmudara' => $this->input->post('nmudara'),
                'idpemohon' => $this->input->post('idpemohon'),
                'nmijin' => $this->input->post('nmijin'),
                'wilayahlayanan'   => $this->input->post('wilayahlayanan'),
                'nmmodulasi'    => $this->input->post('nmmodulasi'),
                'kedudukanstasiunpenyiarankhususlpsindukjaringanggotajaringan'     => $this->input->post('kedudukanstasiunpenyiarankhususlpsindukjaringanggotajaringan'),
                'nmkontak'     => $this->input->post('nmkontak'),
                'notelpkontak'     => $this->input->post('notelpkontak'),
                'nofaxkontak'     => $this->input->post('nofaxkontak'),
                'nohpkontak'     => $this->input->post('nohpkontak'),
                'emailkontak'     => $this->input->post('emailkontak'),
                'buktisuratpenunjukan'     => $this->input->post('buktisuratpenunjukan'),
                'jlnkantor'     => $this->input->post('jlnkantor'),
                'kelurahankantor'     => $this->input->post('kelurahankantor'),
                'kecamatankantor'     => $this->input->post('kecamatankantor'),
                'kabkotakantor'     => $this->input->post('kabkotakantor'),
                'propinsikantor'     => $this->input->post('propinsikantor'),
                'kodeposkantor'     => $this->input->post('kodeposkantor'),
                'zona'     => $this->input->post('zona'),
                'jlnstudio'     => $this->input->post('jlnstudio'),
                'kelurahanstudio'     => $this->input->post('kelurahanstudio'),
                'kecamatanstudio'     => $this->input->post('kecamatanstudio'),
                'kabkotastudio'     => $this->input->post('kabkotastudio'),
                'propinsistudio'     => $this->input->post('propinsistudio'),
                'kodeposstudio'     => $this->input->post('kodeposstudio'),
                'notelpstudio'     => $this->input->post('notelpstudio'),
                'nofaxstudio'     => $this->input->post('nofaxstudio'),
                'nmstapemancar'     => $this->input->post('nmstapemancar'),
                'alamatpemancar'     => $this->input->post('alamatpemancar'),
                'kelurahanpemancar'     => $this->input->post('kelurahanpemancar'),
                'kecamatanpemancar'     => $this->input->post('kecamatanpemancar'),
                'kabkotapemancar'     => $this->input->post('kabkotapemancar'),
                'provinsipemancar'     => $this->input->post('provinsipemancar'),
                'kodepospemancar'     => $this->input->post('kodepospemancar'),
                'npwp'     => $this->input->post('npwp'),
                'nosuketdomisiliusaha'     => $this->input->post('nosuketdomisiliusaha'),
                'tglsuketdomisiliusaha'     => $this->input->post('tglsuketdomisiliusaha'),
                'nminstansipenerbitdomisiliusaha'     => $this->input->post('nminstansipenerbitdomisiliusaha'),
                'noaktapendirian'     => $this->input->post('noaktapendirian'),
                'tglaktapendirian'     => $this->input->post('tglaktapendirian'),
                'nmnotarisaktapendirian'     => $this->input->post('nmnotarisaktapendirian'),
                'domisilinotarisaktapendirian'     => $this->input->post('domisilinotarisaktapendirian'),
                'nopengesahanakta'     => $this->input->post('nopengesahanakta'),
                'tglpengesahanakta'     => $this->input->post('tglpengesahanakta'),
                'nminstansipenerbitpengesahanakta'     => $this->input->post('nminstansipenerbitpengesahanakta'),
                'nositu'     => $this->input->post('nositu'),
                'tglsitu'     => $this->input->post('tglsitu'),
                'nminstansipenerbitsitu'     => $this->input->post('nminstansipenerbitsitu'),
                'notdp'     => $this->input->post('notdp'),
                'tgltdp'     => $this->input->post('tgltdp'),
                'noho'     => $this->input->post('noho'),
                'tglho'     => $this->input->post('tglho'),
                'nminstansipenerbitho'     => $this->input->post('nminstansipenerbitho'),
                'noimb'     => $this->input->post('noimb'),
                'tglimb'     => $this->input->post('tglimb'),
                'nminstansipenerbitimb'     => $this->input->post('nminstansipenerbitimb'),
                'noimbm'     => $this->input->post('noimbm'),
                'tglimbm'     => $this->input->post('tglimbm'),
                'nminstansipenerbitimbm'     => $this->input->post('nminstansipenerbitimbm'),
                'noaktaperubahanakhir'     => $this->input->post('noaktaperubahanakhir'),
                'tglaktaperubahanakhir'     => $this->input->post('tglaktaperubahanakhir'),
                'nmnotarisaktaperubahanakhir'     => $this->input->post('nmnotarisaktaperubahanakhir'),
                'domisiliaktaperubahanakhir'     => $this->input->post('domisiliaktaperubahanakhir'),
                'noaktasahperubahanakhir'     => $this->input->post('noaktasahperubahanakhir'),
                'tglaktasahperubahanakhir'     => $this->input->post('tglaktasahperubahanakhir'),
                'nminstansisahpenerbitakhir'     => $this->input->post('nminstansisahpenerbitakhir'),
                'tglrekomendasi'     => $this->input->post('tglrekomendasi'),
                'tglprafrb'     => $this->input->post('tglprafrb'),
                'tglfrb'     => $this->input->post('tglfrb'),
                'tglseleksi'     => $this->input->post('tglseleksi'),
                'tgleucs'     => $this->input->post('tgleucs'),
                'tglippexpiry'     => $this->input->post('tglippexpiry'),
                'nobafrb'     => $this->input->post('nobafrb'),
                'eucs'     => $this->input->post('eucs'),
                'statuseucs'     => $this->input->post('statuseucs'),
                'statusverifikasi'     => $this->input->post('statusverifikasi'),
                'nosuratpersetujuan'     => $this->input->post('nosuratpersetujuan'),
                'perubahandatasebelum'     => $this->input->post('perubahandatasebelum'),
                'perubahandatasesudah'     => $this->input->post('perubahandatasesudah'),
                'perihalperubahandata'     => $this->input->post('perihalperubahandata'),
                'nomorippprinsip'     => $this->input->post('nomorippprinsip'),
                'tanggalippprinsip'     => $this->input->post('tanggalippprinsip'),
                'masaberlakuippprinsip'     => $this->input->post('masaberlakuippprinsip'),
                'nomorperpanjanganippprinsip'     => $this->input->post('nomorperpanjanganippprinsip'),
                'tanggalperpanjanganippprinsip'     => $this->input->post('tanggalperpanjanganippprinsip'),
                'masaberlakuperpanjanganipprinsip'     => $this->input->post('masaberlakuperpanjanganipprinsip'),
                'noipptetap'     => $this->input->post('noipptetap'),
                'tglipptetap'     => $this->input->post('tglipptetap'),
                'masaberlakuipptetap'     => $this->input->post('masaberlakuipptetap'),
                'nomorippperpanjangan5tahun'     => $this->input->post('nomorippperpanjangan5tahun'),
                'tanggalippperpanjangan5tahun'     => $this->input->post('tanggalippperpanjangan5tahun'),
                'masaberlakuippperpanjangan5tahun'     => $this->input->post('masaberlakuippperpanjangan5tahun'),
                'noipppenyesuaian'     => $this->input->post('noipppenyesuaian'),
                'tglipppenyesuaian'     => $this->input->post('tglipppenyesuaian'),
                'masaberlakuipppenyesuaian'     => $this->input->post('masaberlakuipppenyesuaian'),
                'noipppenyesuaianamfm'     => $this->input->post('noipppenyesuaianamfm'),
                'tglipppenyesuaianamfm'     => $this->input->post('tglipppenyesuaianamfm'),
                'masaberlakuipppenyesuainamfm'     => $this->input->post('masaberlakuipppenyesuainamfm'),
                'statusterakhir'     => $this->input->post('statusterakhir'),
                'kodebiaya'     => $this->input->post('kodebiaya'),
                'idspp'     => $this->input->post('idspp'),
                'saldobayar'     => $this->input->post('saldobayar'),
                'tglbayar'     => $this->input->post('tglbayar'),
                'note'     => $this->input->post('note'),
                );
                    $group = array('TELEVISI', 'RADIO', 'EUCS');
                    if (!$this->ion_auth->in_group($group)){
                        $data['internalnote'] = $this->input->post('keterangan1');
                    }
                    if ($this->ion_auth->in_group('TELEVISI')){
                        $data['radiotv'] = 'TELEVISI';
                    }
                    if ($this->ion_auth->in_group('RADIO')) {
                        $data['radiotv'] = 'RADIO';
                    }
        }
        if ($this->form_validation->run() === TRUE && $this->revisi->insert('final',$data)){
            $this->session->set_flashdata('message', 'Registered');
            redirect("coklit/revisi", 'refresh');
        }
        else{
            $data['provinsi'] = $this->revisi->provinsi();
            $this->template->set_title('Konfirmasi Data Penyelenggara');
            $this->template->set_js('icheck/icheck.min.js','footer');
            $this->template->set_js('parsley/parsley.min.js','footer');
            $this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
            $this->template->set_meta('author','Turan Karatuğ');
            $this->template->set_layout('coklit/tambah_revisi');
            $this->template->render($data);
        }
    }
    
    public function edit($id){
        if ($id==NULL) {
            redirect_back();
        }
        $this->template->set_title('Konfirmasi Data Alamat Kantor');

        $this->form_validation->set_rules('nmlembaga', 'Company Name', 'required');

        if ($this->form_validation->run() === TRUE){
            $user = $this->ion_auth->user()->row()->first_name;
            $data = array(
                'nmlembaga'    => $this->input->post('nmlembaga'),
                'nmudara' => $this->input->post('nmudara'),
                'idpemohon' => $this->input->post('idpemohon'),
                'nmijin' => $this->input->post('nmijin'),
                // 'wilayahlayanan'   => $this->input->post('wilayahlayanan'),
                // 'nmmodulasi'    => $this->input->post('nmmodulasi'),
                'nmkontak'     => $this->input->post('nmkontak'),
                'notelpkontak'     => $this->input->post('notelpkontak'),
                'nofaxkontak'     => $this->input->post('nofaxkontak'),
                'nohpkontak'     => $this->input->post('nohpkontak'),
                'emailkontak'     => $this->input->post('emailkontak'),
                'buktisuratpenunjukan'     => $this->input->post('buktisuratpenunjukan'),
                'jlnkantor'     => $this->input->post('jlnkantor'),
                'kelurahankantor'     => $this->input->post('kelurahankantor'),
                'kecamatankantor'     => $this->input->post('kecamatankantor'),
                'kabkotakantor'     => $this->input->post('kabkotakantor'),
                'propinsikantor'     => $this->input->post('propinsikantor'),
                'kodeposkantor'     => $this->input->post('kodeposkantor'),
                'zona'     => $this->input->post('zona'),
                'jlnstudio'     => $this->input->post('jlnstudio'),
                'kelurahanstudio'     => $this->input->post('kelurahanstudio'),
                'kecamatanstudio'     => $this->input->post('kecamatanstudio'),
                'kabkotastudio'     => $this->input->post('kabkotastudio'),
                'propinsistudio'     => $this->input->post('propinsistudio'),
                'kodeposstudio'     => $this->input->post('kodeposstudio'),
                'notelpstudio'     => $this->input->post('notelpstudio'),
                'nofaxstudio'     => $this->input->post('nofaxstudio'),
                'nmstapemancar'     => $this->input->post('nmstapemancar'),
                'alamatpemancar'     => $this->input->post('alamatpemancar'),
                'kelurahanpemancar'     => $this->input->post('kelurahanpemancar'),
                'kecamatanpemancar'     => $this->input->post('kecamatanpemancar'),
                'kabkotapemancar'     => $this->input->post('kabkotapemancar'),
                'provinsipemancar'     => $this->input->post('provinsipemancar'),
                'kodepospemancar'     => $this->input->post('kodepospemancar'),
                'npwp'     => $this->input->post('npwp'),
                'nosuketdomisiliusaha'     => $this->input->post('nosuketdomisiliusaha'),
                'tglsuketdomisiliusaha'     => $this->input->post('tglsuketdomisiliusaha'),
                'nminstansipenerbitdomisiliusaha'     => $this->input->post('nminstansipenerbitdomisiliusaha'),
                'noaktapendirian'     => $this->input->post('noaktapendirian'),
                'tglaktapendirian'     => $this->input->post('tglaktapendirian'),
                'nmnotarisaktapendirian'     => $this->input->post('nmnotarisaktapendirian'),
                'domisilinotarisaktapendirian'     => $this->input->post('domisilinotarisaktapendirian'),
                'nopengesahanakta'     => $this->input->post('nopengesahanakta'),
                'tglpengesahanakta'     => $this->input->post('tglpengesahanakta'),
                'nminstansipenerbitpengesahanakta'     => $this->input->post('nminstansipenerbitpengesahanakta'),
                'nositu'     => $this->input->post('nositu'),
                'tglsitu'     => $this->input->post('tglsitu'),
                'nminstansipenerbitsitu'     => $this->input->post('nminstansipenerbitsitu'),
                'notdp'     => $this->input->post('notdp'),
                'tgltdp'     => $this->input->post('tgltdp'),
                'noho'     => $this->input->post('noho'),
                'tglho'     => $this->input->post('tglho'),
                'nminstansipenerbitho'     => $this->input->post('nminstansipenerbitho'),
                'noimb'     => $this->input->post('noimb'),
                'tglimb'     => $this->input->post('tglimb'),
                'nminstansipenerbitimb'     => $this->input->post('nminstansipenerbitimb'),
                'noimbm'     => $this->input->post('noimbm'),
                'tglimbm'     => $this->input->post('tglimbm'),
                'nminstansipenerbitimbm'     => $this->input->post('nminstansipenerbitimbm'),
                'noaktaperubahanakhir'     => $this->input->post('noaktaperubahanakhir'),
                'tglaktaperubahanakhir'     => $this->input->post('tglaktaperubahanakhir'),
                'nmnotarisaktaperubahanakhir'     => $this->input->post('nmnotarisaktaperubahanakhir'),
                'domisiliaktaperubahanakhir'     => $this->input->post('domisiliaktaperubahanakhir'),
                'noaktasahperubahanakhir'     => $this->input->post('noaktasahperubahanakhir'),
                'tglaktasahperubahanakhir'     => $this->input->post('tglaktasahperubahanakhir'),
                'nminstansisahpenerbitakhir'     => $this->input->post('nminstansisahpenerbitakhir'),
                'tglrekomendasi'     => $this->input->post('tglrekomendasi'),
                'tglprafrb'     => $this->input->post('tglprafrb'),
                'tglfrb'     => $this->input->post('tglfrb'),
                'tglseleksi'     => $this->input->post('tglseleksi'),
                'tgleucs'     => $this->input->post('tgleucs'),
                'tglippexpiry'     => $this->input->post('tglippexpiry'),
                'nobafrb'     => $this->input->post('nobafrb'),
                'eucs'     => $this->input->post('eucs'),
                'statuseucs'     => $this->input->post('statuseucs'),
                'statusverifikasi'     => $this->input->post('statusverifikasi'),
                'nosuratpersetujuan'     => $this->input->post('nosuratpersetujuan'),
                'perubahandatasebelum'     => $this->input->post('perubahandatasebelum'),
                'perubahandatasesudah'     => $this->input->post('perubahandatasesudah'),
                'perihalperubahandata'     => $this->input->post('perihalperubahandata'),
                'nomorippprinsip'     => $this->input->post('nomorippprinsip'),
                'tanggalippprinsip'     => $this->input->post('tanggalippprinsip'),
                'masaberlakuippprinsip'     => $this->input->post('masaberlakuippprinsip'),
                'nomorperpanjanganippprinsip'     => $this->input->post('nomorperpanjanganippprinsip'),
                'tanggalperpanjanganippprinsip'     => $this->input->post('tanggalperpanjanganippprinsip'),
                'masaberlakuperpanjanganipprinsip'     => $this->input->post('masaberlakuperpanjanganipprinsip'),
                'noipptetap'     => $this->input->post('noipptetap'),
                'tglipptetap'     => $this->input->post('tglipptetap'),
                'masaberlakuipptetap'     => $this->input->post('masaberlakuipptetap'),
                'nomorippperpanjangan5tahun'     => $this->input->post('nomorippperpanjangan5tahun'),
                'tanggalippperpanjangan5tahun'     => $this->input->post('tanggalippperpanjangan5tahun'),
                'masaberlakuippperpanjangan5tahun'     => $this->input->post('masaberlakuippperpanjangan5tahun'),
                'noipppenyesuaian'     => $this->input->post('noipppenyesuaian'),
                'tglipppenyesuaian'     => $this->input->post('tglipppenyesuaian'),
                'masaberlakuipppenyesuaian'     => $this->input->post('masaberlakuipppenyesuaian'),
                'noipppenyesuaianamfm'     => $this->input->post('noipppenyesuaianamfm'),
                'tglipppenyesuaianamfm'     => $this->input->post('tglipppenyesuaianamfm'),
                'masaberlakuipppenyesuainamfm'     => $this->input->post('masaberlakuipppenyesuainamfm'),
                'statusterakhir'     => $this->input->post('statusterakhir'),
                'kodebiaya'     => $this->input->post('kodebiaya'),
                'idspp'     => $this->input->post('idspp'),
                'saldobayar'     => $this->input->post('saldobayar'),
                'tglbayar'     => $this->input->post('tglbayar'),
                'user'     => $user,
                'note'     => $this->input->post('catatan'),
                'zonaspp'     => $this->input->post('zonaspp'),
                'mohonperpanjanganippprinsip'     => $this->input->post('mohonperpanjanganippprinsip'),
                'statipp'     => $this->input->post('statipp'),
                'ISR'     => $this->input->post('ISR'),
                'frek'     => $this->input->post('frek'),
                'kebijakanprinsipmati'     => $this->input->post('kebijakanprinsipmati'),
                'keterangan'     => $this->input->post('keterangan'),
                'tindaklanjut'     => $this->input->post('tindaklanjut'),
                'amfm'     => $this->input->post('amfm'),
                'tanggalmenteri'     => $this->input->post('tanggalmenteri'),
                'jnslembaga'     => $this->input->post('jnslembaga'),
                'nodantanggalrk'     => $this->input->post('nodantanggalrk'),
                'usulanfrekfrb'     => $this->input->post('usulanfrekfrb'),
                'hasilfrb'     => $this->input->post('hasilfrb'),
                'frbpending'     => $this->input->post('frbpending'),
                'suratpenolakan'     => $this->input->post('suratpenolakan'),
                );
                if(is_array($this->input->post('wilayahlayanan'))) {
                    $data['wilayahlayanan']  = "";
                    foreach($this->input->post('wilayahlayanan') as $row){
                        $data['wilayahlayanan'] .= $row;
                        if ($row != end($_POST['wilayahlayanan'])) {
                            $data['wilayahlayanan'] .= ";";
                        }
                    }
                }else{
                    $data['wilayahlayanan'] = $this->input->post('wilayahlayanan');
                }
                $group = array('TELEVISI', 'RADIO', 'EUCS');
                if (!$this->ion_auth->in_group($group)){ 
                    $data['internalnote'] = $this->input->post('keterangan1');
                }
                if ($this->input->post('status')){ 
                    $data['status'] = $this->input->post('status');
                }
                if ($this->input->post('archived')){ 
                    $data['archived'] = $this->input->post('archived');
                }
                if ($this->ion_auth->user()->row()->username === 'gun'){
                    $data['trouble'] = $this->input->post('trouble');
                }
        }
        if ($this->form_validation->run() === TRUE && $this->revisi->update('final', array('sysid' => $id), $data)){
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("coklit/revisi", 'refresh');
        }
        else{
            $data['nmijin'] = $this->revisi->getDistinct('final', 'nmijin');
            $data['data'] = $this->revisi->getWhere('final',array('sysid' => $id));
            $data['provinsi'] = $this->revisi->provinsi();
            $data['provinsiwl'] = $this->revisi->getDistinct('wilayahlayanantv', 'PROVINSI');
            $data['zona2'] = $this->revisi->zona2();
            $this->template->set_title('Konfirmasi Data Penyelenggara');
            $this->template->set_css('icheck/flat/green.css');
            $this->template->set_css('select/select2.min.css');
            $this->template->set_js('select/select2.full.js','footer');
            $this->template->set_js('icheck/icheck.min.js','footer');
            $this->template->set_js('parsley/parsley.min.js','footer');
            $this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
            $this->template->set_meta('author','Turan Karatuğ');
            $this->template->set_layout('coklit/konfirmasi_revisi');
            $this->template->render($data);
        }
    }

    function delete($id){
        if($this->ion_auth->user()->row()->username === 'gun'){
            $this->revisi->delete('final', array('sysid' => $id));
            redirect("coklit/revisi", 'refresh');
        }else{
            return show_error('You must be an administrator to view this page.');
        }
    }

    public function get_zona($id) {
        if( $this->input->is_ajax_request() ) {
            $zn = $this->revisi->get_zona($id);

            echo json_encode($zn);

            return FALSE;
        }
        show_404();
    }

    function ambil_data(){
        $modul=$this->input->post('modul');
        $id=$this->input->post('id');

        if($modul=="kabupaten"){
        echo $this->revisi->kabupaten($id);
        }
        else if($modul=="kecamatan"){
        echo $this->revisi->kecamatan($id);
        }
        else if($modul=="kelurahan"){
        echo $this->revisi->kelurahan($id);
        }
        else if($modul=="kodepos"){
        echo $this->revisi->kodepos($id);
        }
        else if($modul=="zona"){
        echo $this->revisi->zona($id);
        }
        else if($modul=="wilayahlayanantv"){
        echo $this->revisi->wilayahlayanantv($id);
        }
        else if($modul=="wlkabkot"){
        echo $this->revisi->wlkabkot($id);
        }
        else if($modul=="wilayahlayananradio"){
        echo $this->revisi->wilayahlayananradio($id);
        }
    }

}