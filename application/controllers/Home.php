<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		//$this->load->library('template');
		if (!$this->ion_auth->logged_in())redirect('auth/login', 'refresh');
		$this->template->set_platform('public');
		$this->template->set_theme('default');
	}

	public function index(){
		redirect('konfirmasikontak', 'refresh');
	}

	public function konfirmasikontak(){
		$data['content'] = 'Hello';
		$this->template->set_title('Konfirmasi Data Perizinan Penyelenggara Penyiaran');
		// $this->template->set_css('bootstrap.min.css');
		// $this->template->set_css('bootstrap-theme.css');
		// $this->template->set_css('navbar.css');
		// $this->template->set_js('https://code.jquery.com/jquery-1.11.3.min.js','footer','remote');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('home_view');
		$this->template->render($data);
	}

	public function konfirmasipenyelenggara(){
		$data['content'] = 'World !';

		$this->template->set_title('Konfirmasi Data Penyelenggara');
		//$this->template->set_css('bootstrap.min.css');
		//$this->template->set_css('bootstrap-theme.css');
		//$this->template->set_css('navbar.css');
		//$this->template->set_js('https://code.jquery.com/jquery-1.11.3.min.js','footer','remote');
		//$this->template->set_js('bootstrap.min.js','footer');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('konfirmasi_penyelenggara');
		$this->template->render($data);
	}

	public function konfirmasikantor(){
		$data['content']	= 'World !';

		$this->template->set_title('Konfirmasi Data Penyelenggara');
		//$this->template->set_css('bootstrap-datetimepicker.min.css');
		//$this->template->set_css('bootstrap-theme.css');
		//$this->template->set_css('navbar.css');
		//$this->template->set_js('bootstrap.min.js','footer');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_js('moment/moment.min.js','footer');
		$this->template->set_js('bootstrap-datetimepicker.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('konfirmasi_kantor');
		$this->template->render($data);
	}

	public function konfirmasistudio(){
		$data['content']	= 'World !';

		$this->template->set_title('Konfirmasi Data Penyelenggara');
		//$this->template->set_css('bootstrap-datetimepicker.min.css');
		//$this->template->set_css('bootstrap-theme.css');
		//$this->template->set_css('navbar.css');
		//$this->template->set_js('bootstrap.min.js','footer');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('konfirmasi_studio');
		$this->template->render($data);
	}

	public function konfirmasipemancar(){
		$data['content']	= 'World !';

		$this->template->set_title('Konfirmasi Data Penyelenggara');
		//$this->template->set_css('bootstrap-datetimepicker.min.css');
		//$this->template->set_css('bootstrap-theme.css');
		//$this->template->set_css('navbar.css');
		//$this->template->set_js('bootstrap.min.js','footer');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('konfirmasi_pemancar');
		$this->template->render($data);
	}

	public function konfirmasikronologis(){
		$data['content'] = 'World !';
		$this->template->set_title('Konfirmasi Data Penyelenggara');
		$this->template->set_css('http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css','remote');
		$this->template->set_js('http://code.jquery.com/jquery-1.10.2.js','footer','remote');
		$this->template->set_js('http://code.jquery.com/ui/1.11.4/jquery-ui.js','footer','remote');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('konfirmasi_kronologis');
		$this->template->render($data);
	}

	public function konfirmasispp(){
		$data['content']	= 'World !';

		$this->template->set_title('Konfirmasi Data Penyelenggara');
		//$this->template->set_css('bootstrap-theme.css');
		//$this->template->set_css('navbar.css');
		//$this->template->set_js('bootstrap.min.js','footer');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('konfirmasi_spp');
		$this->template->render($data);
	}

	public function cetaksurat(){
		$data['content']	= 'World !';

		$this->template->set_title('Konfirmasi Data Penyelenggara');
		//$this->template->set_css('bootstrap-datetimepicker.min.css');
		//$this->template->set_css('bootstrap-theme.css');
		//$this->template->set_css('navbar.css');
		//$this->template->set_js('bootstrap.min.js','footer');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('cetak_surat');
		$this->template->render($data);
	}

	function ambil_data(){

        $modul=$this->input->post('modul');
        $id=$this->input->post('id');

        if($modul=="kabupaten"){
        echo $this->pejabat->kabupaten($id);
        }
        else if($modul=="kecamatan"){
        echo $this->pejabat->kecamatan($id);
        }
        else if($modul=="kelurahan"){
        echo $this->pejabat->kelurahan($id);
        }
    }

	public function error_404(){
		$this->load->view('page_404');
	}
}