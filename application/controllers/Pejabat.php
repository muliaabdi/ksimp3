<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pejabat extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		//$this->load->library('template');
		if (!$this->ion_auth->logged_in())redirect('auth/login', 'refresh');
		$this->template->set_platform('public');
		$this->template->set_theme('default');
		$this->load->model('M_pejabat', 'pejabat');
        $this->load->library('form_validation');
	}

	public function ajax_penyelenggara(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
//            panggil dulu library datatablesnya
            
            $this->load->library('datatables_ssp');
            
//            atur nama tablenya disini
            $table = 'penyelenggara';
 
            // Table's primary key
            $primaryKey = 'IDPERUSAHAANFINAL';
 
            // Array of database columns which should be read and sent back to DataTables.
            // The `db` parameter represents the column name in the database, while the `dt`
            // parameter represents the DataTables column identifier. In this case simple
            // indexes
 
            $columns = array(
                array('db' => 'IDPERUSAHAANFINAL', 'dt' => 'NO'),
                array('db' => 'IDPERUSAHAANFINAL', 'dt' => 'COMPANY ID'),
                array('db' => 'NMPERUSAHAAN', 'dt' => 'LEMBAGA PENYIARAN'),
                array('db' => 'IDPEMOHON', 'dt' => 'IDPEMOHON'),
                array('db' => 'NMUDARA', 'dt' => 'NMUDARA'),
                array('db' => 'LEMBAGASIAR', 'dt' => 'LEMBAGASIAR'),
                array(
                    'db' => 'IDPERUSAHAANFINAL',
                    'dt' => 'ACTION',
                    'formatter' => function( $d ) {
                        return '<a href="' . site_url('pejabat/konfirmasipenyelenggara/' . $d) . '">Edit</a>';
                    }
                ),
            );

            if ($this->ion_auth->in_group('TELEVISI')){
                $where = "LEMBAGASIAR LIKE '%TELEVISI%' or LEMBAGASIAR = '' or LEMBAGASIAR = '-'";
            }elseif ($this->ion_auth->in_group('RADIO')) {
                $where = "LEMBAGASIAR LIKE '%RADIO%' or LEMBAGASIAR = '' or LEMBAGASIAR = '-'";
            }else{
                $where = null;
            }

 
            // SQL server connection information
            $sql_details = array(
                'user' => 'root',
                'pass' => '',
                'db' => 'simp3_db',
                'host' => 'localhost'
            );
 
            echo json_encode(
                    Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, null, $where)
            );
        }
    }

    public function ajax_kantor(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
//            panggil dulu library datatablesnya
            
            $this->load->library('datatables_ssp');
            
//            atur nama tablenya disini
            $table = 'alamatkantor';
 
            // Table's primary key
            $primaryKey = 'NO';
 
            // Array of database columns which should be read and sent back to DataTables.
            // The `db` parameter represents the column name in the database, while the `dt`
            // parameter represents the DataTables column identifier. In this case simple
            // indexes
 
            $columns = array(
                array('db' => 'NO', 'dt' => 'NO'),
                array('db' => 'IDPERUSAHAANFINAL', 'dt' => 'COMPANY ID'),
                array('db' => 'NMPERUSAHAAN', 'dt' => 'LEMBAGA PENYIARAN'),
                array('db' => 'LEMBAGASIAR', 'dt' => 'LEMBAGASIAR'),
                array('db' => 'ALAMATKANTOR', 'dt' => 'ALAMATKANTOR'),
                array('db' => 'PROPKANTOR', 'dt' => 'PROPKANTOR'),
                array('db' => 'KABKANTOR', 'dt' => 'KABKANTOR'),
                array('db' => 'KECKANTOR', 'dt' => 'KECKANTOR'),
                array('db' => 'KELKANTOR', 'dt' => 'KELKANTOR'),
                array('db' => 'KODEPOS', 'dt' => 'KODEPOS'),
                array('db' => 'NPWP', 'dt' => 'NPWP'),
                array(
                    'db' => 'NO',
                    'dt' => 'ACTION',
                    'formatter' => function( $d ) {
                        return '<a href="' . site_url('pejabat/konfirmasikantor/' . $d) . '">Edit</a>';
                    }
                ),
                array(
                    'db' => 'STATUS',
                    'dt' => 'DELETED',
                ),
            );

            if ($this->ion_auth->in_group('TELEVISI')){
                $where = "LEMBAGASIAR LIKE '%TELEVISI%' or LEMBAGASIAR = '' or LEMBAGASIAR = '-'";
            }elseif ($this->ion_auth->in_group('RADIO')) {
                $where = "LEMBAGASIAR LIKE '%RADIO%' or LEMBAGASIAR = '' or LEMBAGASIAR = '-'";
            }else{
                $where = null;
            }
 
            // SQL server connection information
            $sql_details = array(
                'user' => 'root',
                'pass' => '',
                'db' => 'simp3_db',
                'host' => 'localhost'
            );
 
            echo json_encode(
                    Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, null, $where)
            );
        }
    }

    public function ajax_kronologis(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
//            panggil dulu library datatablesnya
            
            $this->load->library('datatables_ssp');
            
//            atur nama tablenya disini
            $table = 'kronologislembaga';
 
            // Table's primary key
            $primaryKey = 'NO';
 
            // Array of database columns which should be read and sent back to DataTables.
            // The `db` parameter represents the column name in the database, while the `dt`
            // parameter represents the DataTables column identifier. In this case simple
            // indexes
 
            $columns = array(
                array('db' => 'NO', 'dt' => 'NO'),
                array('db' => 'IDPERUSAHAAN', 'dt' => 'COMPANY ID'),
                array('db' => 'LEMBAGAPENYIARAN', 'dt' => 'LEMBAGA PENYIARAN'),
                array('db' => 'LEMBAGASIAR', 'dt' => 'LEMBAGASIAR'),
                array('db' => 'NOBERITAACARAFRB', 'dt' => 'NOBERITAACARAFRB'),
                array('db' => 'TANGGALFRB', 'dt' => 'TANGGALFRB'),
                array('db' => 'NOMORIZINPRINSIP', 'dt' => 'NOMORIZINPRINSIP'),
                array('db' => 'TANGGALIZINPRINSIP', 'dt' => 'TANGGALIZINPRINSIP'),
                array('db' => 'MASABERLAKUIPPPRINSIP', 'dt' => 'MASABERLAKUIPPPRINSIP'),
                array('db' => 'IPPPRINSIPSTATUS', 'dt' => 'IPPPRINSIPSTATUS'),
                array('db' => 'NOMORIZINPRINSIPPERPANJANGAN', 'dt' => 'NOMORIZINPRINSIPPERPANJANGAN'),
                array('db' => 'TANGGALIZINPRINSIPPERPANJANGAN', 'dt' => 'TANGGALIZINPRINSIPPERPANJANGAN'),
                array('db' => 'MASABERLAKUIPPPRINSIPPERPANJANGAN', 'dt' => 'MASABERLAKUIPPPRINSIPPERPANJANGAN'),
                array('db' => 'IPPPRINSIPPERPANJANGANSTATUS', 'dt' => 'IPPPRINSIPPERPANJANGANSTATUS'),
                array('db' => 'STATUSVERIFIKASI', 'dt' => 'STATUSVERIFIKASI'),
                // array('db' => 'NOBERITAACARAEUCS', 'dt' => 'NOBERITAACARAEUCS'),
                // array('db' => 'TANGGALEUCS', 'dt' => 'TANGGALEUCS'),
                // array('db' => 'NOSURATPERSETUJUAN', 'dt' => 'NOSURATPERSETUJUAN'),
                // array('db' => 'PERUBAHANDATASEBELUM', 'dt' => 'PERUBAHANDATASEBELUM'),
                // array('db' => 'PERUBAHANDATASESUDAH', 'dt' => 'PERUBAHANDATASESUDAH'),
                array('db' => 'NOMORIPPTETAP', 'dt' => 'NOMORIPPTETAP'),
                array('db' => 'TANGGALIPPTETAP', 'dt' => 'TANGGALIPPTETAP'),
                array('db' => 'MASABERLAKUIPPTETAP', 'dt' => 'MASABERLAKUIPPTETAP'),
                array('db' => 'IPPTETAPSTATUS', 'dt' => 'IPPTETAPSTATUS'),
                array('db' => 'NOBERITAACARAFRBIPPER', 'dt' => 'NOBERITAACARAFRBIPPER'),
                array('db' => 'TANGGALFRBIPPER', 'dt' => 'TANGGALFRBIPPER'),
                array('db' => 'NOMORIPPPERPANJANGAN', 'dt' => 'NOMORIPPPERPANJANGAN'),
                array('db' => 'TANGGALIPPPERPANJANGAN', 'dt' => 'TANGGALIPPPERPANJANGAN'),
                array('db' => 'MASABERLAKUIPPPERPANJANGAN', 'dt' => 'MASABERLAKUIPPPERPANJANGAN'),
                array('db' => 'IPPTETAPPERPANJANGANSTATUS', 'dt' => 'IPPTETAPPERPANJANGANSTATUS'),
                array('db' => 'NOBERITAACARAFRBIPPPENY', 'dt' => 'NOBERITAACARAFRBIPPPENY'),
                array('db' => 'TANGGALFRBIPPPENY', 'dt' => 'TANGGALFRBIPPPENY'),
                array('db' => 'NOMORIPPPENYESUAIAN', 'dt' => 'NOMORIPPPENYESUAIAN'),
                array('db' => 'TANGGALIPPPENYESUAIAN', 'dt' => 'TANGGALIPPPENYESUAIAN'),
                array('db' => 'MASABERLAKUIPPPENYESUAIAN', 'dt' => 'MASABERLAKUIPPPENYESUAIAN'),
                array('db' => 'IPPPENYESUAIANSTATUS', 'dt' => 'IPPPENYESUAIANSTATUS'),
                array('db' => 'NOBERITAACARAFRBIPPMIG', 'dt' => 'NOBERITAACARAFRBIPPMIG'),
                array('db' => 'TANGGALFRBIPPMIG', 'dt' => 'TANGGALFRBIPPMIG'),
                array('db' => 'NOMORIPPMIGRASIAMFM', 'dt' => 'NOMORIPPMIGRASI(AM-FM)'),
                // array('db' => 'TANGGALIPPMIGRASI', 'dt' => 'TANGGALIPPMIGRASI'),
                // array('db' => 'MASABERLAKUIPPMIGRASI', 'dt' => 'MASABERLAKUIPPMIGRASI'),
                // array('db' => 'IPPMIGRASISTATUS', 'dt' => 'IPPMIGRASISTATUS'),
                // array('db' => 'STATUSAKHIR', 'dt' => 'STATUSAKHIR'),
                // array('db' => 'KETERANGAN', 'dt' => 'KETERANGAN'),
                array(
                    'db' => 'NO',
                    'dt' => 'ACTION',
                    'formatter' => function( $d ) {
                        return '<a href="' . site_url('pejabat/konfirmasikronologis/' . $d) . '">Edit</a>';
                    }
                ),
            );

            if ($this->ion_auth->in_group('TELEVISI')){
                $where = "LEMBAGASIAR LIKE '%TELEVISI%' or LEMBAGASIAR = '' or LEMBAGASIAR = '-'";
            }elseif ($this->ion_auth->in_group('RADIO')) {
                $where = "LEMBAGASIAR LIKE '%RADIO%' or LEMBAGASIAR = '' or LEMBAGASIAR = '-'";
            }else{
                $where = null;
            }
 
            // SQL server connection information
            $sql_details = array(
                'user' => 'root',
                'pass' => '',
                'db' => 'simp3_db',
                'host' => 'localhost'
            );
 
            echo json_encode(
                    Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, null, $where)
            );
        }
    }

    public function ajax_update($NO){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        } else {
            $data = array(
                    'STATUS' => $this->input->post('DELETED'),
                );
            $this->pejabat->update('alamatkantor',array('NO' => $NO), $data);
            echo json_encode(array("status" => TRUE));
        }
    }

	public function penyelenggara(){
		$this->template->set_title('Konfirmasi Data Penyelenggara');
		$this->template->set_css('datatables/tools/css/dataTables.tableTools.css');
		$this->template->set_js('datatables/js/jquery.dataTables.js','footer');
		$this->template->set_js('datatables/js/dataTables.bootstrap.js','footer');
		$this->template->set_js('datatables/tools/js/dataTables.tableTools.js','footer');
		$this->template->set_js('icheck/icheck.min.js','footer');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('pejabat/reportpenyelenggara');
		$this->template->render();
	}

	public function kantor(){
		$this->template->set_title('Konfirmasi Data Kantor');
		$this->template->set_css('datatables/tools/css/dataTables.tableTools.css');
		$this->template->set_js('datatables/js/jquery.dataTables.js','footer');
		$this->template->set_js('datatables/js/dataTables.bootstrap.js','footer');
		$this->template->set_js('datatables/tools/js/dataTables.tableTools.js','footer');
		$this->template->set_js('icheck/icheck.min.js','footer');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('pejabat/reportkantor');
		$this->template->render();
	}

	public function kronologis(){
        $this->template->set_title('Konfirmasi Data Kronologis');
        $this->template->set_css('datatables/tools/css/dataTables.tableTools.css');
        $this->template->set_js('datatables/js/jquery.dataTables.js','footer');
        $this->template->set_js('datatables/js/dataTables.bootstrap.js','footer');
        $this->template->set_js('datatables/tools/js/dataTables.tableTools.js','footer');
        $this->template->set_js('icheck/icheck.min.js','footer');
        $this->template->set_js('parsley/parsley.min.js','footer');
        $this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
        $this->template->set_meta('author','Turan Karatuğ');
        $this->template->set_layout('pejabat/reportkronologis');
        $this->template->render();
	}

	public function konfirmasipenyelenggara($id){
		$data['data'] = $this->pejabat->getWhere('penyelenggara',array('IDPERUSAHAANFINAL' => $id));
		$this->template->set_title('Konfirmasi Data Penyelenggara');
		$this->template->set_js('icheck/icheck.min.js','footer');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('pejabat/konfirmasipenyelenggara');
		$this->template->render($data);
	}

    public function konfirmasikantor($id){
        $data['data'] = $this->pejabat->getWhere('alamatkantor',array('NO' => $id));
        $data['provinsi']=$this->pejabat->provinsi();
        $this->template->set_title('Konfirmasi Data Penyelenggara');
        $this->template->set_js('icheck/icheck.min.js','footer');
        $this->template->set_js('parsley/parsley.min.js','footer');
        $this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
        $this->template->set_meta('author','Turan Karatuğ');
        $this->template->set_layout('pejabat/konfirmasikantor');
        $this->template->render($data);
    }

    public function konfirmasikronologis($id){
        $data['data'] = $this->pejabat->getWhere('kronologislembaga',array('NO' => $id));
        $data['provinsi']=$this->pejabat->provinsi();
        $this->template->set_title('Konfirmasi Data Kronologis');
        $this->template->set_js('http://code.jquery.com/jquery-migrate-1.2.1.min.js','footer','remote');
        $this->template->set_js('http://code.jquery.com/ui/1.11.4/jquery-ui.js','footer','remote');
        $this->template->set_css('http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css','remote');
        //$this->template->set_js('http://code.jquery.com/jquery-1.9.1.js','footer','remote');
        $this->template->set_js('icheck/icheck.min.js','footer');
        $this->template->set_js('parsley/parsley.min.js','footer');
        $this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
        $this->template->set_meta('author','Turan Karatuğ');
        $this->template->set_layout('pejabat/konfirmasikronologis');
        $this->template->render($data);
    }

    function save(){
        $this->form_validation->set_rules('NMPERUSAHAAN', 'COMPANY NAME', 'required');
        $this->form_validation->set_rules('IDPEMOHON', 'ID PEMOHON', 'required');
        $this->form_validation->set_rules('NMUDARA', 'ON AIR NAME', 'required');
        $this->form_validation->set_rules('LEMBAGASIAR', 'LEMBAGA SIAR', 'required');

        if ($this->form_validation->run() == FALSE){
            $this->konfirmasipenyelenggara($this->input->post('IDPERUSAHAANFINAL'));
        }
        else{
            $data = array(
                'NMPERUSAHAAN' => $this->input->post('NMPERUSAHAAN'),
                'IDPEMOHON' => $this->input->post('IDPEMOHON'),
                'NMUDARA' => $this->input->post('NMUDARA'),
                'LEMBAGASIAR' => $this->input->post('LEMBAGASIAR'),
                );
            $this->pejabat->update('penyelenggara',array('IDPERUSAHAANFINAL'=>$this->input->post('IDPERUSAHAANFINAL')),$data);
            redirect(base_url().'pejabat/penyelenggara');
        }
    }

    function savealamat(){
        $this->form_validation->set_rules('ALAMATKANTOR', 'ALAMAT', 'required');
        $this->form_validation->set_rules('PROPKANTOR', 'PROVINSI', 'required');
        $this->form_validation->set_rules('KABKANTOR', 'KABUPATEN', 'required');
        $this->form_validation->set_rules('KECKANTOR', 'KECAMATAN', 'required');
        $this->form_validation->set_rules('KELKANTOR', 'KELURAHAN', 'required');
        $this->form_validation->set_rules('KODEPOS', 'KODE POS', 'required');
        $this->form_validation->set_rules('NPWP', 'NPWP', 'required');

        if ($this->form_validation->run() == FALSE){
            $this->konfirmasikantor($this->input->post('no'));
        }
        else{
            $data = array(
                'ALAMATKANTOR' => $this->input->post('ALAMATKANTOR'),
                'PROPKANTOR' => $this->input->post('PROPKANTOR'),
                'KABKANTOR' => $this->input->post('KABKANTOR'),
                'KECKANTOR' => $this->input->post('KECKANTOR'),
                'KELKANTOR' => $this->input->post('KELKANTOR'),
                'KODEPOS' => $this->input->post('KODEPOS'),
                'NPWP' => $this->input->post('NPWP'),
                );
            $this->pejabat->update('alamatkantor',array('NO'=>$this->input->post('no')),$data);
            redirect(base_url().'pejabat/kantor');
        }
    }

    function savekronologis(){
        $data = array(
            'IDPERUSAHAAN' => $this->input->post('IDPERUSAHAAN'),
            'LEMBAGAPENYIARAN' => $this->input->post('LEMBAGAPENYIARAN'),
            'NOBERITAACARAFRB' => $this->input->post('NOBERITAACARAFRB'),
            'TANGGALFRB' => $this->input->post('TANGGALFRB'),
            'NOMORIZINPRINSIP' => $this->input->post('NOMORIZINPRINSIP'),
            'TANGGALIZINPRINSIP' => $this->input->post('TANGGALIZINPRINSIP'),
            'MASABERLAKUIPPPRINSIP' => $this->input->post('MASABERLAKUIPPPRINSIP'),
            'IPPPRINSIPSTATUS' => $this->input->post('IPPPRINSIPSTATUS'),
            'NOMORIZINPRINSIPPERPANJANGAN' => $this->input->post('NOMORIZINPRINSIPPERPANJANGAN'),
            'TANGGALIZINPRINSIPPERPANJANGAN' => $this->input->post('TANGGALIZINPRINSIPPERPANJANGAN'),
            'MASABERLAKUIPPPRINSIPPERPANJANGAN' => $this->input->post('MASABERLAKUIPPPRINSIPPERPANJANGAN'),
            'IPPPRINSIPPERPANJANGANSTATUS' => $this->input->post('IPPPRINSIPPERPANJANGANSTATUS'),
            'STATUSVERIFIKASI' => $this->input->post('STATUSVERIFIKASI'),
            'NOBERITAACARAEUCS' => $this->input->post('NOBERITAACARAEUCS'),
            'TANGGALEUCS' => $this->input->post('TANGGALEUCS'),
            'NOSURATPERSETUJUAN' => $this->input->post('NOSURATPERSETUJUAN'),
            'PERUBAHANDATASEBELUM' => $this->input->post('PERUBAHANDATASEBELUM'),
            'PERUBAHANDATASESUDAH' => $this->input->post('PERUBAHANDATASESUDAH'),
            'NOMORIPPTETAP' => $this->input->post('NOMORIPPTETAP'),
            'TANGGALIPPTETAP' => $this->input->post('TANGGALIPPTETAP'),
            'MASABERLAKUIPPTETAP' => $this->input->post('MASABERLAKUIPPTETAP'),
            'IPPTETAPSTATUS' => $this->input->post('IPPTETAPSTATUS'),
            'NOBERITAACARAFRBIPPER' => $this->input->post('NOBERITAACARAFRBIPPER'),
            'TANGGALFRBIPPER' => $this->input->post('TANGGALFRBIPPER'),
            'NOMORIPPPERPANJANGAN' => $this->input->post('NOMORIPPPERPANJANGAN'),
            'TANGGALIPPPERPANJANGAN' => $this->input->post('TANGGALIPPPERPANJANGAN'),
            'MASABERLAKUIPPPERPANJANGAN' => $this->input->post('MASABERLAKUIPPPERPANJANGAN'),
            'IPPTETAPPERPANJANGANSTATUS' => $this->input->post('IPPTETAPPERPANJANGANSTATUS'),
            'NOBERITAACARAFRBIPPPENY' => $this->input->post('NOBERITAACARAFRBIPPPENY'),
            'TANGGALFRBIPPPENY' => $this->input->post('TANGGALFRBIPPPENY'),
            'NOMORIPPPENYESUAIAN' => $this->input->post('NOMORIPPPENYESUAIAN'),
            'TANGGALIPPPENYESUAIAN' => $this->input->post('TANGGALIPPPENYESUAIAN'),
            'MASABERLAKUIPPPENYESUAIAN' => $this->input->post('MASABERLAKUIPPPENYESUAIAN'),
            'IPPPENYESUAIANSTATUS' => $this->input->post('IPPPENYESUAIANSTATUS'),
            'NOBERITAACARAFRBIPPMIG' => $this->input->post('NOBERITAACARAFRBIPPMIG'),
            'TANGGALFRBIPPMIG' => $this->input->post('TANGGALFRBIPPMIG'),
            'NOMORIPPMIGRASIAMFM' => $this->input->post('NOMORIPPMIGRASIAMFM'),
            'TANGGALIPPMIGRASI' => $this->input->post('TANGGALIPPMIGRASI'),
            'MASABERLAKUIPPMIGRASI' => $this->input->post('MASABERLAKUIPPMIGRASI'),
            'IPPMIGRASISTATUS' => $this->input->post('IPPMIGRASISTATUS'),
            'STATUSAKHIR' => $this->input->post('STATUSAKHIR'),
            'KETERANGAN' => $this->input->post('KETERANGAN'),
            'NO'=>$this->input->post('NO'),
            );
        $this->pejabat->update('kronologislembaga',array('NO'=>$this->input->post('NO')),$data);
        redirect(base_url().'pejabat/kronologis');
    }

    function ambil_data(){

        $modul=$this->input->post('modul');
        $id=$this->input->post('id');

        if($modul=="kabupaten"){
        echo $this->pejabat->kabupaten($id);
        }
        else if($modul=="kecamatan"){
        echo $this->pejabat->kecamatan($id);
        }
        else if($modul=="kelurahan"){
        echo $this->pejabat->kelurahan($id);
        }
        else if($modul=="kodepos"){
        echo $this->pejabat->kodepos($id);
        }
    }
}