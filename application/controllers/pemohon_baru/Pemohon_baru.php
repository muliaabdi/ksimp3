<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pemohon_baru extends CI_Controller
{
	
    function __construct()
    {
        parent::__construct();
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->load->helper('url');
        $this->load->library('email');
        $this->_ci =& get_instance();
        $this->_ci->load->model('modules/pemohon/pemohon_model','pemohon');
        $this->load->model(MODULE_MODEL_PATH . 'user/dashboard_model','dashboard');

    }

    public function index() {
        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('login', 'refresh');
        }
        elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            // redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }
        else
        {
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $data['title'] = 'Company';
            $data['subtitle'] = 'Company list';
            $this->template->set_template('default');
            $this->template->add_js('assets/js/modules/dt-pemohonbaru.js');
            $this->template->write_view('content', MODULE_VIEW_PATH . 'pemohon/pemohon_baru/index', $data);
            $this->template->render();
        }
    }

    public function get_pemohon_baru() {
        if( $this->input->is_ajax_request() )
        {
            $this->datatables->select("PENYELENGGARA.IDPERUSAHAANFINAL as IDPERUSAHAANFINAL, PENYELENGGARA.NMPERUSAHAAN as NMPERUSAHAAN, PENYELENGGARA.NMUDARA as NMUDARA")
            // ->unset_column('id_pemohon')
            ->add_column('action', get_buttons('$1','PENYELENGGARA'), 'IDPERUSAHAANFINAL')
            ->from('PENYELENGGARA');
            echo $this->datatables->generate();
            return FALSE;
        }
        
        show_404();
    }

    
    

    function create_pemohon()
    {
        $this->data['title'] = $this->lang->line('create_company_title');
       
        // validate form input
        $this->form_validation->set_rules('NMPERUSAHAAN', $this->lang->line('create_company_validation_name_label'), 'required|trim');
        if ($this->form_validation->run() == TRUE)
        {
            //Update the groups user belongs to
            // $groupData = 5;
            $additional_data = array(
                'NMUDARA'    => $this->input->post('NMUDARA'),
            );

            $this->pemohon->create_pemohon($this->input->post('NMPERUSAHAAN'), $this->input->post('id_izin'),$additional_data);
            $this->session->set_flashdata('message', $this->ion_auth->messages());

            if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
            {
                redirect('pemohon_baru', 'refresh');
            } else {
                redirect('login');
            }
        }
        else
        {
            // display the create group form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['nm_pemohon'] = array(
                'name'  => 'nm_pemohon',
                'id'    => 'nm_pemohon',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('nm_pemohon'),
                );
            $this->data['alamat'] = array(
                'name'  => 'alamat',
                'id'    => 'alamat',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('alamat'),
                );                        
            $this->template->write_view('content', MODULE_VIEW_PATH . 'pemohon/pemohon_baru/create', $this->data);
            $this->template->render();
        }
    }

    function edit_pemohon($id_pemohon)
    {
        // bail if no group id given
        if(!$id_pemohon || empty($id_pemohon))
        {
            redirect('pemohon_baru', 'refresh');
        }
        $this->data['title'] = $this->lang->line('edit_company_title');
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
        {
            redirect('id_pemohon', 'refresh');
        }
        $pemohon_baru = $this->pemohon->pemohon_by_id($id_pemohon);
        // validate form input

        $this->form_validation->set_rules('nm_pemohon', $this->lang->line('edit_company_validation_name_label'), 'required|trim');
        if (isset($_POST) && !empty($_POST))
        {
            if ($this->form_validation->run() === TRUE)
            {
                $this->_validate();
                $data = array(
                    'nm_pemohon' => $this->input->post('nm_pemohon'),
                    'alamat' => $this->input->post('alamat'),
                    );
                $pemohon_update = $this->pemohon->update_pemohon(array('id_pemohon' => $this->input->post('id_pemohon')), $data);

                if($pemohon_update)
                {
                    $this->session->set_flashdata('message', $this->lang->line('edit_comp_saved'));
                }
                else
                {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                }
                redirect("pemohon_baru", 'refresh');
            }
        }
        // set the flash data error message if there is one

        $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        // pass the user to the view
        $this->data['pemohon_baru'] = $pemohon_baru;

        $readonly = $this->config->item('admin_group', 'ion_auth') === $pemohon_baru->nm_pemohon ? 'readonly' : '';
         $this->data['nm_pemohon'] = array(
                'name'  => 'nm_pemohon',
                'id'    => 'nm_pemohon',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('nm_pemohon'),
                );
            $this->data['alamat'] = array(
                'name'  => 'alamat',
                'id'    => 'alamat',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('alamat'),
                );                        
            $this->template->set_template('default');
            $this->template->write_view('content', MODULE_VIEW_PATH . 'pemohon/pemohon_baru/update', $this->data);
            $this->template->render();

    }


    function delete_pemohon($IDPERUSAHAANFINAL)

    {

        if( $this->input->is_ajax_request() )
        {
            if($this->pemohon->delete_pemohon($IDPERUSAHAANFINAL)) {
                $message = "<strong>User </strong> Company have been removed!";
                $this->session->set_flashdata('message', $message);
            }
            return FALSE;

        }
        show_404();

    }



    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('nm_pemohon') == '')
        {
            $data['inputerror'][] = 'nm_pemohon';
            $data['error_string'][] = 'pemohon name is required';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
    
    public function get_companies() {
        if( $this->input->is_ajax_request() )
        {
            $companies = $this->company->get_company_option();
                    
            echo json_encode($companies);
            
            return FALSE;
        }
        
        show_404();
    }

    // function sendMail(){
    //     $length = 10;
    //     $Password = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

    //     $IDPERUSAHAANFINAL = $this->pemohon->kodekandidat();
    //     $des_email         = $this->input->post('EMAIL_CP');
    //     //send email
    //     $config['useragent'] = 'CodeIgniter';
    //     $config['protocol'] = 'smtp';
    //     //$config['mailpath'] = '/usr/sbin/sendmail';
    //     $config['smtp_host'] = 'ssl://smtp.googlemail.com';
    //     $config['smtp_user'] = 'erwinnekketsu@gmail.com';
    //     $config['smtp_pass'] = '1@nekketsu';
    //     $config['smtp_port'] = 465; 
    //     $config['smtp_timeout'] = 5;
    //     $config['wordwrap'] = TRUE;
    //     $config['wrapchars'] = 76;
    //     $config['mailtype'] = 'html';
    //     $config['charset'] = 'utf-8';
    //     $config['validate'] = FALSE;
    //     $config['priority'] = 3;
    //     $config['crlf'] = "\r\n";
    //     $config['newline'] = "\r\n";
    //     $config['bcc_batch_mode'] = FALSE;
    //     $config['bcc_batch_size'] = 200;

    //     $this->load->library('email', $config);
    //     $this->email->set_newline("\r\n");
    //     $this->email->from('erwinnekketsu@gmail.com'); // change it to yours
    //     $this->email->to($des_email);// change it to yours
    //     //$this->email->to('erwinnekketsu@gmail.com');// change it to yours
    //     $this->email->subject('Your SIMP3 Access');
    //     $message = 'Username anda : '.$IDPERUSAHAANFINAL.'<br> & Password Anda : '.$Password;
    //     $this->email->message($message);
    //     if($this->email->send()){
    //     //echo 'Email sent.';
    //         echo "Good jobs";
    //     }else{
    //         echo "Sorry";
    //     }
       
    // }
}