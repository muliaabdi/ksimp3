<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kantor extends CI_Controller
{
	function __construct(){
		parent::__construct();
		if (!$this->ion_auth->logged_in())redirect('auth/login', 'refresh');
		$this->template->set_platform('public');
		$this->template->set_theme('default');
		$this->load->model('konfirmasi/Kantor_model','kantor');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
	}

	public function index(){
		$data['data']=$this->kantor->getWhere('alamatkantor',array('IDPERUSAHAANFINAL' => 'TS060400042'));
		$this->template->set_title('Konfirmasi Data Kantor');
		$this->template->set_css('datatables/tools/css/dataTables.tableTools.css');
		$this->template->set_js('datatables/js/jquery.dataTables.js','footer');
		$this->template->set_js('datatables/js/dataTables.bootstrap.js','footer');
		$this->template->set_js('datatables/tools/js/dataTables.tableTools.js','footer');
		$this->template->set_js('icheck/icheck.min.js','footer');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('konfirmasi/report_kantor');
		$this->template->render($data);
	}

	public function edit($id){
		if ($id==NULL) {
			redirect_back();
		}
		$this->template->set_title('Konfirmasi Data Alamat Kantor');

		$this->form_validation->set_rules('ALAMAT', 'Alamat', 'required');
		$this->form_validation->set_rules('PROPKANTOR', 'Provinsi', 'required');
		$this->form_validation->set_rules('KABKANTOR', 'Kabupaten', 'required');
		$this->form_validation->set_rules('KECKANTOR', 'Kecamatan', 'required');
		$this->form_validation->set_rules('KELKANTOR', 'Kelurahan', 'required');
		$this->form_validation->set_rules('KODEPOS', 'Kode Pos', 'required');
		$this->form_validation->set_rules('NPWP', 'NPWP', 'required');

		if ($this->form_validation->run() === TRUE){
			$user = $this->ion_auth->user()->row()->username;
			$data = array(
				'ALAMAT' 	=> $this->input->post('ALAMAT'),
				'PROPKANTOR'=> $this->input->post('PROPKANTOR'),
				'KABKANTOR' => $this->input->post('KABKANTOR'),
				'KECKANTOR' => $this->input->post('KECKANTOR'),
				'KELKANTOR' => $this->input->post('KELKANTOR'),
				'KODEPOS' 	=> $this->input->post('KODEPOS'),
				'NPWP' 		=> $this->input->post('NPWP'),
				);
		}
		if ($this->form_validation->run() === TRUE && $this->kantor->update('alamatkantor', array('username' => $user), $data)){
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("konfirmasi/kantor", 'refresh');
        }
        else{
        	$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        	$this->load->model('Regions_model', 'regions');
        	$data['alamat'] = $this->kantor->getWhere('alamatkantor',array('NO' => $id));
			$data['provinsi'] = $this->regions->provinsi();
			// $this->template->set_js('parsley/parsley.min.js','footer');
			$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
			// $this->template->set_js('moment/moment.min.js','footer');
			$this->template->set_meta('author','Turan Karatuğ');
			$this->template->set_layout('konfirmasi/konfirmasi_kantor');
			$this->template->render($data);
		}
	}
}