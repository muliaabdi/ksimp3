<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemancar extends CI_Controller
{
	function __construct(){
		parent::__construct();
		if (!$this->ion_auth->logged_in())redirect('auth/login', 'refresh');
		$this->template->set_platform('public');
		$this->template->set_theme('default');
		$this->load->model('konfirmasi/Pemancar_model','pemancar');
	}

	public function index(){
		$data['data']=$this->pemancar->getWhere('alamatkantor',array('IDPERUSAHAANFINAL' => 'RK040400001'));
		$this->template->set_title('Konfirmasi Data Kantor');
		$this->template->set_css('datatables/tools/css/dataTables.tableTools.css');
		$this->template->set_js('datatables/js/jquery.dataTables.js','footer');
		$this->template->set_js('datatables/js/dataTables.bootstrap.js','footer');
		$this->template->set_js('datatables/tools/js/dataTables.tableTools.js','footer');
		$this->template->set_js('icheck/icheck.min.js','footer');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('konfirmasi/report_pemancar');
		$this->template->render($data);
	}

	public function edit(){
		$data['provinsi']=$this->pemancar->provinsi();
		$this->template->set_title('Konfirmasi Data Penyelenggara');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_js('moment/moment.min.js','footer');
		$this->template->set_js('bootstrap-datetimepicker.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('konfirmasi/konfirmasi_pemancar');
		$this->template->render($data);
	}

	function ambil_data(){
        $modul=$this->input->post('modul');
        $id=$this->input->post('id');
        if($modul=="kabupaten"){
        echo $this->pemancar->kabupaten($id);
        }
        else if($modul=="kecamatan"){
        echo $this->pemancar->kecamatan($id);
        }
        else if($modul=="kelurahan"){
        echo $this->pemancar->kelurahan($id);
        }
    }
}