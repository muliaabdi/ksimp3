<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Studio extends CI_Controller{
	function __construct(){
		parent::__construct();
		if (!$this->ion_auth->logged_in())redirect('auth/login', 'refresh');
		$this->template->set_platform('public');
		$this->template->set_theme('default');
		$this->load->model('konfirmasi/Kantor_model','studio');
	}

	public function index(){
		$data['data']=$this->studio->getWhere('alamatkantor',array('IDPERUSAHAANFINAL' => 'RK040400001'));
		$this->template->set_title('Konfirmasi Data Penyelenggara');
		$this->template->set_js('parsley/parsley.min.js','footer');
		$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
		$this->template->set_js('moment/moment.min.js','footer');
		$this->template->set_js('bootstrap-datetimepicker.min.js','footer');
		$this->template->set_meta('author','Turan Karatuğ');
		$this->template->set_layout('konfirmasi/report_studio');
		$this->template->render($data);
	}

	public function edit($id){
		if ($id==NULL) {
			redirect_back();
		}
		$this->template->set_title('Konfirmasi Data Alamat Kantor');

		$this->form_validation->set_rules('ALAMAT', 'Alamat', 'required');
		$this->form_validation->set_rules('PROPSTUDIO', 'Provinsi', 'required');
		$this->form_validation->set_rules('KABSTUDIO', 'Kabupaten', 'required');
		$this->form_validation->set_rules('KECSTUDIO', 'Kecamatan', 'required');
		$this->form_validation->set_rules('KELSTUDIO', 'Kelurahan', 'required');
		$this->form_validation->set_rules('KODEPOS', 'Kode Pos', 'required');
		$this->form_validation->set_rules('NOTELP', 'No. Telp', 'required');
		$this->form_validation->set_rules('NOFAX', 'No. Fax', 'required');

		if ($this->form_validation->run() === TRUE){
			$user = $this->ion_auth->user()->row()->username;
			$data = array(
				'ALAMAT' 	=> $this->input->post('ALAMAT'),
				'PROPSTUDIO'=> $this->input->post('PROPSTUDIO'),
				'KABSTUDIO' => $this->input->post('KABSTUDIO'),
				'KECSTUDIO' => $this->input->post('KECSTUDIO'),
				'KELSTUDIO' => $this->input->post('KELSTUDIO'),
				'KODEPOS' 	=> $this->input->post('KODEPOS'),
				'NOTELP' 	=> $this->input->post('NOTELP'),
				'NOFAX' 	=> $this->input->post('NOFAX'),
				);
		}
		if ($this->form_validation->run() === TRUE && $this->kantor->update('alamatstudio', array('username' => $user), $data)){
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("konfirmasi/kantor", 'refresh');
        }
        else{
        	$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        	$this->load->model('Regions_model', 'regions');
        	$data['alamat'] = $this->kantor->getWhere('alamatkantor',array('NO' => $id));
			$data['provinsi'] = $this->regions->provinsi();
			$this->template->set_js('nicescroll/jquery.nicescroll.min.js','footer');
			$this->template->set_meta('author','Turan Karatuğ');
			$this->template->set_layout('konfirmasi/konfirmasi_studio');
			$this->template->render($data);
		}
	}
}