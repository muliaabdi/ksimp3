<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class M_candidate extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function kodekandidat(){
		$this->db->select ( '*' ); 
		$this->db->from ( 'candidate' );
		$this->db->order_by("id_candidate", "desc");
		$query = $this->db->get();
		if(!$query->result_array()){
			$kode = "CA0001";
		}
		else{
			$data = $query->result_array();
			$kode = $data[0]['id_candidate'];
			if( $kode ) {
				$kode = substr( $kode, 0, 6 );
				$kode++;
			}
		}
		return $kode;
		// echo $kode;
	}


}