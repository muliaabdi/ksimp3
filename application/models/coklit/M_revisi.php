<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_revisi extends CI_Model{

	function get(){
		$this->db->select( '*' ); 
		$this->db->from( 'penyelenggara' );
		$query = $this->db->get();
		return $query->result();
	}

	function getWhere($table,$where){
		$data = $this->db->get_where($table, $where)->result();
		return $data;
	}

	function getDistinct($table,$where){
		$data = $this->db->select($where)
		                 ->from($table)
		                 ->group_by($where)
		                 ->get();
		return $data->result();
	}

	function update($table,$where,$data){
		$this->db->where($where)->update($table, $data);
		return true;
	}

	function delete($table,$where){
		$this->db->delete($table,$where);
		return true;
	}

	function insert($table,$data){
		$this->db->insert($table, $data);
		return true;
	}

	function zona2(){
	$this->db->order_by('DAERAH','ASC');
	$provinces= $this->db->get('zona');
	return $provinces->result();
	}

	public function get_zona($id) {
        $zn = $this->db->select('*')
                ->from('zona')
                ->where('NO', $id)
                ->get()->result();
        return $zn;
    }

	function provinsiwl(){
	$this->db->order_by('PROVINSI','ASC');
	$provinces= $this->db->get('wilayahlayanantv');
	return $provinces->result_array();
	}

	function provinsi(){
	$this->db->order_by('PROV','ASC');
	$provinces= $this->db->get('prov');
	return $provinces->result_array();
	}


	function kabupaten($provId){

	$kabupaten="<option value='0'>--pilih--</pilih>";

	$this->db->order_by('KABKOT','ASC');
	$kab= $this->db->get_where('kabkot',array('PROVID'=>$provId));

	foreach ($kab->result_array() as $data ){
	$kabupaten.= "<option value='$data[KABKOTID]'>$data[KABKOT]</option>";
	}

	return $kabupaten;

	}

	function kecamatan($kabId){
	$kecamatan="<option value='0'>--pilih--</pilih>";

	$this->db->order_by('KEC','ASC');
	$kec= $this->db->get_where('kec',array('KABKOTID'=>$kabId));

	foreach ($kec->result_array() as $data ){
	$kecamatan.= "<option value='$data[KECID]'>$data[KEC]</option>";
	}

	return $kecamatan;
	}

	function kelurahan($kecId){
	$kelurahan="<option value='0'>--pilih--</pilih>";

	$this->db->order_by('KEL','ASC');
	$kel= $this->db->get_where('kel',array('KECID'=>$kecId));

	foreach ($kel->result_array() as $data ){
	$kelurahan.= "<option value='$data[KELID]'>$data[KEL]</option>";
	}

	return $kelurahan;
	}

	function kodepos($kecId){
	$kelurahan="<option value='0'>--pilih--</pilih>";

	$this->db->order_by('kodepos','ASC');
	$kel= $this->db->get_where('kodepos',array('KECID'=>$kecId));

	foreach ($kel->result_array() as $data ){
	$kelurahan.= "<option value='$data[kodepos]'>$data[kodepos]</option>";
	}

	return $kelurahan;
	}

	function zona($kabId){
	$zona="<option value='0'>--pilih--</pilih>";

	$this->db->order_by('KABKOT','ASC');
	$kec= $this->db->get_where('kabkot',array('KABKOTID'=>$kabId));

	foreach ($kec->result_array() as $data ){
	$zona.= "<option value='$data[ZONA]'>$data[ZONA]</option>";
	}

	return $zona;
	}

	function wilayahlayanantv($provId){
	$zona="";

	$this->db->order_by('WILAYAH','ASC');
	$prov= $this->db->select('*')->from('wilayahlayanantv')->where_in('PROVID',$provId)->group_by('PROVINSI')->get()->result();
	foreach ($prov as $opt) {
		$zona.= "<optgroup label='$opt->PROVINSI'>";
		$kec= $this->db->select('*')->from('wilayahlayanantv')->where('PROVID',$opt->PROVID)->get();
		foreach ($kec->result_array() as $data ){
			$zona.= "<option value='$data[WILAYAH]'>$data[WILAYAH]</option>";
		}
	}

	return $zona;
	}

	function wlkabkot($provId){
	$zona="<option value='0'>--pilih--</pilih>";

	$this->db->order_by('KABKOT','ASC');
	$this->db->group_by('KABKOT');
	$kec= $this->db->get_where('wilayahlayananradio',array('PROVID'=>$provId));

	foreach ($kec->result_array() as $data ){
	$zona.= "<option value='$data[KABKOTID]'>$data[KABKOT]</option>";
	}

	return $zona;
	}

	function wilayahlayananradio($kabkotId){
	$zona="<option value='0'>--pilih--</pilih>";

	$this->db->order_by('SALURAN','ASC');
	$kec= $this->db->get_where('wilayahlayananradio',array('KABKOTID'=>$kabkotId));

	foreach ($kec->result_array() as $data ){
	$zona.= "<option value='$data[WILAYAH]'>$data[WILAYAH]</option>";
	}

	return $zona;
	}

}