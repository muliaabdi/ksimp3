<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pemohon_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		// $this->trigger_events('model_constructor');
		$this->lang->load('ion_auth');
                date_default_timezone_set('Asia/Jakarta');
		
	}

	function get_jenis_izin()
	{
		$query = $this->db->query("SELECT * FROM jenis_izin");
		return $query->result();
	}

	function get_jenis_izin_id($id)
	{
		$query = $this->db->query("SELECT * FROM jenis_izin where id_izin = '".$id."' ");
		return $query->result();
	}

	function pemohon_by_id($IDPERUSAHAANFINAL)
	{
		$this->db->where('IDPERUSAHAANFINAL',$IDPERUSAHAANFINAL);
		$query = $this->db->get('PENYELENGGARA');

		return $query->row();
	}

	function get_company_by_id($id)
	{
		$query = $this->db->query("SELECT * FROM company INNER JOIN users_metas ON company.comp_id = users_metas.comp_id WHERE user_id = '".$id."'");

		return $query;
	}

	public function get_user($id)
	{
		$query = $this->db->query("SELECT * from users_metas JOIN penyelenggara on users_metas.first_name = penyelenggara.IDPERUSAHAANFINAL JOIN users ON users_metas.user_id = users.id WHERE user_id = ".$id." ");
        return $query;
	}

	function get_company_option()
	{
		$query = $this->db->query("SELECT * FROM company");
		return $query->result();
	}

	function get_company($comp_id)
	{
		$query = $this->db->query("SELECT * FROM company INNER JOIN users_metas ON company.comp_id = users_metas.comp_id WHERE users_metas.id = '".$comp_id."'");
		return $query->result();
	}


	public function update_pemohon($id_pemohon = FALSE, $data )
	{
		$this->db->update('pemohon_baru', $data, $id_pemohon);
		// $this->set_message('company_update_successful');
		return $this->db->affected_rows();

		return TRUE;
	}

	public function delete_pemohon($IDPERUSAHAANFINAL = FALSE)
	{
		$this->db->where('IDPERUSAHAANFINAL', $IDPERUSAHAANFINAL);
		$this->db->delete('PENYELENGGARA');
	}

	
}