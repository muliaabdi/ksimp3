<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kantor_model extends CI_Model{

	function get(){
		$this->db->select( '*' ); 
		$this->db->from( 'penyelenggara' );
		$query = $this->db->get();
		return $query->result();
	}

	function getWhere($table,$where){
		$data = $this->db->get_where($table, $where)->result();
		return $data;
	}

	function update($table,$where,$data){
		$this->db->where($where)->update($table, $data);
		return true;
	}

}