<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of General_model
 *
 * @author Lenovo
 */
class General_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function trans_category($cat_id) {
        $this->db->select('cat_id,cat_name');
        $this->db->where('cat_id',$cat_id);
        
        return $this->db->get('categories')->result();
    }
    
    public function trans_user($user_id) {
        $this->db->select('id,CONCAT(first_name," ",last_name) AS username');
        $this->db->where('id',$user_id);
        
        return $this->db->get('users')->result();
    }
    
    public function unapprove_documents($file_name) {
        $this->db->select('documents.doc_id,documents.doc_name,doc_num');
        $this->db->join('approvals','documents.doc_id = approvals.doc_id','LEFT');
        $this->db->where('approvals.doc_id IS NULL');
        $this->db->like('documents.doc_name',$file_name,'both');
        
        
        return $this->db->get('documents')->result();
    }
    
    public function approve_documents($file_name) {
        $this->db->select('documents.doc_id,documents.doc_name,doc_num');
        $this->db->join('approvals','documents.doc_id = approvals.doc_id');
        $this->db->where('approvals.status','502');
        $this->db->like('documents.doc_name',$file_name,'both');
        
        return $this->db->get('documents')->result();
    }
    
    public function get_users() {
        $this->db->select('users.id AS id,CONCAT(users_metas.first_name," ",users_metas.last_name) AS name');
        $this->db->join('users_metas','users_metas.user_id = users.id');
        
        return $this->db->get('users')->result();
    }
    
    public function get_departments() {
        $this->db->select('departments.dep_id AS id,departments.dep_name AS name');
        
        return $this->db->get('departments')->result();
    }
    
    public function get_department_by_userid($user_id) {
        $this->db->select('departments.dep_id,departments.dep_name,company.comp_id,company.comp_name');
        $this->db->join('departments','departments.dep_id = users_metas.dep_id');
        $this->db->join('company','company.comp_id = users_metas.comp_id');
        $this->db->where('users_metas.user_id',$user_id);
        
        return $this->db->get('users_metas')->row();
    }

    public function get_company() {
        $this->db->select('company.comp_id AS id,company.comp_name AS name');
        
        return $this->db->get('company')->result();
    }
    
    public function get_company_by_userid($user_id) {
        $this->db->select('company.comp_id,company.comp_name');
        $this->db->join('company','company.comp_id = users_metas.comp_id');
        $this->db->where('users_metas.user_id',$user_id);
        
        return $this->db->get('users_metas')->row();
    }
    
    public function count_user_notificatons($user_id) {
        $this->db->where(array('notif_to'=>$user_id,'is_read'=>'700'));
        
        return $this->db->get('users_notifs')->num_rows();
    }
    
    public function get_user_notificatons($user_id,$mode="all") {
        $this->db->select('users_notifs.*,CONCAT(users1.first_name," ",users1.last_name) AS name_from, CONCAT(users2.first_name," ",users2.last_name) AS name_to');
        $this->db->join('users_metas users1','users1.user_id = users_notifs.notif_from');
        $this->db->join('users_metas users2','users2.user_id = users_notifs.notif_to');
        $this->db->where('notif_to', $user_id);
        $this->db->order_by('notif_added', 'DESC');
        if($mode != "all"){
            $this->db->limit(10);
        }
        
        return $this->db->get('users_notifs')->result();
    }
    
    public function get_single_notification($notif_id) {
        $this->db->select('users_notifs.*,CONCAT(users1.first_name," ",users1.last_name) AS name_from');
        $this->db->join('users_metas users1','users1.user_id = users_notifs.notif_from');
        $this->db->where('notif_id', $notif_id);
        
        return $this->db->get('users_notifs')->row();
    }
    
    public function trans_filename($doc_id) {
        $file_name = $this->db->select('documents.doc_name')->where('documents.doc_id',$doc_id)->get('documents')->row();

        return $file_name->doc_name;
    }
    
    public function notification_read($id) {
        return $this->db->where('notif_id', $id)->update('users_notifs', array('is_read'=>'701'));
    }
    
    public function get_email_address($dep,$type,$comp=NULL) {
        $this->db->select("CONCAT(users_metas.first_name,' ',users_metas.last_name) AS username, users.email");
        $this->db->join("users","users_metas.user_id = users.id");
        if($type == "single") {
            $this->db->where("users_metas.user_id",$dep);
        } else if($type == "multi"){
            $this->db->join("departments","users_metas.dep_id = departments.dep_id");
            $this->db->where(array("users_metas.dep_id"=>$dep,"users_metas.comp_id"=>$comp));
        } else {
            $this->db->join("departments","users_metas.dep_id = departments.dep_id");
            $this->db->join("company","users_metas.comp_id = company.comp_id");
            $this->db->where("users_metas.dep_id",$dep);
        }
        
        return $this->db->get("users_metas")->result_array();
    }
    
    public function get_department_uid($comp_id,$dep_id) {
        return $this->db->select('users_metas.user_id')->where(array("users_metas.dep_id"=>$dep_id,"users_metas.comp_id"=>$comp_id))->get('users_metas')->result_array();
    }
    
    public function get_company_uid($comp_id) {
        return $this->db->select('users_metas.user_id')->where('comp_id',$comp_id)->get('users_metas')->result_array();
    }
    
    public function get_attribute_uid($comp_id) {
        return $this->db->select('users_metas.user_id,users_metas.comp_id,users_metas.dep_id')->where('comp_id',$comp_id)->get('users_metas')->result_array();
    }
    
    public function save_document($data) {
        $this->db->insert('documents', $data);
        return $this->db->insert_id();
    }
    
    public function get_categories() {
        $this->db->select('categories.cat_id AS id,categories.cat_name AS name');
        
        return $this->db->get('categories')->result();
    }
    
    public function get_description($doc_id) {
        $file_name = $this->db->select('documents.doc_desc')->where('documents.doc_id',$doc_id)->get('documents')->row();

        return $file_name->doc_desc;
    }
    
    public function get_setting() {
        $settings = $this->db->select('key_value_str')
                // ->where('key_config IN("email_sender_address","email_sender_password")')
                ->get('settings')->result();
        
        return $settings;
    }
    
    public function set_setting($addr="",$pass="",$locpath="") {
        $status = "";
        if($addr != ""){
            $dataAddr = array(
                    'key_value_str' => $addr
            );
            
            $this->db->update('settings', $dataAddr, 'key_config = "email_sender_address"');
        } 
        if($pass != "") {
            $dataPass = array(
                    'key_value_str' => $pass
            );
            
            $this->db->update('settings', $dataPass, 'key_config = "email_sender_password"');
        }
        if($pass != "") {
            $dataPass = array(
                    'key_value_str' => $locpath
            );
            
            $this->db->update('settings', $dataPass, 'key_config = "backup_location_path"');
        }
        
        return TRUE;
    }

     public function get()
    {
        # code...
    }

    function provinsi_kantor(){


    $this->db->order_by('name','ASC');
    $provinces= $this->db->get('provinces');


    return $provinces->result_array();


    }


    function kabupaten_kantor($provId){

    $kabupaten="<option value='0'>--pilih--</pilih>";

    $this->db->order_by('name','ASC');
    $kab= $this->db->get_where('regencies',array('province_id'=>$provId));

    foreach ($kab->result_array() as $data ){
    $kabupaten.= "<option value='$data[id]'>$data[name]</option>";
    }

    return $kabupaten;

    }

    function kecamatan_kantor($kabId){
    $kecamatan="<option value='0'>--pilih--</pilih>";

    $this->db->order_by('name','ASC');
    $kec= $this->db->get_where('districts',array('regency_id'=>$kabId));

    foreach ($kec->result_array() as $data ){
    $kecamatan.= "<option value='$data[id]'>$data[name]</option>";
    }

    return $kecamatan;
    }

    function kelurahan_kantor($kecId){
    $kelurahan="<option value='0'>--pilih--</pilih>";

    $this->db->order_by('name','ASC');
    $kel= $this->db->get_where('villages',array('district_id'=>$kecId));

    foreach ($kel->result_array() as $data ){
    $kelurahan.= "<option value='$data[id]'>$data[name]</option>";
    }

    return $kelurahan;
    }
    
    // alias from get_company()
    // DEPRECATED!
//    public function get_companies() {
//        $this->db->select('company.comp_id,company.comp_name');
//        
//        return $this->db->get('company')->result();
//    }
}
