<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pejabat extends CI_Model{

	function get(){
		$this->db->select( '*' ); 
		$this->db->from( 'penyelenggara' );
		$query = $this->db->get();
		return $query->result();
	}

	function getWhere($table,$where){
		$data = $this->db->get_where($table, $where)->result();
		return $data;
	}

	function update($table,$where,$data){
		$this->db->where($where)->update($table, $data);
		return true;
	}

	function provinsi(){
	$this->db->order_by('PROV','ASC');
	$provinces= $this->db->get('prov');
	return $provinces->result_array();
	}


	function kabupaten($provId){

	$kabupaten="<option value='0'>--pilih--</pilih>";

	$this->db->order_by('KABKOT','ASC');
	$kab= $this->db->get_where('kabkot',array('PROVID'=>$provId));

	foreach ($kab->result_array() as $data ){
	$kabupaten.= "<option value='$data[KABKOTID]'>$data[KABKOT]</option>";
	}

	return $kabupaten;

	}

	function kecamatan($kabId){
	$kecamatan="<option value='0'>--pilih--</pilih>";

	$this->db->order_by('KEC','ASC');
	$kec= $this->db->get_where('kec',array('KABKOTID'=>$kabId));

	foreach ($kec->result_array() as $data ){
	$kecamatan.= "<option value='$data[KECID]'>$data[KEC]</option>";
	}

	return $kecamatan;
	}

	function kelurahan($kecId){
	$kelurahan="<option value='0'>--pilih--</pilih>";

	$this->db->order_by('KEL','ASC');
	$kel= $this->db->get_where('kel',array('KECID'=>$kecId));

	foreach ($kel->result_array() as $data ){
	$kelurahan.= "<option value='$data[KELID]'>$data[KEL]</option>";
	}

	return $kelurahan;
	}

	function kodepos($kecId){
	$kelurahan="<option value='0'>--pilih--</pilih>";

	$this->db->order_by('kodepos','ASC');
	$kel= $this->db->get_where('kodepos',array('KECID'=>$kecId));

	foreach ($kel->result_array() as $data ){
	$kelurahan.= "<option value='$data[kodepos]'>$data[kodepos]</option>";
	}

	return $kelurahan;
	}

}